package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.Utility;

public class ChangeMobileNumberActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    Button update_bt;
    EditText mobile_et;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_mobile_number);
        initXml();
        initObj();
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_changemobile_back);
        mobile_et = findViewById(R.id.et_changemobile_number);
        update_bt = findViewById(R.id.bt_changemobile_update);

        back_iv.setOnClickListener(this);
        update_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.bt_changemobile_update:
                getData();
                break;

            case R.id.iv_changemobile_back:
                finish();
                break;
        }
    }

    private void getData() {

        String c_id = sessionManager.getData(SessionManager.KEY_CID);
        String mobile = mobile_et.getText().toString();
        if (!mobile.equals("")){
            boolean internet = connectionDetector.isConnected();
            if (internet){
                changeMobile(c_id,mobile);
            }else {
                Utility.toastView(ctx,ctx.getString(R.string.no_internet));
            }
        }else {
            Utility.toastView(ctx,"Enter Mobile number");
        }
    }

    private void changeMobile(String c_id, final String mobile) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_CHANGE_MOBILE)
                .addBodyParameter("customer_id", c_id)
                .addBodyParameter("mobile_number", mobile)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        setResponse(response.toString(),mobile);
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        setResponse("error",mobile);
                    }
                });

    }

    private void setResponse(String s, String mobile) {

        if (!s.equals("error")){
            try {
                JSONObject jsonObject = new JSONObject(s);
                String status = jsonObject.getString("status");
                if (status.equals("1")){
                    Utility.toastView(ctx,"Successfully updated");
                    sessionManager.setData(SessionManager.KEY_MOBILE,mobile);
                    startActivity(new Intent(ctx,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.something_went));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.something_went));
        }
    }
}
