package igrand.technology.info.eganacsi.seller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import igrand.technology.info.eganacsi.model.C_SellerProfileData;
import igrand.technology.info.eganacsi.model.C_SellerProfileResponse;
import igrand.technology.info.eganacsi.model.MyProductData;
import igrand.technology.info.eganacsi.model.MyProductListResp;
import igrand.technology.info.eganacsi.model.MyProductsList;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.seller.adapter.MySellerProductAdapter;
import igrand.technology.info.eganacsi.seller.adapter.SellerMyServicesAdapter;
import igrand.technology.info.eganacsi.seller.response.myproduct.MyProductStatus;
import igrand.technology.info.eganacsi.seller.response.myproduct.SellerPopularProduct;
import igrand.technology.info.eganacsi.seller.response.myservices.MyServiceStatus;
import igrand.technology.info.eganacsi.seller.response.myservices.SellerServices;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerMyProductActivity extends AppCompatActivity implements View.OnClickListener , MySellerProductAdapter.OnClickListener {

    Context ctx;
    ImageView back_iv;
    TextView title_tv;
    Button add_bt;
    RecyclerView recyclerView;
    String type;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    MySellerProductAdapter sellerProductAdapter;
    SellerMyServicesAdapter sellerMyServicesAdapter;
    igrand.technology.info.eganacsi.ApiInterface apiInterface;
    String Cid;

    public static final int PRODUCT_VIEW = 1;
    public static final int SERVICE_VIEW = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_my_product);

        SharedPreferences sharedPreferences =getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email","");
        String password = sharedPreferences.getString("password","");

        apiInterface= igrand.technology.info.eganacsi.ApiClient.getClient1().create(igrand.technology.info.eganacsi.ApiInterface.class);
        Call<C_SellerProfileResponse> call=apiInterface.CustomerSellerProfile(email,password);
        call.enqueue(new Callback<C_SellerProfileResponse>() {
            @Override
            public void onResponse(Call<C_SellerProfileResponse> call, Response<C_SellerProfileResponse> response) {
                try {
                    if (response.isSuccessful());
                    C_SellerProfileResponse c_sellerProfileResponse=response.body();
                    if (c_sellerProfileResponse.status.equals("1")) {
                        C_SellerProfileData c_sellerProfileData = c_sellerProfileResponse.data;
                        Cid = c_sellerProfileData.sId.toString();
                        apiInterface = igrand.technology.info.eganacsi.ApiClient.getClient1().create(igrand.technology.info.eganacsi.ApiInterface.class);
                        Call<MyProductListResp> call1 = apiInterface.Myproducts(Cid);
                        call1.enqueue(new Callback<MyProductListResp>() {
                            @Override
                            public void onResponse(Call<MyProductListResp> call, Response<MyProductListResp> response) {
                                if (response.isSuccessful());
                                MyProductListResp resp = response.body();
                                if (resp.status ==1){
                                    MyProductData myProductData = resp.data;
                                    List<MyProductsList>  myProductsList = myProductData.popularProduct;
                                    sellerProductAdapter = new MySellerProductAdapter(myProductsList,ctx,PRODUCT_VIEW);
                                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                                    recyclerView.setLayoutManager(layoutManager);
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    recyclerView.setAdapter(sellerProductAdapter);
                                }
                            }

                            @Override
                            public void onFailure(Call<MyProductListResp> call, Throwable t) {

                            }
                        });


                    }else if (c_sellerProfileResponse.status.equals("0")){

                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<C_SellerProfileResponse> call, Throwable t) {

            }
        });


        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            if (type.equals("product")){
//                getProducts(Cid);




            }else {
                getService(Cid);
            }
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
    }

    private void getService(String seller_id) {

        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<MyServiceStatus> call = apiService.getSellerServices(seller_id);
        call.enqueue(new Callback<MyServiceStatus>() {
            @Override
            public void onResponse(Call<MyServiceStatus> call, Response<MyServiceStatus> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                String message = response.body().getMessage();
                if (message.equals("Success")) {
                    setServiceAdapter(response);
                } else {
                    Utility.toastView(ctx, "Please try again");
                }
            }

            @Override
            public void onFailure(Call<MyServiceStatus> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setServiceAdapter(Response<MyServiceStatus> response) {

        List<SellerServices> list = response.body().getData().getSellerServices();
        sellerMyServicesAdapter = new SellerMyServicesAdapter(list,ctx,SERVICE_VIEW);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(sellerMyServicesAdapter);

    }

    private void getProducts(String seller_id) {

        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<MyProductStatus> call = apiService.getSellerProducts(seller_id);
        call.enqueue(new Callback<MyProductStatus>() {
            @Override
            public void onResponse(Call<MyProductStatus> call, Response<MyProductStatus> response) {
                Utility.hideLoader();
                if (response.isSuccessful());
                MyProductStatus status = response.body();
                if (status.getStatus()==1){
//                    setProductAdapter(response);

                }else if (status.getStatus() ==0){
                    Toast.makeText(SellerMyProductActivity.this, "error", Toast.LENGTH_SHORT).show();
                }

//                setProductAdapter(response);
//                int statusCode = response.code();
//                String message = response.body().getMessage();
//                if (message.equals("Success")) {
//
//                } else {
//                    Utility.toastView(ctx, "Please try again");
//                }
            }

            @Override
            public void onFailure(Call<MyProductStatus> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void setProductAdapter(Response<MyProductStatus> response) {
//
//        List<SellerPopularProduct> list = response.body().getData().getPopularProduct();
//        sellerProductAdapter = new MySellerProductAdapter(list,ctx,PRODUCT_VIEW);
//        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        recyclerView.setAdapter(sellerProductAdapter);
//    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_sellerproduct_back);
        add_bt = findViewById(R.id.bt_sellerproduct_add);
        title_tv = findViewById(R.id.tv_sellerproduct_title);
        recyclerView = findViewById(R.id.rv_sellerproduct_recycler);

        Intent intent = getIntent();
        if (!intent.equals(null)){
            String title = intent.getStringExtra("title");
            type = intent.getStringExtra("type");
            title_tv.setText(title);
        }
        back_iv.setOnClickListener(this);
        add_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.bt_sellerproduct_add:
                if (type.equals("product")){
                    startActivity(new Intent(ctx,SellerAddProductActivity.class)
                    .putExtra("type","new"));
                }else {
                    startActivity(new Intent(ctx,SellerAddServiceActivity.class)
                    .putExtra("type","new"));
                }
                break;

            case R.id.iv_sellerproduct_back:
                finish();
                break;
        }
    }

    @Override
    public void OnSellerClicklistener(View view, int position, int Viewtype) {

            switch (view.getId()){

                case R.id.tv_adpsellerproduct_edit:
                    if (Viewtype==PRODUCT_VIEW){
                        Utility.toastView(ctx,position+"product edit");
                    }else {
                        Utility.toastView(ctx,position+"seller edit");
                    }
                    break;

                case R.id.tv_adpsellerproduct_viewmore:
                    if (Viewtype==PRODUCT_VIEW){
                        Utility.toastView(ctx,position+"product view more");
                    }else {
                        Utility.toastView(ctx,position+"seller view more");
                    }
                    break;
            }

    }
}
