package igrand.technology.info.eganacsi.activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.ProductLIstAdapter;
import igrand.technology.info.eganacsi.model.PopularProduct;
import igrand.technology.info.eganacsi.network.NetworkUtils;

/**
 * Created by kamlesh on 4/21/2018.
 */

public class AllProductListActivity extends Fragment implements NetworkUtils.NetworkListner {


    private RecyclerView recyclerView;
    Context context;
    private NetworkUtils networkUtils;
    ArrayList<PopularProduct> mList = new ArrayList<PopularProduct>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        networkUtils = new NetworkUtils(context, this);
        networkUtils.call(Product_Lisr_Req);
        Bundle bundle = getArguments();
        if (bundle != null) {
            mList = (ArrayList<PopularProduct>) bundle.getSerializable("product_list");
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.all_product_list, null);
        initView(view);

        return view;
    }

    private void initView(View view) {

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        ProductLIstAdapter adapter = new ProductLIstAdapter(context, mList);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onResult(String result, int req_code) {

        if (req_code == Product_Lisr_Req) {

        }
    }


}
