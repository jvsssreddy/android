package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 4/23/2018.
 */

public class ViewAll1
{
    private String type;
    private String id;
    private String name;
    private String price;
    private String image;
    private String categoryid;
    private String categoryname;
    private String subcategoryid;
    private String subcategoryname;
    private String subsubcategoryid;
    private String subsubcategoryname;

    public ViewAll1(String type, String id, String name, String price, String image, String categoryid, String categoryname, String subcategoryid, String subcategoryname, String subsubcategoryid, String subsubcategoryname) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.categoryid = categoryid;
        this.categoryname = categoryname;
        this.subcategoryid = subcategoryid;
        this.subcategoryname = subcategoryname;
        this.subsubcategoryid = subsubcategoryid;
        this.subsubcategoryname = subsubcategoryname;
    }

    public String getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(String categoryid) {
        this.categoryid = categoryid;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }

    public String getSubcategoryid() {
        return subcategoryid;
    }

    public void setSubcategoryid(String subcategoryid) {
        this.subcategoryid = subcategoryid;
    }

    public String getSubcategoryname() {
        return subcategoryname;
    }

    public void setSubcategoryname(String subcategoryname) {
        this.subcategoryname = subcategoryname;
    }

    public String getSubsubcategoryid() {
        return subsubcategoryid;
    }

    public void setSubsubcategoryid(String subsubcategoryid) {
        this.subsubcategoryid = subsubcategoryid;
    }

    public String getSubsubcategoryname() {
        return subsubcategoryname;
    }

    public void setSubsubcategoryname(String subsubcategoryname) {
        this.subsubcategoryname = subsubcategoryname;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
