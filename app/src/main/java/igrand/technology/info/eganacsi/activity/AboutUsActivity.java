package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;

public class AboutUsActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;

    TextView description_tv;
    ExpandableListView expandableListView;
    ConnectionDetector connectionDetector;

    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            getData();
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
//        prepareListData();
//        AdapterFAQs listAdapter = new AdapterFAQs(this, listDataHeader, listDataChild);
//        expandableListView.setAdapter(listAdapter);
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx =this;
        back_iv = (ImageView)findViewById(R.id.iv_about_back);
        description_tv = findViewById(R.id.tv_aboutus_description);
        expandableListView = findViewById(R.id.lvexp_aboutus_expandable);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Introduction");
        listDataHeader.add("Values");
        listDataHeader.add("Vision");
        listDataHeader.add("Mission");
        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add(ctx.getString(R.string.answer));

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add(ctx.getString(R.string.answer));

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add(ctx.getString(R.string.answer));

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);
        listDataChild.put(listDataHeader.get(3), comingSoon);
    }

    private void getData() {
        //getting the progressbar
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("loading...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebApi.URL_ABOUT_US,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressDialog.dismiss();
                        setResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
//                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setResponse(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);

            String msg = jsonObject.getString("message");
            if (msg.equals("Success")){

                JSONObject object_data = jsonObject.getJSONObject("data");

                    String title = object_data.getString("page_name");
                    String description = object_data.getString("description");

                    description_tv.setText(description);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
