package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.ViewAllAdapter;
import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 4/23/2018.
 */

@SuppressLint("ValidFragment")
public class SubCategoryFragment extends Fragment implements AdapterView.OnItemClickListener {
    View view;
    Context ctx;
    GridView gridView;
    RecyclerView recyclerView;
    EditText et_single_search;

    String service,id;
    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    ArrayList<ViewAll>viewAllArrayList;

    String data = "";
    JSONArray data_array;
    JSONObject subsubCate_obj;

    public SubCategoryFragment(Context ctx, String service,String id) {
        this.ctx = ctx;
        this.service= service;
        this.id= id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewall,null);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            getSubSubCategory(id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
        return view;
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
        Utility.setTitle(ctx,service);

        viewAllArrayList = new ArrayList<>();
    }

    private void getSubSubCategory(String id) {

        AndroidNetworking.post(WebApi.URL_SUB_CATEGORY)
                .addBodyParameter("subcategory_id", id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        try {
            JSONObject object = new JSONObject(s);
            String message = object.getString("message");
            if (message.equals("Success")){
                data = s;
                data_array = object.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {

                    subsubCate_obj = data_array.getJSONObject(i);

                    String id = subsubCate_obj.getString("subsubcategory_id");
                    String name = subsubCate_obj.getString("subsubcategory_name");
                    String image = subsubCate_obj.getString("subsubcategory_image");

                    viewAllArrayList.add(new ViewAll("",id,name,image));
                }
                ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ctx,viewAllArrayList,3);
                gridView.setAdapter(viewAllAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        et_single_search = view.findViewById(R.id.et_single_search);
        gridView = view.findViewById(R.id.gv_viewall_gridview);
        recyclerView = view.findViewById(R.id.rv_viewall_recyclerview);

        recyclerView.setVisibility(View.GONE);
        gridView.setVisibility(View.VISIBLE);

        View single_seacrh = (View) view.findViewById(R.id.include_viewall_single);
        View double_search = (View) view.findViewById(R.id.include_viewall_double);

        single_seacrh.setVisibility(View.VISIBLE);
        double_search.setVisibility(View.GONE);

        gridView.setOnItemClickListener(this);

        et_single_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFragment searchFragment = new SearchFragment(ctx);
                Utility.setFragment(searchFragment,ctx);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ProductListingFragment u = new  ProductListingFragment(ctx);

        setFragment(u);
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("userData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String idd = viewAllArrayList.get(position).getId();
        String name = viewAllArrayList.get(position).getName();
        editor.putString("subid",idd);
        editor.putString("namee",name);
        editor.commit();


//        SubSubCategoryFragment subSubCategoryFragment = new SubSubCategoryFragment(ctx,viewAllArrayList.get(position).getName(),data,position);
//        setFragment(subSubCategoryFragment);
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

}
