package igrand.technology.info.eganacsi.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.List;

import igrand.technology.info.eganacsi.ApiClient;
import igrand.technology.info.eganacsi.ApiInterface;
import igrand.technology.info.eganacsi.model.C_SellerProfileData;
import igrand.technology.info.eganacsi.model.C_SellerProfileResponse;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamlesh on 4/12/2018.
 */

public class MyAccountActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv,profile_iv;
    LinearLayout change_mobile_ll,change_password_ll,address_book_ll,notification_ll;
    ImageView edit_image,edit_comany;
    TextView name_tv,mobile_tv,email_tv,address_tv,subscription,address_change;
    RelativeLayout hide1,hide2;
    LinearLayout hide3;
    View hide4;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    public static final int PICK_IMAGE_REQUEST = 2;

    TextView company,mobile,mail,address,website;

    ApiInterface apiInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_account);
        address_change=findViewById(R.id.address_change);
        address_change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this,EditProfileActivity.class));
            }
        });
        address=findViewById(R.id.c_address);

        company=findViewById(R.id.comany);
        mobile=findViewById(R.id.mobile);
        mail=findViewById(R.id.mail);
        website=findViewById(R.id.web);
        subscription=findViewById(R.id.subsciptionplan);
        subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyAccountActivity.this,SubscriptionPlan_SellerActivity.class));
            }
        });

        hide1=findViewById(R.id.hide1);
        hide2=findViewById(R.id.relative_profile);
        hide3=findViewById(R.id.linear_profile);
        hide4=findViewById(R.id.view_profile);


        initXml();
        click();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            String user_id = sessionManager.getData(SessionManager.KEY_CID);
            getUserDetails(user_id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }


        SharedPreferences sharedPreferences =getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email","");
        String password = sharedPreferences.getString("password","");

        apiInterface= ApiClient.getClient1().create(ApiInterface.class);
        Call<C_SellerProfileResponse> call=apiInterface.CustomerSellerProfile(email,password);
        call.enqueue(new Callback<C_SellerProfileResponse>() {
            @Override
            public void onResponse(Call<C_SellerProfileResponse> call, Response<C_SellerProfileResponse> response) {
                try {
                    if (response.isSuccessful());
                    C_SellerProfileResponse c_sellerProfileResponse=response.body();
                    if (c_sellerProfileResponse.status.equals("1")) {
                        C_SellerProfileData c_sellerProfileData = c_sellerProfileResponse.data;
                        company.setText(c_sellerProfileData.buisnessName);
                        mobile.setText(c_sellerProfileData.phone);
                        mail.setText(c_sellerProfileData.email);
                        address.setText(c_sellerProfileData.buisnessAddress);
                        website.setText(c_sellerProfileData.websiteAddress);

                    }else if (c_sellerProfileResponse.status.equals("0")){

                        hide1.setVisibility(View.GONE);
                        hide2.setVisibility(View.GONE);
                        hide3.setVisibility(View.GONE);
                        hide4.setVisibility(View.GONE);
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<C_SellerProfileResponse> call, Throwable t) {

            }
        });
    }

    private void getUserDetails(String user_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_PROFILE)
                .addBodyParameter("customer_id", user_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        sessionManager.setData(SessionManager.PROFILE_DATA,response.toString());
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });

    }

    private void setResponse(String s) {

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")){
                JSONObject data_obj = object.getJSONObject("data");
                JSONObject customer_details_object = data_obj.getJSONObject("customer_details");

                String fname = customer_details_object.getString("first_name");
                String lname = customer_details_object.getString("last_name ");
                String email = customer_details_object.getString("email");
                String city = customer_details_object.getString("city");
                String area = customer_details_object.getString("area");
                String address1 = customer_details_object.getString("address1");
                String address2 = customer_details_object.getString("address2");
                String pin = customer_details_object.getString("pin");
                String image = customer_details_object.getString("image");

                setData(fname,lname,email,city,area,address1,address2,pin,image);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setData(String fname, String lname, String email, String city, String area, String address1, String address2, String pin, String image) {

        String mobile = sessionManager.getData(SessionManager.KEY_MOBILE);
        name_tv.setText(fname+" "+lname);
        email_tv.setText(email);
        mobile_tv.setText(mobile);
        if (address1.equals("")){
            address_tv.setText("not available");
        }else {
            address_tv.setText(address1);
        }

        if (!image.equals("http://igrand.info/egns/")){
            Utility.loadImage(ctx,profile_iv,image);
        }
    }

    private void click() {
        edit_image.setOnClickListener(this);
        change_password_ll.setOnClickListener(this);
        change_mobile_ll.setOnClickListener(this);
        edit_comany.setOnClickListener(this);
//        address_book_ll.setOnClickListener(this);
        back_iv.setOnClickListener(this);
//        profile_iv.setOnClickListener(this);
//        notification_ll.setOnClickListener(this);
    }

    private void initXml() {
        ctx = this;
        name_tv = findViewById(R.id.tv_myacc_name);
        mobile_tv = findViewById(R.id.tv_myacc_mobile);
        email_tv = findViewById(R.id.tv_myacc_email);
        address_tv = findViewById(R.id.tv_myaccount_address);
        change_mobile_ll = (LinearLayout)findViewById(R.id.ll_account_chngemobile);
        change_password_ll = (LinearLayout)findViewById(R.id.ll_account_chngepassword);
//        address_book_ll = (LinearLayout)findViewById(R.id.ll_account_myaddressbook);
//        notification_ll = (LinearLayout)findViewById(R.id.ll_myacc_notification);

        back_iv = findViewById(R.id.iv_myaccount_back);
        profile_iv = findViewById(R.id.iv_myacc_profile);
        edit_image = findViewById(R.id.iv_myaccount_edit);
        edit_comany=findViewById(R.id.edit_cdetails);


        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.ll_account_chngemobile:
                startActivity(new Intent(MyAccountActivity.this,ChangeMobileNumberActivity.class));
                break;

            case R.id.ll_account_chngepassword:
                startActivity(new Intent(MyAccountActivity.this,ChangePasswordActivity.class));
                break;

            case R.id.iv_myaccount_back:
                finish();
                break;

            case R.id.iv_myaccount_edit:
                Intent intent = new Intent(MyAccountActivity.this, EditProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.edit_cdetails:
                Intent intent1 = new Intent(MyAccountActivity.this, Edit_Business_Profile.class);
                startActivity(intent1);
                break;
            case R.id.iv_myacc_profile:
                requestStoragePermission();
                break;
        }
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Utility.toastView(ctx,"All permissions are granted!");
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            browseImage();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Utility.toastView(ctx,"Error occurred! ");
//                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void browseImage() {
        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image/*");
        startActivityForResult(openGalleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            Bitmap bmp = null;
            try {
                bmp = Utility.getBitmapFromUri(uri,ctx);
                profile_iv.setImageBitmap(bmp);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }
}
