package igrand.technology.info.eganacsi.seller.response.myservices;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kamlesh on 5/16/2018.
 */

public class MyServiceData
{
    @SerializedName("seller_services")
    @Expose
    private List<SellerServices> sellerServices = null;

    public List<SellerServices> getSellerServices() {
        return sellerServices;
    }

    public void setSellerServices(List<SellerServices> sellerServices) {
        this.sellerServices = sellerServices;
    }
}
