package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.Slider;


/**
 * Created by Admin on 07-Apr-18.
 */
public class HomePagerAdapter extends PagerAdapter{

    ArrayList<Slider> list;
    Context context;

    public HomePagerAdapter(ArrayList<Slider> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater  inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view  = inflater.inflate(R.layout.pager_item,container,false);
        ImageView imageView = view.findViewById(R.id.pager_image);
        Glide.with(context).load(list.get(position).getImage()).into(imageView);
        container.addView(view);
        return view;

    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view ==object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}

