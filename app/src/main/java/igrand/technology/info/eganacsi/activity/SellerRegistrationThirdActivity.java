package igrand.technology.info.eganacsi.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.seller_register.SellerRegistrationBean;
import igrand.technology.info.eganacsi.model.seller_register.Seller_Reg_Reasponse;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 06-May-18.
 */

public class SellerRegistrationThirdActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    TextView amount_tv;
    Button procced_bt;
    String type;

    RadioButton cash_rb,paypal_rb,netbanking_rb;
    private SellerRegistrationBean bean;
    String membership,price,month,credit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_registration_3);
        initView();
        Intent intent = getIntent();
        if (intent != null) {

            type = getIntent().getStringExtra("type");
            if (type.equals("main")){
                membership = intent.getStringExtra(SubscriptionPlanActivity.MEMBERSHIP);
                month = intent.getStringExtra(SubscriptionPlanActivity.MONTH);
                price= intent.getStringExtra(SubscriptionPlanActivity.PRICE);

                amount_tv.setText("Amount to be paid    "+price+"$");

            }else {

                bean = (SellerRegistrationBean) intent.getSerializableExtra(SellarRegisterActivity.SELLER_DETAIL);
                membership = intent.getStringExtra(SubscriptionPlanActivity.MEMBERSHIP);
                month = intent.getStringExtra(SubscriptionPlanActivity.MONTH);
                price= intent.getStringExtra(SubscriptionPlanActivity.PRICE);

                amount_tv.setText("Amount to be paid    "+price+"$");
            }
        }
    }

    private void initView() {
        ctx = this;
        procced_bt = findViewById(R.id.bt_sellreg3_proceed);
        amount_tv = findViewById(R.id.tv_sellreg3_paid);
        cash_rb = findViewById(R.id.rb_sellreg3_cash);
        paypal_rb = findViewById(R.id.rb_sellreg3_paypal);
        netbanking_rb = findViewById(R.id.rb_sellreg3_netbanking);
        back_iv = findViewById(R.id.iv_sellerreg3_back);

        back_iv.setOnClickListener(this);
        procced_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.iv_sellerreg3_back:
                finish();
                break;

            case R.id.bt_sellreg3_proceed:
                boolean check = chackPayment();
                if (check){
                    if (type.equals("main")){
                        dialogPaymentSuccess();
                    }else {
                        getData();
                    }
                }else {
                    Utility.toastView(ctx,"Please select payment method");
                }
                break;
        }
    }

    private boolean chackPayment() {

        if (paypal_rb.isChecked()){
            credit = "Paypal";
            return true;
        }else if (cash_rb.isChecked()){
            credit = "Cash";
            return true;
        }else if (netbanking_rb.isChecked()){
            credit = "Net Banking";
            return true;
        }else {
            return false;
        }
    }

    public void getData() {

        String fname = bean.firstname;
        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<Seller_Reg_Reasponse> call = apiService.registerSeller(
                bean.getFirstname(),
                bean.getLastname(),
                bean.getMobile(),
                bean.getPassword(),
                bean.getEmail(),
                bean.getCity(),
                bean.getArea(),
                bean.getAddressline1(),
                bean.getAddressline2(),
                bean.getPincode(),
                bean.getBuisness_name(),
                bean.getBuisness_address(),
                bean.getBuisness_city(),
                bean.getWebsite_address(),
                membership,month,price,credit);
        call.enqueue(new Callback<Seller_Reg_Reasponse>() {
            @Override
            public void onResponse(Call<Seller_Reg_Reasponse> call, Response<Seller_Reg_Reasponse> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                String msg = response.body().getMessage();
                if (msg.equals("Seller Successfully Register")){

                    String seller_id = response.body().getUserId();
                    SessionManager sessionManager = new SessionManager(ctx);
                    sessionManager.setData(SessionManager.KEY_SELLER_ID,seller_id);
                    dialogPaymentSuccess();
                }else {
                    Utility.toastView(ctx,"Please try again later");
                }
            }

            @Override
            public void onFailure(Call<Seller_Reg_Reasponse> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dialogPaymentSuccess() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.dialog_paymentsuccess);
        Button ok_bt = dialog.findViewById(R.id.bt_dialog_successpayment);

        ok_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx,MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });

        dialog.show();
    }
}
