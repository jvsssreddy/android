package igrand.technology.info.eganacsi.seller.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.api_response.NewCampaignResp;
import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCampaignActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    TextView credit_tv;
    Spinner city_sp, area_sp;
    Button submit_bt;
    EditText name_et, count_et, website_et, text_et;

    String location_data = "";
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_campaign);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getCity();
        }
    }

    private void getCity() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_LOCATION)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        location_data = response.toString();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        ArrayList<String> city_list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONArray array_location = object.getJSONArray("Locations");
                for (int i = 0; i < array_location.length(); i++) {
                    JSONObject data_object = array_location.getJSONObject(i);
                    String city = data_object.getString("city_name");
                    city_list.add(city);
                }
                ArrayAdapter<String> city_adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);
                city_sp.setAdapter(city_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_newcamp_back);
        credit_tv = findViewById(R.id.tv_newcamp_credits);
        city_sp = findViewById(R.id.sp_newcamp_city);
//        area_sp = findViewById(R.id.sp_newcamp_area);
        submit_bt = findViewById(R.id.bt_newcamp_submit);
        name_et = findViewById(R.id.et_newcamp_name);
        count_et = findViewById(R.id.et_newcamp_customer);
        website_et = findViewById(R.id.et_newcamp_website);
        text_et = findViewById(R.id.et_newcamp_text);

        back_iv.setOnClickListener(this);
        submit_bt.setOnClickListener(this);

        city_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getArea(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getArea(int position) {

        ArrayList<String> area_list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(location_data);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONArray array_location = object.getJSONArray("Locations");
                JSONObject data_object = array_location.getJSONObject(position);
                JSONArray area_array = data_object.getJSONArray("area");
                for (int i = 0; i < area_array.length(); i++) {

                    JSONObject area_obj = area_array.getJSONObject(i);
                    String area_name = area_obj.getString("area_name");
                    area_list.add(area_name);
                }
                ArrayAdapter<String> area_adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, area_list);
//                area_sp.setAdapter(area_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_newcamp_back:
                finish();
                break;

            case R.id.bt_newcamp_submit:
                boolean internet = connectionDetector.isConnected();
                if (internet) {
                    String Seller_id = sessionManager.getData(SessionManager.KEY_SELLER_ID);
                    if (!Seller_id.equals("")){
                        getData(Seller_id);
                    }else {
                        Utility.toastView(ctx,"Please first complete seller registratrion");
                    }
                } else {
                    Utility.toastView(ctx, ctx.getString(R.string.no_internet));
                }
                break;
        }
    }

    private void getData(String seller_id) {

        String camp_name = name_et.getText().toString();
        String city = city_sp.getSelectedItem().toString();
//        String area = area_sp.getSelectedItem().toString();
        String cust_count = count_et.getText().toString();
        String text = text_et.getText().toString();
        String url = website_et.getText().toString();

        if (!camp_name.equals("") || !text.equals("") || !cust_count.equals("")) {

            Utility.showLoader(ctx);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            Call<NewCampaignResp> call = apiService.newcampaign(seller_id,
                    city, url, text, "", cust_count);
            call.enqueue(new Callback<NewCampaignResp>() {
                @Override
                public void onResponse(Call<NewCampaignResp> call, Response<NewCampaignResp> response) {
                    Utility.hideLoader();
                    int statusCode = response.code();
                    String message = response.body().getMessage();
                    if (message.equals("Success")) {
                        Utility.toastView(ctx, "successfully created campaign");
                        finish();
                        startActivity(new Intent(ctx, MyCampaignActivity.class));
                    } else {
                        Utility.toastView(ctx, "Please try again");
                    }
                }

                @Override
                public void onFailure(Call<NewCampaignResp> call, Throwable t) {
                    // Log error here since request failed
                    Utility.hideLoader();
                    Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Utility.toastView(ctx, "Please enter all fields");
        }
    }
}
