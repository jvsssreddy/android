package igrand.technology.info.eganacsi.util;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 4/7/2018.
 */

public class Utility {
    static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private static ProgressDialog dialog;

    public static void toastView(Context ctx, String messgae) {
        Toast.makeText(ctx, messgae, Toast.LENGTH_LONG);
    }

    public static void snakView(CoordinatorLayout coordinatorLayout,String msg) {

        Snackbar snackbar = Snackbar.make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static void setTitle(Context ctx, String title) {

        TextView txtView = (TextView) ((Activity) ctx).findViewById(R.id.tv_main_title);
        txtView.setText(title);
    }

    public static boolean checkMailPatteren(String email) {

        if (email.matches(emailPattern)) {
            return true;
        } else {
            return false;
        }
    }

    public static void showLoader(Context ctx) {

        dialog = new ProgressDialog(ctx);
        dialog.setCancelable(false);
        dialog.setMessage("loading..");
        dialog.show();
    }

    public static void hideLoader() {

        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

    public static void setFragment(Fragment fragment,Context ctx) {

        FragmentTransaction fragmentTransaction = ((FragmentActivity)ctx).getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

    public static void loadImage(Context ctx, ImageView imageView, String url){
        Picasso.with(ctx)
                .load(url)
                .into(imageView);
    }

    public static String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static String encodeImage(String path)
    {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try{
            fis = new FileInputStream(imagefile);
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,100,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        //Base64.de
        return encImage;
    }

    public static Bitmap getBitmapFromUri(Uri uri,Context ctx) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                ctx.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static File reduceImagefile(File file){
        try {

            // BitmapFactory options to downsize the image
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            o.inSampleSize = 6;
            // factor of downsizing the image

            FileInputStream inputStream = new FileInputStream(file);
            //Bitmap selectedBitmap = null;
            BitmapFactory.decodeStream(inputStream, null, o);
            inputStream.close();

            // The new size we want to scale to
            final int REQUIRED_SIZE=75;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while(o.outWidth / scale / 2 >= REQUIRED_SIZE &&
                    o.outHeight / scale / 2 >= REQUIRED_SIZE) {
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            inputStream = new FileInputStream(file);

            Bitmap selectedBitmap = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();

            // here i override the original image file
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);

            selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100 , outputStream);

            return file;
        } catch (Exception e) {
            return null;
        }
    }
}
