package igrand.technology.info.eganacsi.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.cast.framework.SessionManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import igrand.technology.info.eganacsi.util.WebApi;


/**
 * Created by kamlesh on 4/21/2018.
 */

public class NetworkUtils {


    Context context;
    URL url;
    int request_code;
    NetworkListner listner;
    private ProgressDialog dialog;
    igrand.technology.info.eganacsi.util.SessionManager sessionManager;

    public NetworkUtils(Context context, NetworkListner listner) {
        this.context = context;
        this.listner = listner;
        sessionManager = new igrand.technology.info.eganacsi.util.SessionManager(context);
    }

    public String call(int request_code) {

        StringBuilder builder = new StringBuilder();
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setMessage("loading..");
        dialog.show();
        try {
            this.request_code = request_code;
            if (request_code == NetworkListner.Home_Data_Req) {
                url = new URL(WebApi.BASE_URL + WebApi.HOME_URL);
            } else if (request_code == NetworkListner.CategoryReq) {
                url = new URL(WebApi.BASE_URL + WebApi.ALL_CATEGORY_URL);
            } else if (request_code == NetworkListner.Product_Lisr_Req){
                url = new URL(WebApi.BASE_URL + WebApi.PRODUCT_LIST_URL);
            }else if (request_code == NetworkListner.Service_List_Req){
                url = new URL(WebApi.BASE_URL + WebApi.Service_LIST_URL);
            }
            new Task().execute();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

//        catch (IOException e) {
//            e.printStackTrace();
//        }
        return builder.toString();

    }

    public interface NetworkListner {

        int Home_Data_Req = 101;
        int CategoryReq = 102;
        int Product_Lisr_Req = 103;
        int Service_List_Req = 104;
        public void onResult(String result, int req_code);

    }

    public String networkCall() {

        StringBuilder builder = new StringBuilder();
        try {

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);
            InputStream stream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

            String l = "";
            while ((l = reader.readLine()) != null) {
                builder.append(l);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();

    }

    class Task extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... strings) {

            return networkCall();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.e("onPostExecute: ", s);
            if (dialog != null){
                dialog.dismiss();
            }
            if (request_code == NetworkListner.Home_Data_Req) {
                sessionManager.setData(igrand.technology.info.eganacsi.util.SessionManager.HOME_API_DATA,s);
            }
            if (request_code == NetworkListner.CategoryReq) {
                sessionManager.setData(igrand.technology.info.eganacsi.util.SessionManager.HOME_API_ALL_CATE,s);
            }
            listner.onResult(s, request_code);
        }
    }
}
