package igrand.technology.info.eganacsi.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.model.Wishlist;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;


public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.ViewHolder> {

    Context ctx;
    ArrayList<Wishlist> wish_list;
    SessionManager sessionManager;
    String cust_id;


    public static final int PRODUCT_VIEW = 1;
    public static final int SERVICE_VIEW = 2;

    public WishlistAdapter(Context ctx, ArrayList<Wishlist> img_list) {
        this.ctx = ctx;
        this.wish_list = img_list;
        sessionManager = new SessionManager(ctx);
        cust_id = sessionManager.getData(SessionManager.KEY_CID);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (viewType == PRODUCT_VIEW) {
            v = inflater.inflate(R.layout.adp_product_wishlist, parent, false);
        } else {
            v = inflater.inflate(R.layout.adp_service_wishlist, parent, false);
        }
        ViewHolder vh = new ViewHolder(v, viewType);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.like_iv.setImageResource(R.drawable.like_red_icon);
        Utility.loadImage(ctx,holder.product_iv,wish_list.get(position).getImage());
        holder.title_tv.setText(wish_list.get(position).getName());
        holder.c_name_tv.setText(wish_list.get(position).getComp_name());
        holder.c_address_tv.setText(wish_list.get(position).getComp_address());

        holder.details_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getItemViewType(position) == PRODUCT_VIEW) {
                    ctx.startActivity(new Intent(ctx, ProductDetailActivity.class)
                            .putExtra("product_id", wish_list.get(position).getP_id()));
                }
                if (getItemViewType(position) == SERVICE_VIEW) {
                    ctx.startActivity(new Intent(ctx, ServiceDetailActivity.class)
                            .putExtra("Service", wish_list.get(position).getP_id()));
                }
            }
        });
        holder.get_quote_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, GetQuoteActivity.class));
            }
        });
        holder.like_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String service_id1 = wish_list.get(position).getP_id();
                String type = wish_list.get(position).getType();
                if (type.equals("product")) {
                    dislikeproductandService(cust_id, service_id1, "", position, "p");
                } else {
                    dislikeproductandService(cust_id, "", service_id1, position, "s");
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return wish_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView product_iv, like_iv;
        TextView title_tv,c_name_tv,c_address_tv;
        LinearLayout get_quote_tv, details_tv;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            product_iv = itemView.findViewById(R.id.iv_adpwish_pro_image);
            like_iv = itemView.findViewById(R.id.wishlist_like);
            title_tv = itemView.findViewById(R.id.tv_adpwish_pro_title);
            get_quote_tv = itemView.findViewById(R.id.get_quote);
            details_tv = itemView.findViewById(R.id.view_detail);
            c_name_tv = itemView.findViewById(R.id.product_man);
            c_address_tv = itemView.findViewById(R.id.product_address);
        }
    }

    @Override
    public int getItemViewType(int position) {

        String type = wish_list.get(position).getType();
        int viewtype = 0;
        if (type.equals("product")) {
            viewtype = PRODUCT_VIEW;
        }
        if (type.equals("service")) {
            viewtype = SERVICE_VIEW;
        }
        return viewtype;
    }

    private void dislikeproductandService(String cust_id, final String product_id1, final String service_id1, final int position, final String type) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_DELETE_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                wish_list.remove(position);
                                notifyDataSetChanged();
                                Utility.toastView(ctx, "Remove to wishlist");
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

}