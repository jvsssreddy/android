package igrand.technology.info.eganacsi.model.product_list;

/**
 * Created by kamlesh on 4/21/2018.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductData {

    @SerializedName("popular_product")
    @Expose
    private List<Product> Product = null;

    public List<Product> getProduct() {
        return Product;
    }

    public void setProduct(List<Product> Product) {
        this.Product = Product;
    }

}