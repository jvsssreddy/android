package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 4/13/2018.
 */

public class Enquiry
{
    private String en_id;
    private String cust_id;
    private String seller_id;
    private String product_id;
    private String product_name;
    private String service_name;
    private String c_name;
    private String c_phone;
    private String c_email;
    private String c_msg;
    private String date;
    private String appointment;
    private String seller_name;


    public Enquiry(String en_id, String cust_id, String seller_id, String product_id, String product_name, String service_name, String c_name, String c_phone, String c_email, String c_msg, String date, String appointment,String seller_name) {
        this.en_id = en_id;
        this.cust_id = cust_id;
        this.seller_id = seller_id;
        this.product_id = product_id;
        this.product_name = product_name;
        this.service_name = service_name;
        this.c_name = c_name;
        this.c_phone = c_phone;
        this.c_email = c_email;
        this.c_msg = c_msg;
        this.date = date;
        this.appointment = appointment;
        this.seller_name=seller_name;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getEn_id() {
        return en_id;
    }

    public void setEn_id(String en_id) {
        this.en_id = en_id;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getC_name() {
        return c_name;
    }

    public void setC_name(String c_name) {
        this.c_name = c_name;
    }

    public String getC_phone() {
        return c_phone;
    }

    public void setC_phone(String c_phone) {
        this.c_phone = c_phone;
    }

    public String getC_email() {
        return c_email;
    }

    public void setC_email(String c_email) {
        this.c_email = c_email;
    }

    public String getC_msg() {
        return c_msg;
    }

    public void setC_msg(String c_msg) {
        this.c_msg = c_msg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }
}
