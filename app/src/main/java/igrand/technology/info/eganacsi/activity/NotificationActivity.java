package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.adapter.NotificationAdapter;
import igrand.technology.info.eganacsi.model.Notification;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;

public class NotificationActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    RecyclerView recyclerView;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    List<Notification> notification_List;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initXml();
        initObj();
        String c_id = sessionManager.getData(SessionManager.KEY_CID);
        boolean internet = connectionDetector.isConnected();
        if (internet){
            getNotification(c_id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
    }

    private void getNotification(String c_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_CHANGE_NOTIFICATION)
                .addBodyParameter("customer_id", "2")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        setResponse("error");
                    }
                });
    }

    private void setResponse(String s) {

        if (!s.equals("error")){
            try {
                JSONObject jsonObject = new JSONObject(s);
                String status = jsonObject.getString("status");
                if (status.equals("1")){
                    JSONObject dat_obj = jsonObject.getJSONObject("data");
                    JSONArray noti_array = dat_obj.getJSONArray("noti_details");

                    for (int i = 0; i < noti_array.length(); i++) {

                        JSONObject noti_object = noti_array.getJSONObject(i);

                        String seller_name = noti_object.getString("seller_name");
                        String msg = noti_object.getString("message");
                        String date = noti_object.getString("date");
                        String link  = noti_object.getString("website");

                        String subdate = date.substring(0,10);
                        notification_List.add(new Notification(seller_name,msg,subdate,link));
                    }
                    setAdapter();
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.something_went));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.something_went));
        }
    }


    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);

        notification_List = new ArrayList<>();
    }

    private void setAdapter() {

        NotificationAdapter mAdapter = new NotificationAdapter(notification_List);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    private void initXml() {
        ctx = this;
        back_iv = (ImageView)findViewById(R.id.iv_notification_back);
        recyclerView= (RecyclerView) findViewById(R.id.rv_notification_recyclerview);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
