package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/27/2018.
 */

public class EnquiryDetailsData {


    @SerializedName("en_id")
    @Expose
    public String enId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("company_name")
    @Expose
    public String companyName;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("appointment")
    @Expose
    public String appointment;
    @SerializedName("description")
    @Expose
    public String description;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("enId", enId).append("productName", productName).append("companyName", companyName).append("name", name).append("email", email).append("phone", phone).append("appointment", appointment).append("description", description).toString();
    }
}
