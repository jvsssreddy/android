package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by administator on 6/23/2018.
 */

public class ProductListData {
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("product_price")
    @Expose
    public String productPrice;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    public String discountPercent;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("subsubcategory_name")
    @Expose
    public String subsubcategoryName;
    @SerializedName("seller_id")
    @Expose
    public String sellerId;
    @SerializedName("item_code")
    @Expose
    public String itemCode;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("seller_name")
    @Expose
    public String sellerName;
    @SerializedName("company_name")
    @Expose
    public String companyName;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("rating")
    @Expose
    public String rating;
    @SerializedName("no_of_person")
    @Expose
    public String noOfPerson;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("type")
    @Expose
    public Integer type;
    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
    @SerializedName("service_image")
    @Expose
    public String serviceImage;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("sunday")
    @Expose
    public String sunday;
    @SerializedName("monday")
    @Expose
    public String monday;
    @SerializedName("tuesday")
    @Expose
    public String tuesday;
    @SerializedName("wednesday")
    @Expose
    public String wednesday;
    @SerializedName("thursday")
    @Expose
    public String thursday;
    @SerializedName("friday")
    @Expose
    public String friday;
    @SerializedName("saturday")
    @Expose
    public String saturday;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("productImage", productImage).append("productPrice", productPrice).append("discountPrice", discountPrice).append("discountPercent", discountPercent).append("categoryId", categoryId).append("categoryName", categoryName).append("subcategoryId", subcategoryId).append("subcategoryName", subcategoryName).append("subsubcategoryId", subsubcategoryId).append("subsubcategoryName", subsubcategoryName).append("sellerId", sellerId).append("itemCode", itemCode).append("sortOrder", sortOrder).append("description", description).append("address", address).append("sellerName", sellerName).append("companyName", companyName).append("phone", phone).append("city", city).append("area", area).append("rating", rating).append("noOfPerson", noOfPerson).append("availability", availability).append("type", type).append("serviceId", serviceId).append("serviceName", serviceName).append("serviceImage", serviceImage).append("price", price).append("sunday", sunday).append("monday", monday).append("tuesday", tuesday).append("wednesday", wednesday).append("thursday", thursday).append("friday", friday).append("saturday", saturday).toString();
    }
}
