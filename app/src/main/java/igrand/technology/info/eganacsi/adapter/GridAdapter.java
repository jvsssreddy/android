package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.CategoryBean;
import igrand.technology.info.eganacsi.model.PopularProduct;
import igrand.technology.info.eganacsi.model.PopularService;
import igrand.technology.info.eganacsi.util.Utility;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.Holder> {

    private final HomeAdapter.OnClickListener listener;
    Context context;
    int mViewType;
    ArrayList list;

    public GridAdapter(Context context, int viewtype, ArrayList list, HomeAdapter.OnClickListener listener) {
        this.context = context;
        this.mViewType = viewtype;
        this.list = list;
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAdapterClick(view, position, mViewType);
            }
        });
        if (mViewType == HomeAdapter.ViewHandler.slider_view) {

            PopularProduct popularProduct = (PopularProduct) list.get(0);
//            Glide.with(context).load(popularProduct.getProductImage()).into(holder.product_image);
            Utility.loadImage(context,holder.product_image,popularProduct.getProductImage());
            holder.product_title.setText(popularProduct.getProductName());

        } else if (mViewType == HomeAdapter.ViewHandler.popular_product) {

            PopularProduct popularProduct = (PopularProduct) list.get(0);
            /*Glide.with(context)
                    .load(popularProduct.getProductImage())
                    .into(holder.product_image)
                    .onLoadFailed(null, context.getDrawable(R.drawable.no_image));*/
            Utility.loadImage(context,holder.product_image,popularProduct.getProductImage());
            holder.product_title.setText(popularProduct.getProductName());
            holder.product_title.setText(popularProduct.getProductName());

        } else if (mViewType == HomeAdapter.ViewHandler.popular_service) {

            PopularService popularService = (PopularService) list.get(0);
//            Glide.with(context).load(popularService.getImage()).into(holder.product_image);
            Utility.loadImage(context,holder.product_image,popularService.getImage());
            holder.product_title.setText(popularService.getServiceName());

        } else if (mViewType == HomeAdapter.ViewHandler.all_category) {

            CategoryBean allCategory = (CategoryBean) list.get(position);
            String image = allCategory.getCategoryImage();
            Glide.with(context).load(image).into(holder.product_image);
//            Utility.loadImage(context,holder.product_image,allCategory.getCategoryImage());
            holder.product_title.setText(allCategory.getCategoryName());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Holder holder;

        if (mViewType == HomeAdapter.ViewHandler.all_category) {

            View view = inflater.inflate(R.layout.item_grid_card_adapter, null);
            holder = new Holder(view);
        } else {

            View view = inflater.inflate(R.layout.item_grid_adapter, null);
            holder = new Holder(view);
        }
        return holder;

    }


    class Holder extends RecyclerView.ViewHolder {

        TextView product_title;
        CircleImageView product_image;

        public Holder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);


        }
    }


}