package igrand.technology.info.eganacsi.seller.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import igrand.technology.info.eganacsi.seller.response.MyLeads.Lead;
import igrand.technology.info.eganacsi.seller.response.myquoatation.MyQuotationEnquiry;
import igrand.technology.info.eganacsi.R;

public class MyleadViewmoreActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    TextView title_tv,id_tv,date_tv,proname_tv,custname_tv,city_tv,area_tv,descr_tv,email_tv,phone_tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mylead_viewmore);
        initXml();
        String type = getIntent().getStringExtra("type");
        if (type.equals("Lead")){
            setLeadData();
        }else {
            setQuoteData();
        }
    }

    private void setLeadData() {
        Lead lead= (Lead) getIntent().getParcelableExtra("data");
        title_tv.setText(lead.getProductName());
        id_tv.setText("Lead ID : "+lead.getEnId());
        date_tv.setText("Date : "+lead.getDatetime());
        proname_tv.setText("Product/Service Name : "+lead.getProductName());
        custname_tv.setText("Customer Name : "+lead.getCName());
        city_tv.setText("City : ");
        area_tv.setText("Area : ");
        descr_tv.setText(lead.getMessage());
        email_tv.setText("E-mail : "+lead.getCEmail());
        phone_tv.setText("Phone  : "+lead.getCPhone());

    }

    private void setQuoteData() {
        MyQuotationEnquiry enquiry = (MyQuotationEnquiry) getIntent().getParcelableExtra("data");

        String pro_name = enquiry.getProductName();
        String ser_name = enquiry.getServiceName();
        if (pro_name.equals("")){
            title_tv.setText(""+enquiry.getServiceName());
            proname_tv.setText("Product/Service Name : "+enquiry.getServiceName());
        }else {
            title_tv.setText(""+enquiry.getProductName());
            proname_tv.setText("Product/Service Name : "+enquiry.getProductName());
        }
        id_tv.setText("Lead ID : "+enquiry.getEnId());
        date_tv.setText("Date : "+enquiry.getDatetime());
        custname_tv.setText("Customer Name : "+enquiry.getCName());
        city_tv.setText("City : ");
        area_tv.setText("Area : ");
        descr_tv.setText(enquiry.getMessage());
        email_tv.setText("E-mail : "+enquiry.getCEmail());
        phone_tv.setText("Phone  : "+enquiry.getCPhone());
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_myleadviewmore_back);
        title_tv = findViewById(R.id.tv_myleadviewmore_title);
        id_tv = findViewById(R.id.tv_myleadviewmore_id);
        date_tv = findViewById(R.id.tv_myleadviewmore_date);
        proname_tv = findViewById(R.id.tv_myleadviewmore_productname);
        custname_tv = findViewById(R.id.tv_myleadviewmore_custname);
        city_tv = findViewById(R.id.tv_myleadviewmore_city);
        area_tv = findViewById(R.id.tv_myleadviewmore_area);
        descr_tv = findViewById(R.id.tv_myleadviewmore_description);
        email_tv = findViewById(R.id.tv_myleadviewmore_email);
        phone_tv = findViewById(R.id.tv_myleadviewmore_phone);

        Intent in = getIntent();
        if (!in.equals(null)){
            String title = in.getStringExtra("title");
            title_tv.setText(title);
        }

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
