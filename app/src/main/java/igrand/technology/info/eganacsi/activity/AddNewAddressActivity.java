package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

public class AddNewAddressActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    EditText title_et,fname_et,lname_et,mobile_et,houseno_et,residence_complex_et,
            area_et,pincode_et,street_et,landmark_et;
    CheckBox default_cb;
    Button save_bt;
    ImageView back_iv;

    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        initXml();
        initObj();
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();

        save_bt.setOnClickListener(this);
        back_iv.setOnClickListener(this);
    }

    private void initXml() {
        ctx = this;
        title_et = findViewById(R.id.et_newaddress_title);
        fname_et = findViewById(R.id.et_newaddress_fname);
        lname_et = findViewById(R.id.et_newaddress_lname);
        mobile_et = findViewById(R.id.et_newaddress_mobile);
        houseno_et = findViewById(R.id.et_newaddress_houseno);
        residence_complex_et = findViewById(R.id.et_newaddress_resident_complex);
        area_et = findViewById(R.id.et_newaddress_area);
        pincode_et = findViewById(R.id.et_newaddress_pincode);
        street_et = findViewById(R.id.et_newaddress_streetname);
        landmark_et = findViewById(R.id.et_newaddress_landmark);
        default_cb = findViewById(R.id.cb_newaddress_bydefault);
        save_bt = findViewById(R.id.bt_newaddress_save);
        back_iv = findViewById(R.id.iv_newaddres_back);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.iv_newaddres_back:
                finish();
                break;

            case R.id.bt_newaddress_save:
                boolean internet = connectionDetector.isConnected();
                if (internet){
                    getData();
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.no_internet));
                }
                break;
        }
    }

    private void getData() {

        String title = title_et.getText().toString();
        String fname = fname_et.getText().toString();
        String  lname =lname_et.getText().toString();
        String  mobile = mobile_et.getText().toString();
        String  houseno = houseno_et.getText().toString();
        String residence_com = residence_complex_et.getText().toString();
        String area = area_et.getText().toString();
        String  pincode = pincode_et.getText().toString();
        String  street = street_et.getText().toString();
        String landmark = landmark_et.getText().toString();

        if (title.equals("")){
            Utility.toastView(ctx,"Please enter Address nickname");
        }else if (fname.equals("")){
            Utility.toastView(ctx,"Enter first name");
        }else if (lname.equals("")){
            Utility.toastView(ctx,"Enter last name");
        }else if (mobile.equals("")){
            Utility.toastView(ctx,"Enter mobile number");
        }else if (houseno.equals("")){
            Utility.toastView(ctx,"Enter House number");
        }else if (area.equals("")){
            Utility.toastView(ctx,"Enter area");
        }else if (pincode.equals("")){
            Utility.toastView(ctx,"Enter pincode");
        }else {
            addAddress(title,fname,lname,mobile,houseno,residence_com,area,pincode,street,landmark);
        }
    }

    private void addAddress(String title, String fname, String lname, String mobile, String houseno, String residence_com, String area, String pincode, String street, String landmark) {

        SessionManager sessionManager = new SessionManager(ctx);
        String user_id = sessionManager.getData(SessionManager.KEY_CID);

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADD_NEW_ADDRESS)
                .addBodyParameter("title", title)
                .addBodyParameter("firstname", fname)
                .addBodyParameter("lastname", lname)
                .addBodyParameter("email", "")
                .addBodyParameter("address", area)
                .addBodyParameter("city", "")
                .addBodyParameter("state", "")
                .addBodyParameter("country", "")
                .addBodyParameter("phone", mobile)
                .addBodyParameter("comments", "")
                .addBodyParameter("customer_id", user_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")){
                Utility.toastView(ctx,"Successfully add ");
                finish();
            }else {
                Utility.toastView(ctx,"Something went wrong please try again");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
