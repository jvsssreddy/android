package igrand.technology.info.eganacsi.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.seller.activity.SellerAddServiceActivity;
import igrand.technology.info.eganacsi.seller.response.myservices.SellerServices;

/**
 * Created by kamlesh on 5/14/2018.
 */

public class SellerMyServicesAdapter extends RecyclerView.Adapter<SellerMyServicesAdapter.ViewHolder> {
    List<SellerServices> servicesList;
    Context ctx;
    int viewtype;
    private OnClickListener listener;

    public SellerMyServicesAdapter(List<SellerServices> servicesList, Context ctx, int viewtype) {
        this.servicesList = servicesList;
        this.ctx = ctx;
        this.listener= listener;
        this.viewtype= viewtype;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_sellerproduct, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.decr_tv.setText(servicesList.get(position).getServiceName());
        Glide.with(ctx).load(servicesList.get(position).getImage())
                .into(holder.product_image)
                .onLoadFailed(null, ctx.getDrawable(R.drawable.no_image));

        holder.edit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SellerServices sellerServices = servicesList.get(position);
                ctx.startActivity(new Intent(ctx, SellerAddServiceActivity.class)
                .putExtra("type","update")
                .putExtra("data",sellerServices));
            }
        });
        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, ServiceDetailActivity.class)
                .putExtra("Service",servicesList.get(position).getServiceId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return servicesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView decr_tv, edit_tv, viewmore_tv;
        ImageView product_image;


        public ViewHolder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.iv_adpsellerproduct_image);
            decr_tv = itemView.findViewById(R.id.tv_adpsellerproduct_descr);
            edit_tv = itemView.findViewById(R.id.tv_adpsellerproduct_edit);
            viewmore_tv = itemView.findViewById(R.id.tv_adpsellerproduct_viewmore);
        }
    }

    public interface OnClickListener{
        void OnSellerClicklistener(View view, int position, int Viewtype);
    }
}
