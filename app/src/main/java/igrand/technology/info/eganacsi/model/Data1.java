package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by administator on 6/28/2018.
 */

public class Data1 {


    @SerializedName("slider")
    @Expose
    public List<Slider1> slider = null;
    @SerializedName("popular_product")
    @Expose
    public List<PopularProduct1> popularProduct = null;
    @SerializedName("popular_service")
    @Expose
    public List<PopularService1> popularService = null;
    @SerializedName("featured_product")
    @Expose
    public List<FeaturedProduct1> featuredProduct = null;
    @SerializedName("featured_service")
    @Expose
    public List<FeaturedService1> featuredService = null;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("slider", slider).append("popularProduct", popularProduct).append("popularService", popularService).append("featuredProduct", featuredProduct).append("featuredService", featuredService).toString();
    }


}
