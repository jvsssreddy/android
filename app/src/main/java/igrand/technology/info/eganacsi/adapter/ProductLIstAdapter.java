package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.PopularProduct;

/**
 * Created by kamlesh on 4/21/2018.
 */

public class ProductLIstAdapter extends RecyclerView.Adapter<ProductLIstAdapter.Holder> {


    Context context;
    ArrayList<PopularProduct> mList;

    public ProductLIstAdapter(Context context, ArrayList<PopularProduct> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_product_wishlist, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        PopularProduct model = mList.get(position);
        Glide.with(context).load(model.getProductImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
        holder.product_title.setText(model.getProductName());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        ImageView product_image;
        TextView product_title;
        TextView product_man;
        TextView product_address;
        ImageView like_iamge;
        TextView get_quote;
        TextView view_detail;

        public Holder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);
            product_man = itemView.findViewById(R.id.product_man);
            product_address = itemView.findViewById(R.id.product_address);
//            like_iamge = itemView.findViewById(R.id.like_iamge);
//            get_quote = itemView.findViewById(R.id.get_quote);
//            view_detail = itemView.findViewById(R.id.view_detail);
        }
    }
}
