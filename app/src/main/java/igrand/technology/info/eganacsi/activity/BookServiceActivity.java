package igrand.technology.info.eganacsi.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.BreakIterator;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;

public class BookServiceActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Context ctx;
    ImageView back_iv;
    TextView title_tv,tv_bookservice_date;
    static TextView bookservice_time;
    Spinner sel_cate_sp, sel_subcate_sp, sel_subsubcate_sp, sel_service_sp;
    EditText name_et, phone_et, email_et, msg_et;
    Button submit_bt;

    String data, subdata,sel_product_id="";
    ConnectionDetector connectionDetector;
    ArrayList<String> subcate_idlist, product_idlist;
    static final int DATE_DIALOG_ID = 0;
    private int mYear,mMonth,mDay;
    @SuppressWarnings("deprecation")
    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_service);
        initXml();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getCategoryData();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void getCategoryData() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_GET_QUOTE_DATA_CATE)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        data = response.toString();
                        setCateResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setCateResponse(String s) {

        ArrayList<String> cate_list = new ArrayList<>();
        cate_list.add("Select Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String cate_name = cate_obj.getString("category_name");
                    cate_list.add(cate_name);
                }
                ArrayAdapter<String> cate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cate_list);
                sel_cate_sp.setAdapter(cate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSpinner() {

        ArrayList<String> service_list = new ArrayList<>();

        service_list.add("Laundry");
        service_list.add("painter");
        service_list.add("Plumber");

        ArrayAdapter<String> service_adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, service_list);
        sel_service_sp.setAdapter(service_adapter);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_bookservice_back);
        title_tv = findViewById(R.id.tv_bookservice_title);
        sel_cate_sp = findViewById(R.id.sp_bookservice_selcate);
        sel_subcate_sp = findViewById(R.id.sp_bookservice_selsubcate);
        sel_subsubcate_sp = findViewById(R.id.sp_bookservice_selsubsubcate);
        sel_service_sp = findViewById(R.id.sp_bookservice_selservice);
        name_et = findViewById(R.id.et_bookservice_name);
        phone_et = findViewById(R.id.et_bookservice_phone);
        email_et = findViewById(R.id.et_bookservice_email);
        msg_et = findViewById(R.id.et_bookservice_message);
        submit_bt = findViewById(R.id.bt_bookservice_submit);

        subcate_idlist = new ArrayList<>();
        connectionDetector = new ConnectionDetector();

        back_iv.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }));


        submit_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getData();
            }
        });

        bookservice_time=findViewById(R.id.bookservice_time);
        bookservice_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTruitonTimePickerDialog(v);
            }
        });

        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        tv_bookservice_date=findViewById(R.id.tv_bookservice_date);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        tv_bookservice_date.setText(sdf.format(c.getTime()));
        tv_bookservice_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showDialog(DATE_DIALOG_ID);
            }
        });

        sel_cate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                setSubCateSP(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sel_subcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position!=0){
                    int pos = position - 1;
                    getsubSubCate(pos);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sel_subsubcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                setproductSp(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sel_service_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position!=0){
                    int pos = position-1;
                    sel_product_id = product_idlist.get(pos);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setproductSp(int pos) {

        product_idlist = new ArrayList<>();
        ArrayList<String> product_list = new ArrayList<>();
        product_list.add("Select Product");
        try {
            JSONObject jsonObject = new JSONObject(subdata);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                JSONObject cate_obj = data_array.getJSONObject(pos);
                JSONArray prod_array = cate_obj.getJSONArray("sub_category");
                for (int i = 0; i < prod_array.length(); i++) {
                    JSONObject pro_object = prod_array.getJSONObject(i);
                    String product_name = pro_object.getString("product_name");
                    String product_id = pro_object.getString("product_id");
                    product_idlist.add(product_id);
                    product_list.add(product_name);
                }
                ArrayAdapter<String> subsubcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, product_list);
                sel_service_sp.setAdapter(subsubcate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void getsubSubCate(int pos) {

        String id = subcate_idlist.get(pos);
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_QUOTE_DATA_SUBSUBCATE)
                .addBodyParameter("subcategory_id", id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        subdata = response.toString();
                        setsubsubCateResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setsubsubCateResponse(String s) {

        ArrayList<String> subsubcate_list = new ArrayList<>();
        subsubcate_list.add("Select sub sub Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String subsubcate_name = cate_obj.getString("subsubcategory_name");
                    subsubcate_list.add(subsubcate_name);
                }
                ArrayAdapter<String> subsubcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subsubcate_list);
                sel_subsubcate_sp.setAdapter(subsubcate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getData() {

        String category = sel_cate_sp.getSelectedItem().toString();
        String subcategory = sel_cate_sp.getSelectedItem().toString();
        String subsubcategory = sel_cate_sp.getSelectedItem().toString();
        String service = sel_cate_sp.getSelectedItem().toString();

        String name = name_et.getText().toString();
        String email = email_et.getText().toString();
        String phone = phone_et.getText().toString();
        String msg = msg_et.getText().toString();
        String date = msg_et.getText().toString();
        String time = msg_et.getText().toString();

        if (name.equals("") || email.equals("") || phone.equals("")) {
            Utility.toastView(ctx, "Enter all fields");
        } else if (!Utility.checkMailPatteren(email)) {
            Utility.toastView(ctx, "Invalid E-mail");
        } else {
            getSubmit(category, subcategory, subsubcategory, service, name, email, phone, msg,date);
        }
    }

    private void getSubmit(String category, String subcategory, String subsubcategory, String service, String name, String email, String phone, String msg, String date) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_QUOTE)
                .addBodyParameter("customer_id", email)
                .addBodyParameter("product_id", sel_product_id)
                .addBodyParameter("service_id", "3")
                .addBodyParameter("customer_fname", name)
                .addBodyParameter("customer_lname", "")
                .addBodyParameter("customer_phone", phone)
                .addBodyParameter("customer_email", email)
                .addBodyParameter("message", msg)
                .addBodyParameter("appointment",date)
                .addBodyParameter("product_name", "demo")
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);

            String status = object.getString("status");
            if (status.equals("1")) {
                Utility.toastView(ctx, "Successfully done");
                finish();
            } else {
                Utility.toastView(ctx, "Please try again");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSubCateSP(int pos) {

        subcate_idlist = new ArrayList<>();
        ArrayList<String> subcate_list = new ArrayList<>();
        subcate_list.add("Select sub category");
        try {
            JSONObject jsonObject = new JSONObject(data);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                JSONObject cate_obj = data_array.getJSONObject(pos);

                JSONArray subcate_array = cate_obj.getJSONArray("sub_category");
                for (int i = 0; i < subcate_array.length(); i++) {
                    JSONObject sub_object = subcate_array.getJSONObject(i);

                    String sub_category_name = sub_object.getString("sub_category_name");
                    String sub_category_id = sub_object.getString("sub_category_id");
                    subcate_list.add(sub_category_name);
                    subcate_idlist.add(sub_category_id);
                }
                ArrayAdapter<String> subcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subcate_list);
                sel_subcate_sp.setAdapter(subcate_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DATE_DIALOG_ID:
                return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth, mDay);
        }
        return null;
    }
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            mYear = year;
            mMonth = monthOfYear;
            mDay = dayOfMonth;
            tv_bookservice_date.setText(new StringBuilder().append(mDay).append("/").append(mMonth+1).append("/").append(mYear));
        }
    };
    public void showTruitonTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

        @Override
       public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
         final Calendar c = Calendar.getInstance();
           int hour = c.get(Calendar.HOUR_OF_DAY);int minute = c.get(Calendar.MINUTE);
           // Create a new instance of TimePickerDialog and return it
           return new TimePickerDialog(getActivity(), this, hour, minute,
                   DateFormat.is24HourFormat(getActivity()));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            bookservice_time.setText(hourOfDay + ":" + minute);
        }
//        @Override
//        public Dialog onCreateDialog(Bundle savedInstanceState) {
//            // Use the current time as the default values for the picker
//            final Calendar c = Calendar.getInstance();
//            int hour = c.get(Calendar.HOUR_OF_DAY);
//            int minute = c.get(Calendar.MINUTE);
//
//            // Create a new instance of TimePickerDialog and return it
//            return new TimePickerDialog(getActivity(), this, hour, minute,
//                    DateFormat.is24HourFormat(getActivity()));
//        }
//
//        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            // Do something with the time chosen by the user
//            editText1.setText(hourOfDay + ":" + minute);
//        }
    }
}
