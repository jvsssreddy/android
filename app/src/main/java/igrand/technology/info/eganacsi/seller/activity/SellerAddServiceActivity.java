package igrand.technology.info.eganacsi.seller.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.seller.UploadImageInterface;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.seller.response.AddService;
import igrand.technology.info.eganacsi.seller.response.UpdateResponse;
import igrand.technology.info.eganacsi.seller.response.myservices.SellerServices;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.R;


public class SellerAddServiceActivity extends AppCompatActivity implements View.OnClickListener{

    Context ctx;
    ImageView back_iv;
    Spinner cate_sp, subcate_sp, subsubcate_sp, status_sp;
    EditText pname_et, mprice_et, oprice_et, percent_et, desc_et;
    RadioButton mon_rb, tue_rb, wed_rb, thu_rb, fri_rb, sat_rb, sun_rb;
    Button submit_bt;
    TextView browse_tv,title_tv,filename_tv;

    ArrayList<String> cate_list, cate_idlist, subcate_idlist, subsubcate_list, subsubcate_idlist, status_list;
    private String data, subdata;
    String v_cate_id, v_subcate_id, v_subsubcate_id="not", v_status, v_pname, v_mprice,
            v_oprice, v_percent, v_desc, v_seller_id,filename="";
    String v_service_id;

    String v_mon = "0", v_tue = "0", v_wed = "0", v_thu = "0", v_fri = "0", v_sat = "0", v_sun = "0";

    File file;
    String TAG = "Seller Image Upload";
    String type;

    public static final int PICK_IMAGE_REQUEST = 1;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_add_service);
        initXml();
        initObj();
        checkType();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            setStatus();
            getCategoryData();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void checkType() {
        type = getIntent().getStringExtra("type");
        if (type.equals("update")) {
            SellerServices sellerServices = (SellerServices) getIntent().getParcelableExtra("data");
            if (sellerServices != null) {
                title_tv.setText("Seller My Service Edit Service");
                submit_bt.setText("Update");
                v_service_id = sellerServices.getServiceId();
                desc_et.setText(sellerServices.getDescription());
                pname_et.setText(sellerServices.getServiceName());
                mprice_et.setText(sellerServices.getServicePrice());
                oprice_et.setText(sellerServices.getDiscountPrice());
                percent_et.setText(sellerServices.getDiscountPercent());
                String mon = sellerServices.getMonday();
                if (mon.equals("1")) {
                    mon_rb.setChecked(true);
                }
                String tues = sellerServices.getTuesday();
                if (tues.equals("1")) {
                    tue_rb.setChecked(true);
                }
                String wed = sellerServices.getWednesday();
                if (wed.equals("1")) {
                    wed_rb.setChecked(true);
                }
                String thurs= sellerServices.getThursday();
                if (thurs.equals("1")) {
                    thu_rb.setChecked(true);
                }
                String fri = sellerServices.getFriday();
                if (fri.equals("1")) {
                    fri_rb.setChecked(true);
                }
                String sat = sellerServices.getSaturday();
                if (sat.equals("1")) {
                    sat_rb.setChecked(true);
                }
                String sun = sellerServices.getSunday();
                if (sun.equals("1")) {
                    sun_rb.setChecked(true);
                }
            }
        }
    }

    private void setStatus() {

        status_list = new ArrayList<>();
        status_list.add("Active");
        status_list.add("InActive");
        ArrayAdapter<String> status_adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, status_list);
        status_sp.setAdapter(status_adapter);
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_selladdserv_back);
        cate_sp = findViewById(R.id.sp_selladdserv_category);
        subcate_sp = findViewById(R.id.sp_selladdserv_subcategory);
        subsubcate_sp = findViewById(R.id.sp_selladdserv_subsubcategory);
        status_sp = findViewById(R.id.sp_selladdserv_status);
        pname_et = findViewById(R.id.et_selladdserv_pname);
        mprice_et = findViewById(R.id.et_selladdserv_mprice);
        oprice_et = findViewById(R.id.et_selladdserv_oprice);
        percent_et = findViewById(R.id.et_selladdserv_percent);
        desc_et = findViewById(R.id.et_selladdserv_description);
        mon_rb = findViewById(R.id.rb_selladdserv_mon);
        tue_rb = findViewById(R.id.rb_selladdserv_tue);
        wed_rb = findViewById(R.id.rb_selladdserv_wen);
        thu_rb = findViewById(R.id.rb_selladdserv_thu);
        fri_rb = findViewById(R.id.rb_selladdserv_fri);
        sat_rb = findViewById(R.id.rb_selladdserv_sat);
        sun_rb = findViewById(R.id.rb_selladdserv_sun);
        submit_bt = findViewById(R.id.bt_selladdserv_submit);
        browse_tv = findViewById(R.id.tv_selladdserv_browse);
        filename_tv = findViewById(R.id.tv_selladdserv_filename);
        title_tv = findViewById(R.id.tv_selladdserv_title);

//        mon_rb.setOnClickListener(this);
//        tue_rb.setOnClickListener(this);
//        wed_rb.setOnClickListener(this);
//        thu_rb.setOnClickListener(this);
//        fri_rb.setOnClickListener(this);
//        sat_rb.setOnClickListener(this);
//        sun_rb.setOnClickListener(this);

        cate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                setSubCateSP(pos);
                if (pos != -1) {
                    v_cate_id = cate_idlist.get(pos);
//                    Utility.toastView(ctx, v_cate_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    int pos = position - 1;
                    getsubSubCate(pos);
                    if (pos != -1) {
                        v_subcate_id = subcate_idlist.get(pos);
//                        Utility.toastView(ctx, v_subcate_id);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subsubcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                if (pos != -1) {
                    v_subsubcate_id = subsubcate_idlist.get(pos);
//                    Utility.toastView(ctx, v_subsubcate_id);
                } else {
                    v_subsubcate_id = "not";
//                    Utility.toastView(ctx, v_subsubcate_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                v_subsubcate_id = "not";
                Utility.toastView(ctx, v_subsubcate_id);
            }
        });

        back_iv.setOnClickListener(this);
        browse_tv.setOnClickListener(this);
        submit_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_selladdserv_back:
                finish();
                break;
            case R.id.bt_selladdserv_submit:
                boolean internet = connectionDetector.isConnected();
                if (internet) {
                    if (type.equals("update")){
                        submitData();
                    }else {
                        submitData();
                    }
                } else {
                    Utility.toastView(ctx, ctx.getString(R.string.no_internet));
                }
                break;

            case R.id.tv_selladdserv_browse:
//                browseImage();
                requestStoragePermission();
                break;

            case R.id.rb_selladdserv_sun:
                boolean check = sun_rb.isChecked();
                if (check) {
                    sun_rb.setChecked(false);
                }else {
                    sun_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_mon:
                if (mon_rb.isChecked()) {
                    mon_rb.setChecked(false);
                }else {
                    mon_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_tue:
                if (tue_rb.isChecked()) {
                    tue_rb.setChecked(false);
                }else {
                    tue_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_wen:
                if (wed_rb.isChecked()) {
                    wed_rb.setChecked(false);
                }else {
                    wed_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_thu:
                if (thu_rb.isChecked()) {
                    thu_rb.setChecked(false);
                }else {
                    thu_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_fri:
                if (fri_rb.isChecked()) {
                    fri_rb.setChecked(false);
                }else {
                    fri_rb.setChecked(true);
                }
                break;

            case R.id.rb_selladdserv_sat:
                if (sat_rb.isChecked()) {
                    sat_rb.setChecked(false);
                }else {
                    sat_rb.setChecked(true);
                }
                break;
        }
    }

    private void updateData() {

    }

    private void submitData() {

//        v_seller_id = sessionManager.getData(SessionManager.KEY_SELLER_ID);
        v_seller_id = "2";
        v_status = status_sp.getSelectedItem().toString();
        v_pname = pname_et.getText().toString();
        v_mprice = mprice_et.getText().toString();
        v_oprice = oprice_et.getText().toString();
        v_percent = percent_et.getText().toString();
        v_desc = desc_et.getText().toString();

        if (mon_rb.isChecked()) {
            v_mon = "1";
        }
        if (tue_rb.isChecked()) {
            v_tue = "1";
        }
        if (wed_rb.isChecked()) {
            v_wed = "1";
        }
        if (thu_rb.isChecked()) {
            v_thu = "1";
        }
        if (fri_rb.isChecked()) {
            v_fri = "1";
        }
        if (sat_rb.isChecked()) {
            v_sat = "1";
        }
        if (sun_rb.isChecked()) {
            v_sun = "1";
        }

        if (v_pname.equals("") || v_mprice.equals("") || v_oprice.equals("") || v_percent.equals("") || v_desc.equals("")) {
            Utility.toastView(ctx, "Enter All Filed");
        } else if (v_subsubcate_id.equals("not")) {
            Utility.toastView(ctx, "Selact All Service Category");
        }else if (filename.equals("")){
            Utility.toastView(ctx,"Please select file");
        } else {
            if (type.equals("update")){
                updateApi();
            }else {
                callApi();
            }
        }


    }

    private void callApi() {
        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("uploading...");
        pd.show();

        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody seller_id = RequestBody.create(MediaType.parse("text/plain"), v_seller_id);
        RequestBody cate_id = RequestBody.create(MediaType.parse("text/plain"), v_cate_id);
        RequestBody subcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subcate_id);
        RequestBody subsubcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subsubcate_id);
        RequestBody service_name = RequestBody.create(MediaType.parse("text/plain"), v_pname);
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), v_mprice);
        RequestBody d_price = RequestBody.create(MediaType.parse("text/plain"), v_oprice);
        RequestBody d_percent = RequestBody.create(MediaType.parse("text/plain"), v_percent);
        RequestBody sun = RequestBody.create(MediaType.parse("text/plain"), v_sun);
        RequestBody mon = RequestBody.create(MediaType.parse("text/plain"), v_mon);
        RequestBody tues = RequestBody.create(MediaType.parse("text/plain"), v_tue);
        RequestBody wedn = RequestBody.create(MediaType.parse("text/plain"), v_wed);
        RequestBody thurs = RequestBody.create(MediaType.parse("text/plain"), v_thu);
        RequestBody fri = RequestBody.create(MediaType.parse("text/plain"), v_fri);
        RequestBody sat = RequestBody.create(MediaType.parse("text/plain"), v_sat);
        RequestBody descr = RequestBody.create(MediaType.parse("text/plain"), v_desc);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), v_status);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
        Call<AddService> fileUpload = uploadImage.uploadServicesFile(fileToUpload,
                seller_id, cate_id, subcate_id, subsubcate_id, service_name
                , price, d_price, d_percent, sun, mon, tues, wedn, thurs, fri, sat, descr, status);
        fileUpload.enqueue(new Callback<AddService>() {
            @Override
            public void onResponse(Call<AddService> call, Response<AddService> response) {
                pd.dismiss();
                Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_LONG).show();
                clear();
                finish();
            }

            @Override
            public void onFailure(Call<AddService> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG, "Error " + t.getMessage());
                Utility.toastView(ctx, t.getMessage());
            }
        });
    }

    private void updateApi() {
        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("updating...");
        pd.show();

        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody seller_id = RequestBody.create(MediaType.parse("text/plain"), v_seller_id);
        RequestBody cate_id = RequestBody.create(MediaType.parse("text/plain"), v_cate_id);
        RequestBody subcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subcate_id);
        RequestBody subsubcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subsubcate_id);
        RequestBody service_name = RequestBody.create(MediaType.parse("text/plain"), v_pname);
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), v_mprice);
        RequestBody d_price = RequestBody.create(MediaType.parse("text/plain"), v_oprice);
        RequestBody d_percent = RequestBody.create(MediaType.parse("text/plain"), v_percent);
        RequestBody sun = RequestBody.create(MediaType.parse("text/plain"), v_sun);
        RequestBody mon = RequestBody.create(MediaType.parse("text/plain"), v_mon);
        RequestBody tues = RequestBody.create(MediaType.parse("text/plain"), v_tue);
        RequestBody wedn = RequestBody.create(MediaType.parse("text/plain"), v_wed);
        RequestBody thurs = RequestBody.create(MediaType.parse("text/plain"), v_thu);
        RequestBody fri = RequestBody.create(MediaType.parse("text/plain"), v_fri);
        RequestBody sat = RequestBody.create(MediaType.parse("text/plain"), v_sat);
        RequestBody descr = RequestBody.create(MediaType.parse("text/plain"), v_desc);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), v_status);
        RequestBody service_id = RequestBody.create(MediaType.parse("text/plain"), v_service_id);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
        Call<UpdateResponse> fileUpload = uploadImage.upDateServiceDetails(fileToUpload,
                seller_id, cate_id, subcate_id, subsubcate_id, service_name
                , price, d_price, d_percent, sun, mon, tues, wedn, thurs, fri, sat, descr, status,service_id);
        fileUpload.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                pd.dismiss();
                Toast.makeText(ctx, "Success " + response.body().getMessage(), Toast.LENGTH_LONG).show();
                clear();
                finish();
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG, "Error " + t.getMessage());
                Utility.toastView(ctx, t.getMessage());
            }
        });
    }

    private void clear() {
        pname_et.setText("");
        mprice_et.setText("");
        oprice_et.setText("");
        percent_et.setText("");
        desc_et.setText("");
    }

    private void browseImage() {
        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image/*");
        startActivityForResult(openGalleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, this);
            file = new File(filePath);
            filename = file.getName();
            Log.d(TAG, "Filename " + file.getName());
            filename_tv.setText(file.getName());
            //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void getCategoryData() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_GET_QUOTE_DATA_CATE)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        data = response.toString();
                        setCateResponse(response.toString());
                    }


                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setCateResponse(String s) {

        cate_idlist = new ArrayList<>();
        cate_list = new ArrayList<>();
        cate_list.add("Select Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String cate_name = cate_obj.getString("category_name");
                    String cate_id = cate_obj.getString("category_id");
                    cate_list.add(cate_name);
                    cate_idlist.add(cate_id);
                }
                ArrayAdapter<String> cate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cate_list);
                cate_sp.setAdapter(cate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSubCateSP(int pos) {

        subcate_idlist = new ArrayList<>();
        ArrayList<String> subcate_list = new ArrayList<>();
        subcate_list.add("Select sub category");
        try {
            JSONObject jsonObject = new JSONObject(data);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                JSONObject cate_obj = data_array.getJSONObject(pos);

                JSONArray subcate_array = cate_obj.getJSONArray("sub_category");
                for (int i = 0; i < subcate_array.length(); i++) {
                    JSONObject sub_object = subcate_array.getJSONObject(i);

                    String sub_category_name = sub_object.getString("sub_category_name");
                    String sub_category_id = sub_object.getString("sub_category_id");
                    subcate_list.add(sub_category_name);
                    subcate_idlist.add(sub_category_id);
                }
                ArrayAdapter<String> subcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subcate_list);
                subcate_sp.setAdapter(subcate_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getsubSubCate(int pos) {

        String id = subcate_idlist.get(pos);
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_QUOTE_DATA_SUBSUBCATE)
                .addBodyParameter("subcategory_id", id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        subdata = response.toString();
                        setsubsubCateResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setsubsubCateResponse(String s) {

        subsubcate_idlist = new ArrayList<>();
        subsubcate_list = new ArrayList<>();
        subsubcate_list.add("Select sub sub Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String subsubcate_name = cate_obj.getString("subsubcategory_name");
                    String subsubcate_id = cate_obj.getString("subsubcategory_id");
                    subsubcate_list.add(subsubcate_name);
                    subsubcate_idlist.add(subsubcate_id);
                }
                ArrayAdapter<String> subsubcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subsubcate_list);
                subsubcate_sp.setAdapter(subsubcate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            browseImage();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    public void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
