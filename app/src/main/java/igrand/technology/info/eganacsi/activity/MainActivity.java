package igrand.technology.info.eganacsi.activity;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.ApiClient;
import igrand.technology.info.eganacsi.ApiInterface;
import igrand.technology.info.eganacsi.fragment.HomeFragment;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.NavigationAdapter;
import igrand.technology.info.eganacsi.fragment.BrowseCategoryFragment;
import igrand.technology.info.eganacsi.fragment.SearchFragment;
import igrand.technology.info.eganacsi.fragment.WishListFragment;
import igrand.technology.info.eganacsi.model.C_SellerProfileData;
import igrand.technology.info.eganacsi.model.C_SellerProfileResponse;
import igrand.technology.info.eganacsi.model.Navigation;
import igrand.technology.info.eganacsi.seller.activity.MyCampaignActivity;
import igrand.technology.info.eganacsi.seller.activity.MyLeadsActivity;
import igrand.technology.info.eganacsi.seller.activity.SellerMyProductActivity;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {

    Context ctx;
    ListView listView;
    ImageView menu_iv, user_iv, edit_iv,profile_iv;

    TextView nav_name_tv, nav_number_tv;
    TextView title_tv;
    ArrayList<Navigation> nav_list;
    NavigationView navigationView;
    LinearLayout home_ll, wish_ll, post_ll, noti_ll, search_ll;
    ImageView home_iv, wish_iv, search_iv;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    Fragment currentfragment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    ApiInterface apiInterface;
    C_SellerProfileResponse c_sellerProfileResponse;
    String stauts;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initxml();
        initObj();
        setNavigation();
/*        boolean internet = connectionDetector.isConnected();
        if (internet){
            String user_id = sessionManager.getData(SessionManager.KEY_CID);
            getUserDetails(user_id);
        }*/
//        setTitle("Hi Kamal,");

    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void getUserDetails(String user_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_PROFILE)
                .addBodyParameter("customer_id", user_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setNavigation();
                        sessionManager.setData(SessionManager.PROFILE_DATA,response.toString());
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        setNavigation();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONObject data_obj = object.getJSONObject("data");
                JSONObject customer_details_object = data_obj.getJSONObject("customer_details");

                String fname = customer_details_object.getString("first_name");
                String image = customer_details_object.getString("image");

                Utility.setTitle(ctx,fname);
                if (!image.equals("http://igrand.info/egns/")){
                    Utility.loadImage(ctx,profile_iv,image);
                }
                /*if (!image.equals("http://igrand.info/egns/")) {

                    Glide.with(ctx)
                            .load(image)
                            .into(circleImageView);
                }*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setNavigation() {


        SharedPreferences sharedPreferences =ctx.getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email","");
        String password = sharedPreferences.getString("password","");

        apiInterface= ApiClient.getClient1().create(ApiInterface.class);
        Call<C_SellerProfileResponse> call=apiInterface.CustomerSellerProfile(email,password);
        call.enqueue(new Callback<C_SellerProfileResponse>() {
            @Override
            public void onResponse(Call<C_SellerProfileResponse> call, Response<C_SellerProfileResponse> response) {
                try {
                    if (response.isSuccessful());
                    c_sellerProfileResponse=response.body();
//                    stauts=c_sellerProfileResponse.status.toString();

                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<C_SellerProfileResponse> call, Throwable t) {

            }
        });

        nav_list = new ArrayList<>();

        nav_list.add(new Navigation(R.drawable.home, "Home"));
        nav_list.add(new Navigation(R.drawable.search, "Search Products"));
        nav_list.add(new Navigation(R.drawable.browse, "Browse By Category"));
        nav_list.add(new Navigation(R.drawable.order, "Order Your Requirment"));
        nav_list.add(new Navigation(R.drawable.account, "My enquiries"));
        nav_list.add(new Navigation(R.drawable.seller, "My Notification"));
        nav_list.add(new Navigation(R.drawable.mywish, "My Wishlist"));
        nav_list.add(new Navigation(R.drawable.sellonegn, "Sell on Eganacsi"));

        //addded new
        nav_list.add(new Navigation(R.drawable.myleads, "My Lead"));
        nav_list.add(new Navigation(R.drawable.myquotation, "My Quotation Request"));
        nav_list.add(new Navigation(R.drawable.myproducts, "My Products"));
        nav_list.add(new Navigation(R.drawable.myservices, "My Services"));
        nav_list.add(new Navigation(R.drawable.mycampaigns, "My Campaigns"));
        nav_list.add(new Navigation(R.drawable.subscriptionplan, "Subscription Plans"));

        nav_list.add(new Navigation(R.drawable.home, "More"));

        setAdapter();
        listView.setOnItemClickListener(this);

        boolean search =sessionManager.getBooleanData(SessionManager.KEY_MAIN_ACTIVITY_TYPE);
        if (search) {
            sessionManager.setBooleanData(SessionManager.KEY_MAIN_ACTIVITY_TYPE, false);
            SearchFragment searchFragment = new SearchFragment(ctx);
            setFragment(searchFragment);
            changeicon(false, false, true);
        } else {
            sessionManager.setBooleanData(SessionManager.KEY_MAIN_ACTIVITY_TYPE, false);
            HomeFragment fragment = new HomeFragment(ctx);
            setFragment(fragment);
        }
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }


    private void initxml() {
        ctx = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView = findViewById(R.id.nav_view);
        home_ll = findViewById(R.id.ll_mainbottom_home);
        wish_ll = findViewById(R.id.ll_mainbottom_wishlist);
        post_ll = findViewById(R.id.ll_mainbottom_post);
        noti_ll = findViewById(R.id.ll_mainbottom_notification);
        search_ll = findViewById(R.id.ll_mainbottom_search);
        listView = (ListView) findViewById(R.id.lv_nav_listview);
        menu_iv = (ImageView) findViewById(R.id.menu);
//        user_iv = (ImageView) findViewById(R.id.iv_main_user);
        title_tv = (TextView) findViewById(R.id.tv_main_title);
        home_iv = (ImageView) findViewById(R.id.iv_home_home);
        wish_iv = (ImageView) findViewById(R.id.iv_home_wish);
        search_iv = (ImageView) findViewById(R.id.iv_home_search);

        View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);

        // view
        edit_iv = headerLayout.findViewById(R.id.iv_navheader_edit);
        profile_iv = headerLayout.findViewById(R.id.iv_navheader_profile);
        nav_name_tv = headerLayout.findViewById(R.id.tv_nav_name);
        nav_number_tv = headerLayout.findViewById(R.id.tv_nav_number);

        title_tv.setText("Hi , Kamal");

        sessionManager = new SessionManager(ctx);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        menu_iv.setOnClickListener(this);
        home_ll.setOnClickListener(this);
        wish_ll.setOnClickListener(this);
        post_ll.setOnClickListener(this);
        noti_ll.setOnClickListener(this);
        search_ll.setOnClickListener(this);
        edit_iv.setOnClickListener(this);

        String name = sessionManager.getData(SessionManager.KEY_FNAME);
        String number = sessionManager.getData(SessionManager.KEY_MOBILE);

        nav_name_tv.setText(name);
        nav_number_tv.setText(number);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else  {
            String page = sessionManager.getData(SessionManager.KEY_PAGE);
            if (page.equals("home")) {
                showAlert();
            } else if (page.equals("other")) {
                HomeFragment homeFragment = new HomeFragment(ctx);
                setFragment(homeFragment);
            }
            else if (page.equals("prod")){
                super.onBackPressed();

            }
            else {
                super.onBackPressed();
            }
        }
    }

    private void showAlert() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ctx);
        alertDialog.setTitle("Exit");
        alertDialog.setMessage("Are you sure you want close this application?");


        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                startActivity(new Intent(ctx, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                sessionManager.logoutUser();
                finish();
            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (position == 0) {
            // home
            changeicon(true, false, false);
            HomeFragment fragment = new HomeFragment(ctx);
            setFragment(fragment);
            drawer.closeDrawer(GravityCompat.START);
        } else if (position == 1) {
            // search
            changeicon(false, false, true);
            SearchFragment fragment = new SearchFragment(ctx);
            setFragment(fragment);
            drawer.closeDrawer(GravityCompat.START);
        } else if (position == 2) {
            // browse bt category
            BrowseCategoryFragment fragment = new BrowseCategoryFragment(ctx);
            setFragment(fragment);
            drawer.closeDrawer(GravityCompat.START);
        } else if (position == 3) {
            // order your requirment
            startActivity(new Intent(ctx, GetQuoteActivity.class));
        } else if (position == 4) {
            // my enquiries
            startActivity(new Intent(ctx, MyEnquiriesActivity.class));
        } else if (position == 5) {
            // notification
            startActivity(new Intent(ctx, NotificationActivity.class));
        } else if (position == 6) {
            // wishlist
            changeicon(false, true, false);
            WishListFragment fragment = new WishListFragment(ctx);
            setFragment(fragment);
            drawer.closeDrawer(GravityCompat.START);
        } else if (position == 7) {

            startActivity(new Intent(ctx, SellarRegisterActivity.class));



        } else if (position == 8) {
            // my leads
            startActivity(new Intent(ctx, MyLeadsActivity.class)
                    .putExtra("type", "leads")
                    .putExtra("title", "My Lead"));
        } else if (position == 9) {
            // my quotation request
            startActivity(new Intent(ctx, MyLeadsActivity.class)
                    .putExtra("type", "quote")
                    .putExtra("title", "My Quotation Request"));
        } else if (position == 10) {
            // my products
            startActivity(new Intent(ctx, SellerMyProductActivity.class)
                    .putExtra("title", "Seller My Product")
                    .putExtra("type", "product"));
        } else if (position == 11) {
            // my service
            startActivity(new Intent(ctx, SellerMyProductActivity.class)
                    .putExtra("title", "Seller My Services")
                    .putExtra("type", "service"));
        } else if (position == 12) {
            // my campign
            startActivity(new Intent(ctx, MyCampaignActivity.class));
        } else if (position == 13) {
            // subscription plans
            startActivity(new Intent(ctx, SubscriptionPlanActivity.class)
                    .putExtra("type", "main"));
        } else if (position == 14) {
            // more
            String name = nav_list.get(position).getName();
            if (name.equals("More")) {
                nav_list.set(14, new Navigation(R.drawable.termscond, "Less"));
                nav_list.add(new Navigation(R.drawable.termscond, "Terms & Condition"));
                nav_list.add(new Navigation(R.drawable.privary, "Privacy & Policy"));
                nav_list.add(new Navigation(R.drawable.faq, "FAQs"));
                nav_list.add(new Navigation(R.drawable.about, "About US"));
                nav_list.add(new Navigation(R.drawable.feedback, "Feedback"));
                nav_list.add(new Navigation(R.drawable.share, "Share App"));
                nav_list.add(new Navigation(R.drawable.star, "Rate US"));
                setAdapter();
                listView.setSelection(15);
            }
            if (name.equals("Less")) {
                nav_list.set(14, new Navigation(R.drawable.home, "More"));
                nav_list.remove(21);
                nav_list.remove(20);
                nav_list.remove(19);
                nav_list.remove(18);
                nav_list.remove(17);
                nav_list.remove(16);
                nav_list.remove(15);
                setAdapter();
                listView.setSelection(8);
            }
        } else if (position == 15) {
            // terms condition
            startActivity(new Intent(ctx, TermsConditionActivity.class));
        } else if (position == 16) {
            // privacuy policy
            startActivity(new Intent(ctx, PrivacyPolicyActivity.class));
        } else if (position == 17) {
            // faq
            startActivity(new Intent(ctx, FAQsActivity.class));
        } else if (position == 18) {
            // about us
            startActivity(new Intent(ctx, AboutUsActivity.class));
        } else if (position == 19) {
            // feedback
            startActivity(new Intent(ctx, FeedbackActivity.class));
        } else if (position == 20) {
            // share app
            shareApp();
        } else if (position == 21) {
            // rate us
            dialogRateUs();
        }
    }

    private void shareApp() {
        try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "eGanacsi");
            String sAux = "\nPlease install this application \n\n";
            sAux = sAux + "https://play.google.com/store/apps/details?id= \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    private void setAdapter() {

        NavigationAdapter adp_navigation = new NavigationAdapter(ctx, nav_list);
        listView.setAdapter(adp_navigation);
    }

    public void dialogRateUs() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.dialog_rateus);
        ImageView image = (ImageView) dialog.findViewById(R.id.iv_dialograte_cancel);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.menu:
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;

//            case R.id.iv_main_user:
//                startActivity(new Intent(ctx, MyAccountActivity.class));
//                break;

            case R.id.iv_navheader_edit:
                startActivity(new Intent(ctx, EditProfileActivity.class));
                break;

            case R.id.ll_mainbottom_home:
                changeicon(true, false, false);
                HomeFragment fragment = new HomeFragment(ctx);

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fl_main_framelayout, fragment,"homee");
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case R.id.ll_mainbottom_wishlist:
                changeicon(false, true, false);
                WishListFragment fragment1 = new WishListFragment(ctx);
                setFragment(fragment1);
                break;
            case R.id.ll_mainbottom_post:
//                startActivity(new Intent(ctx, ContactUsActivity.class));
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View vi = inflater.inflate(R.layout.popup, null);
                builder.setView(vi);
                final AlertDialog b = builder.create();
                Button button=vi.findViewById(R.id.btn_product);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this,GetQuoteActivity.class));
                    }
                });
                Button button1=vi.findViewById(R.id.btn_service);
                button1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(MainActivity.this,BookServiceActivity.class));
                    }
                });
                b.show();



                break;

            case R.id.ll_mainbottom_notification:
                startActivity(new Intent(ctx, NotificationActivity.class));
                break;

            case R.id.ll_mainbottom_search:
                changeicon(false, false, true);
               startActivity(new Intent(MainActivity.this,MyAccountActivity.class));
                break;
        }
    }

    private void changeicon(boolean home, boolean wishlist, boolean search) {

        if (home) {
            home_iv.setImageResource(R.drawable.home_blue);
        } else {
            home_iv.setImageResource(R.drawable.home_black);
        }
        if (wishlist) {
            wish_iv.setImageResource(R.drawable.wishlist_blue);
        } else {
            wish_iv.setImageResource(R.drawable.wishlist_black);
        }
        if (search) {
            search_iv.setImageResource(R.drawable.user_circle_blue);
        } else {
            search_iv.setImageResource(R.drawable.user_circle_black);
        }

    }
    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


}
