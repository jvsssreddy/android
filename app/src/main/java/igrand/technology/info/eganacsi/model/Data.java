package igrand.technology.info.eganacsi.model;

/**
 * Created by Admin on 07-Apr-18.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("slider")
    @Expose
    private List<Slider> slider = null;
    @SerializedName("popular_product")
    @Expose
    private List<PopularProduct> popularProduct = null;
    @SerializedName("popular_service")
    @Expose
    private List<PopularService> popularService = null;
    @SerializedName("featured_product")
    @Expose
    private List<FeaturedProduct> featuredProduct = null;
    @SerializedName("featured_service")
    @Expose
    private List<FeaturedService> featuredService = null;

    public List<Slider> getSlider() {
        return slider;
    }

    public void setSlider(List<Slider> slider) {
        this.slider = slider;
    }

    public List<PopularProduct> getPopularProduct() {
        return popularProduct;
    }

    public void setPopularProduct(List<PopularProduct> popularProduct) {
        this.popularProduct = popularProduct;
    }

    public List<PopularService> getPopularService() {
        return popularService;
    }

    public void setPopularService(List<PopularService> popularService) {
        this.popularService = popularService;
    }

    public List<FeaturedProduct> getFeaturedProduct() {
        return featuredProduct;
    }

    public void setFeaturedProduct(List<FeaturedProduct> featuredProduct) {
        this.featuredProduct = featuredProduct;
    }

    public List<FeaturedService> getFeaturedService() {
        return featuredService;
    }

    public void setFeaturedService(List<FeaturedService> featuredService) {
        this.featuredService = featuredService;
    }

}