package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ProgressDialog progressDialog;
    Button login_bt;
    EditText email_et, password_et;
    TextView newuser_tv, forgot_tv, privacy_tv, terms_tv;
    CheckBox remenber_cb;
    ImageView facebook_iv, gplus_iv;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    //    facebook login
    LoginButton facebbokloginButton;
    CallbackManager callbackManager;
    private static final String EMAIL = "email";

    // google login
    GoogleSignInClient mGoogleSignInClient;
    public static final int RC_SIGN_IN = 12345;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initxml();
        initObj();
        googleLogin();
        checkLogin();
    }

    private void checkLogin() {

        boolean login = sessionManager.isLoggedIn();
        if (login) {
            startActivity(new Intent(ctx, MainActivity.class));
        }
    }

    private void googleLogin() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

//        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
//        updateUI(account);
    }

    private void updateUI(GoogleSignInAccount account) {

        Log.e("Register activity", "display name: " + account.getDisplayName());
        String personName = account.getDisplayName();
        String personPhotoUrl = account.getPhotoUrl().toString();
        String email = account.getEmail();
        String fname = account.getGivenName();
        String g_id = account.getId();
//        Utility.toastView(ctx,personName+" "+email);

        getSocialLogin("gp",fname,"",email,"",g_id);
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Register Activity", "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initxml() {
        progressDialog = new ProgressDialog(getApplicationContext());
        progressDialog.setMessage("Loading...");
        ctx = this;
        facebbokloginButton = (LoginButton) findViewById(R.id.login_button);
        login_bt = (Button) findViewById(R.id.bt_login_login);
        newuser_tv = (TextView) findViewById(R.id.tv_login_newuser);
        forgot_tv = (TextView) findViewById(R.id.tv_login_fogot);
        privacy_tv = (TextView) findViewById(R.id.tv_login_privacy);
        terms_tv = (TextView) findViewById(R.id.tv_login_terms);
        remenber_cb = (CheckBox) findViewById(R.id.cb_login_remember);
        facebook_iv = (ImageView) findViewById(R.id.iv_login_facebook);
        gplus_iv = (ImageView) findViewById(R.id.iv_login_gplus);
        email_et = (EditText) findViewById(R.id.et_login_email);
        password_et = (EditText) findViewById(R.id.et_login_password);
        login_bt.setOnClickListener(this);
        newuser_tv.setOnClickListener(this);
        forgot_tv.setOnClickListener(this);
        privacy_tv.setOnClickListener(this);
        terms_tv.setOnClickListener(this);
        facebook_iv.setOnClickListener(this);
        gplus_iv.setOnClickListener(this);
        setTextWithUnderline(terms_tv, getString(R.string.term_condtion));
        setTextWithUnderline(privacy_tv, getString(R.string.privacy_policy));



    }

    private void setTextWithUnderline(TextView privacy_tv, String s) {

        SpannableString content = new SpannableString(s);
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        privacy_tv.setText(content);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.bt_login_login:
                boolean internet = connectionDetector.isConnected();
                if (internet) {
                    getData();
//                    startActivity(new Intent(ctx,MainActivity.class));
                } else {
                    Utility.toastView(ctx, ctx.getString(R.string.no_internet));
                }
                break;

            case R.id.tv_login_newuser:
                startActivity(new Intent(ctx, RegisterActivity.class));
                break;

            case R.id.tv_login_fogot:
                break;

            case R.id.tv_login_privacy:
                startActivity(new Intent(ctx, PrivacyPolicyActivity.class));
                break;

            case R.id.tv_login_terms:
                startActivity(new Intent(ctx, TermsConditionActivity.class));
                break;

            case R.id.iv_login_facebook:
                facebookLogin();
                break;

            case R.id.iv_login_gplus:
                signIn();
                break;
        }
    }

    private void getData() {

        String email = email_et.getText().toString().trim();
        String password = password_et.getText().toString().trim();


        if (email.equals("") || password.equals("")) {
            Utility.toastView(ctx, "Enter Username and password");
        } else if (!email.matches(emailPattern)) {
            Utility.toastView(ctx, "Invalid Username");
        } else {
            getLogin(email, password);
        }
    }

    private void getLogin(String email, String password) {
        progressDialog.show();


        AndroidNetworking.post(WebApi.URL_LOGIN)
                .addBodyParameter("email", email)
                .addBodyParameter("password", password)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
//                        Utility.toastView(ctx, response.toString());
                        setResponse(response.toString());
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(ANError error) {
                        Utility.toastView(ctx, error.toString());
                        progressDialog.dismiss();
                        // handle error
                    }
                });
    }

    private void setResponse(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            String status = jsonObject.getString("status");
            String message = jsonObject.getString("message");
            if (status.equals("1")) {

                JSONObject dat_obj = jsonObject.getJSONObject("data");
                String c_id = dat_obj.getString("c_id");
                String customer_id = dat_obj.getString("customer_id");
                String fname = dat_obj.getString("first_name");
                String lname = dat_obj.getString("last_name");
                String mobile = dat_obj.getString("phone");
                String email = dat_obj.getString("email");
                String pass=dat_obj.getString("password");

                SharedPreferences sharedPreferences = ctx.getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("email",email);
                editor.putString("password",pass);
                editor.commit();

                sessionManager.createLoginSession(fname, lname, mobile, email, c_id, customer_id);
                startActivity(new Intent(ctx, MainActivity.class));
                finish();
            } else if (status.equals("0") || message.equals(ctx.getString(R.string.res_inavlid_id_pass))) {
                Utility.toastView(ctx, "Invalid id and password");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void facebookLogin() {

        callbackManager = CallbackManager.Factory.create();
        facebbokloginButton.setReadPermissions(Arrays.asList(EMAIL));
        facebbokloginButton.setReadPermissions("user_friends");
        facebbokloginButton.setReadPermissions("public_profile");
        facebbokloginButton.setReadPermissions("email");
        facebbokloginButton.setReadPermissions("user_birthday");
        // If you are using in a fragment, call facebbokloginButton.setFragment(this);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", EMAIL));

        // Callback registration
        facebbokloginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Toast.makeText(ctx, "login button  success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getApplicationContext(), "cancel Login", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getApplicationContext(), "error in Login", Toast.LENGTH_SHORT).show();
            }
        });


//        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        try {
//                                            Util.toastView(ctx,object.toString());
                                            Log.d("TAG", "fb json object: " + object);
                                            Log.d("TAG", "fb graph response: " + response);

                                            String token = loginResult.getAccessToken().toString();
                                            String facebook_id = object.getString("id");
                                            String first_name = object.getString("first_name");
                                            String last_name = object.getString("last_name");

                                            String email = "";
                                            if (object.has("email")) {
                                                email = object.getString("email");
                                            }
                                            getSocialLogin("fb",first_name,last_name,email,facebook_id,"");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(ctx, "facebook login cancel by user", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        String errt = exception.toString();
                        Toast.makeText(ctx, exception.toString() + "", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getSocialLogin(String type,final String first_name, final String last_name, final String email, String fb_id,String gp_id) {

        String social_type ="";
        String token ="";
        if (type.equals("fb")){
            social_type = "0";
            token = fb_id;
        }else {
            social_type = "1";
            token = email;
        }
        AndroidNetworking.post(WebApi.URL_SOCIAL_LOGIN)
                .addBodyParameter("social_type", social_type)
                .addBodyParameter("firstname", first_name)
                .addBodyParameter("email", email)
                .addBodyParameter("token", token)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")){

                                JSONObject object = response.getJSONObject("data");
                                String c_id= object.getString("c_id");
                                String customer_id= object.getString("customer_id");

                                sessionManager.createLoginSession(first_name, last_name, "", email, c_id, customer_id);
                                startActivity(new Intent(ctx, MainActivity.class));
                                finish();
                            }else {
                                Utility.toastView(ctx,ctx
                                .getString(R.string.something_went));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }
}
