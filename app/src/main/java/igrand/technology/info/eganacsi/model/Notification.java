package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 4/13/2018.
 */

public class Notification
{
    private String name;
    private String message;
    private String date;
    private String link;

    public Notification(String name, String message, String date, String link) {
        this.name = name;
        this.message = message;
        this.date = date;
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
