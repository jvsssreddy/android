package igrand.technology.info.eganacsi.seller.response.MyLeads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyLeadData
{
    @SerializedName("leads")
    @Expose
    private List<Lead> leads = null;

    public List<Lead> getLeads() {
        return leads;
    }

    public void setLeads(List<Lead> leads) {
        this.leads = leads;
    }
}
