package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.ViewAllAdapter;
import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 4/23/2018.
 */

@SuppressLint("ValidFragment")
public class SubAllCategoryFragment extends Fragment implements AdapterView.OnItemClickListener {
    View view;
    Context ctx;
    int position;
    RecyclerView recyclerView;
    Spinner location_sp;
    GridView gridView;
    EditText et_double_search;
    String service;
    SessionManager sessionManager;

    ArrayList<ViewAll> viewAllArrayList;

    public SubAllCategoryFragment(Context ctx, String service, int position) {
        this.ctx = ctx;
        this.service = service;
        this.position = position;
        sessionManager = new SessionManager(ctx);
        sessionManager.setData(SessionManager.KEY_PAGE, "other");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewall, null);
        initXml();
        setLocation();
        Utility.setTitle(ctx, "All Category");
        String all_cate = sessionManager.getData(SessionManager.HOME_API_ALL_CATE);
        allcategoy(all_cate);
        return view;
    }

    private void allcategoy(String data) {

        viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray popular_product_array = jsonObject.getJSONArray("data");
            JSONObject object = popular_product_array.getJSONObject(position);
            String cate_name = object.getString("category_name");
            Utility.setTitle(ctx,cate_name);
            JSONArray sub_cate_array = object.getJSONArray("sub_category");
            for (int i = 0; i < sub_cate_array.length(); i++) {

                JSONObject sub_object = sub_cate_array.getJSONObject(i);
                String sub_cate_id = sub_object.getString("sub_category_id");
                String sub_category_image = sub_object.getString("sub_category_image");
//                Toast.makeText(ctx, sub_category_image, Toast.LENGTH_SHORT).show();
                String sub_category_name = sub_object.getString("sub_category_name");
                viewAllArrayList.add(new ViewAll("",sub_cate_id,sub_category_name,sub_category_image));
            }

            ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ctx, viewAllArrayList, 3);
            gridView.setAdapter(viewAllAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        location_sp = view.findViewById(R.id.sp_double_location);
        et_double_search = view.findViewById(R.id.et_double_search);
        gridView = view.findViewById(R.id.gv_viewall_gridview);
        recyclerView = view.findViewById(R.id.rv_viewall_recyclerview);

        recyclerView.setVisibility(View.GONE);
        gridView.setVisibility(View.VISIBLE);
        gridView.setOnItemClickListener(this);

        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFragment searchFragment = new SearchFragment(ctx);
                Utility.setFragment(searchFragment,ctx);
            }
        });
    }

    private void setLocation() {
        String location = sessionManager.getData(SessionManager.LOCATION_DATA);
        if (!location.equals(null)){

            ArrayList<String> city_list = new ArrayList<>();
            city_list.add("Anywhere");
            try {
                JSONObject object = new JSONObject(location);
                String status = object.getString("status");

                if (status.equals("1")) {

                    JSONArray array_location = object.getJSONArray("Locations");
                    for (int i = 0; i < array_location.length(); i++) {

                        JSONObject data_object = array_location.getJSONObject(i);
                        String city = data_object.getString("city_name");
                        city_list.add(city);
                    }
                    ArrayAdapter<String> city_adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                    location_sp.setAdapter(city_adapter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String service = viewAllArrayList.get(position).getName();
        String id_1 = viewAllArrayList.get(position).getId();
        SubCategoryFragment subCategoryFragment = new SubCategoryFragment(ctx, service, id_1);
        setFragment(subCategoryFragment);
    }



    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }
}
