package igrand.technology.info.eganacsi.seller.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.seller.response.camp.Campaign;
import igrand.technology.info.eganacsi.util.SessionManager;

public class MyCampViewmoreActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    TextView name_tv, date_tv, time_tv, city_tv, area_tv,content_tv,url_tv,noti_tv;

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycamp_viewmore);
        initXml();
        initObj();
        setData();
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
    }


    private void setData() {
        Campaign campaign = (Campaign) getIntent().getParcelableExtra("data");

        String comp_date = campaign.getDate();
        String date = comp_date.substring(0,10);
        String time = comp_date.substring(11);

        name_tv.setText("Campaign Name : "+campaign.getCompaignName());
        date_tv.setText("Date : "+date);
        time_tv.setText("Time : "+time);
        content_tv.setText(campaign.getContent());
        url_tv.setText("Url : "+campaign.getWebsite());
        noti_tv.setText(campaign.getNoOfCustomer()+"");

        setLocation(campaign);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_mycampdet_back);
        name_tv = findViewById(R.id.tv_mycampdet_name);
        date_tv = findViewById(R.id.tv_mycampdet_date);
        time_tv = findViewById(R.id.tv_mycampdet_time);
        city_tv = findViewById(R.id.tv_mycampdet_city);
        area_tv = findViewById(R.id.tv_mycampdet_area);
        content_tv = findViewById(R.id.tv_mycampdet_content);
        url_tv = findViewById(R.id.tv_mycampdet_url);
        noti_tv = findViewById(R.id.tv_mycampdet_noofnoti);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setLocation(Campaign campaign) {

        String location = sessionManager.getData(SessionManager.LOCATION_DATA);
        if (!location.equals(null)){

            ArrayList<String> city_list = new ArrayList<>();
            try {
                JSONObject object = new JSONObject(location);
                String status = object.getString("status");
                if (status.equals("1")) {
                    JSONArray array_location = object.getJSONArray("Locations");
                    for (int i = 0; i < array_location.length(); i++) {
                        JSONObject data_object = array_location.getJSONObject(i);
                        String city_id = data_object.getString("city_id");
                        if (city_id.equals(campaign.getCity())){
                            String city_name = data_object.getString("city_name");
                            city_tv.setText("City : "+city_name);

                            JSONArray area_array = data_object.getJSONArray("area");
                            if (!area_array.equals(null)){
                                for (int j = 0; j < area_array.length(); j++) {
                                    JSONObject area_obj = area_array.getJSONObject(j);
                                    String area_id = area_obj.getString("area_id");
                                    if (area_id.equals(campaign.getArea())){
                                        String area_name = area_obj.getString("area_name");
                                        area_tv.setText("Area : "+area_name);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
