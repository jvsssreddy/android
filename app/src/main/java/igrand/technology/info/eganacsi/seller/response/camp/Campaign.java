package igrand.technology.info.eganacsi.seller.response.camp;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Campaign implements Parcelable
{
    @SerializedName("cm_id")
    @Expose
    private String cmId;
    @SerializedName("compaign_name")
    @Expose
    private String compaignName;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("customer_ids")
    @Expose
    private List<String> customerIds = null;
    @SerializedName("no_of_customer")
    @Expose
    private Integer noOfCustomer;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("admin_message")
    @Expose
    private String adminMessage;

    protected Campaign(Parcel in) {
        cmId = in.readString();
        compaignName = in.readString();
        date = in.readString();
        city = in.readString();
        area = in.readString();
        website = in.readString();
        content = in.readString();
        number = in.readString();
        customerIds = in.createStringArrayList();
        if (in.readByte() == 0) {
            noOfCustomer = null;
        } else {
            noOfCustomer = in.readInt();
        }
        status = in.readString();
        adminMessage = in.readString();
    }

    public static final Creator<Campaign> CREATOR = new Creator<Campaign>() {
        @Override
        public Campaign createFromParcel(Parcel in) {
            return new Campaign(in);
        }

        @Override
        public Campaign[] newArray(int size) {
            return new Campaign[size];
        }
    };

    public String getCmId() {
        return cmId;
    }

    public void setCmId(String cmId) {
        this.cmId = cmId;
    }

    public String getCompaignName() {
        return compaignName;
    }

    public void setCompaignName(String compaignName) {
        this.compaignName = compaignName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<String> getCustomerIds() {
        return customerIds;
    }

    public void setCustomerIds(List<String> customerIds) {
        this.customerIds = customerIds;
    }

    public Integer getNoOfCustomer() {
        return noOfCustomer;
    }

    public void setNoOfCustomer(Integer noOfCustomer) {
        this.noOfCustomer = noOfCustomer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdminMessage() {
        return adminMessage;
    }

    public void setAdminMessage(String adminMessage) {
        this.adminMessage = adminMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cmId);
        dest.writeString(compaignName);
        dest.writeString(date);
        dest.writeString(city);
        dest.writeString(area);
        dest.writeString(website);
        dest.writeString(content);
        dest.writeString(number);
        dest.writeStringList(customerIds);
        if (noOfCustomer == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(noOfCustomer);
        }
        dest.writeString(status);
        dest.writeString(adminMessage);
    }
}
