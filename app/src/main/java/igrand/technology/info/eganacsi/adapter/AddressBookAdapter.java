package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.AddressBook;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 5/1/2018.
 */

public class AddressBookAdapter extends BaseAdapter
{
    Context ctx;
    ArrayList<AddressBook>addressBooks_list;

    public AddressBookAdapter(Context ctx, ArrayList<AddressBook> addressBooks_list) {
        this.ctx = ctx;
        this.addressBooks_list = addressBooks_list;
    }

    @Override
    public int getCount() {
        return addressBooks_list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_addressbook,null);

        TextView title_tv = view.findViewById(R.id.tv_adpaddress_title);
        TextView name_tv = view.findViewById(R.id.tv_adpaddress_name);
        TextView mobile_tv = view.findViewById(R.id.tv_adpaddress_mobile);
        TextView area_tv = view.findViewById(R.id.tv_adpaddress_area);
        TextView city_tv = view.findViewById(R.id.tv_adpaddress_city);

        String fname = addressBooks_list.get(position).getFirstname();
        String lname = addressBooks_list.get(position).getLastname();
        String area = addressBooks_list.get(position).getAddress();
        String mobile = addressBooks_list.get(position).getPhone();
        String city = addressBooks_list.get(position).getPhone();
        String state = addressBooks_list.get(position).getState();
        String country = addressBooks_list.get(position).getCountry();

        title_tv.setText(addressBooks_list.get(position).getTitle());
        name_tv.setText(fname+" "+lname);
        mobile_tv.setText(mobile);
        area_tv.setText(area);
        city_tv.setText(city+","+state+","+country);

        return view;
    }
}
