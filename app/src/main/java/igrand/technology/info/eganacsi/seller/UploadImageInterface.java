package igrand.technology.info.eganacsi.seller;

import igrand.technology.info.eganacsi.model.api_response.UpdateProfile;
import igrand.technology.info.eganacsi.seller.response.AddService;
import igrand.technology.info.eganacsi.seller.response.UpdateResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by kamlesh on 5/17/2018.
 */

public interface UploadImageInterface
{

    @Multipart
    @POST("seller-service-add.php")
    Call<AddService> uploadProductFile(@Part MultipartBody.Part file,
                                       @Part("seller_id") RequestBody seller_id,
                                       @Part("category_id") RequestBody cate_id,
                                       @Part("subcategory_id") RequestBody subcate_id,
                                       @Part("subsubcategory_id") RequestBody subsubcate_id,
                                       @Part("product_name") RequestBody service_name,
                                       @Part("product_price") RequestBody price,
                                       @Part("discount_price") RequestBody dis_price,
                                       @Part("discount_percent") RequestBody dis_offer,
                                       @Part("description") RequestBody descr,
                                       @Part("availability") RequestBody availble,
                                       @Part("status") RequestBody status);

    @Multipart
    @POST("seller-product-add.php")
    Call<AddService> uploadServicesFile(@Part MultipartBody.Part file,
                                        @Part("seller_id") RequestBody seller_id,
                                        @Part("category_id") RequestBody cate_id,
                                        @Part("subcategory_id") RequestBody subcate_id,
                                        @Part("subsubcategory_id") RequestBody subsubcate_id,
                                        @Part("service_name") RequestBody service_name,
                                        @Part("price") RequestBody price,
                                        @Part("discount_price") RequestBody dis_price,
                                        @Part("discount_percent") RequestBody dis_offer,
                                        @Part("sunday") RequestBody sunday,
                                        @Part("monday") RequestBody monday,
                                        @Part("tuesday") RequestBody tues,
                                        @Part("wednesday") RequestBody wedn,
                                        @Part("thursday") RequestBody thurs,
                                        @Part("friday") RequestBody fri,
                                        @Part("saturday") RequestBody sat,
                                        @Part("description") RequestBody descr,
                                        @Part("status") RequestBody status);

    @Multipart
    @POST("seller-product-edit.php")
    Call<UpdateResponse> upDateProductDetails(@Part MultipartBody.Part file,
                                              @Part("seller_id") RequestBody seller_id,
                                              @Part("category_id") RequestBody cate_id,
                                              @Part("subcategory_id") RequestBody subcate_id,
                                              @Part("subsubcategory_id") RequestBody subsubcate_id,
                                              @Part("product_name") RequestBody service_name,
                                              @Part("product_price") RequestBody price,
                                              @Part("discount_price") RequestBody dis_price,
                                              @Part("discount_percent") RequestBody dis_offer,
                                              @Part("description") RequestBody descr,
                                              @Part("availability") RequestBody availble,
                                              @Part("status") RequestBody status,
                                              @Part("product_id") RequestBody product_id);

    @Multipart
    @POST("seller-service-edit.php")
    Call<UpdateResponse> upDateServiceDetails(@Part MultipartBody.Part file,
                                        @Part("seller_id") RequestBody seller_id,
                                        @Part("category_id") RequestBody cate_id,
                                        @Part("subcategory_id") RequestBody subcate_id,
                                        @Part("subsubcategory_id") RequestBody subsubcate_id,
                                        @Part("service_name") RequestBody service_name,
                                        @Part("price") RequestBody price,
                                        @Part("discount_price") RequestBody dis_price,
                                        @Part("discount_percent") RequestBody dis_offer,
                                        @Part("sunday") RequestBody sunday,
                                        @Part("monday") RequestBody monday,
                                        @Part("tuesday") RequestBody tues,
                                        @Part("wednesday") RequestBody wedn,
                                        @Part("thursday") RequestBody thurs,
                                        @Part("friday") RequestBody fri,
                                        @Part("saturday") RequestBody sat,
                                        @Part("description") RequestBody descr,
                                        @Part("status") RequestBody status,
                                        @Part("service_id") RequestBody service_id);


    @Multipart
    @POST("edit-profile.php")
    Call<UpdateProfile> updateprofile(@Part MultipartBody.Part file,
                                      @Part("user_id") RequestBody seller_id,
                                      @Part("first_name") RequestBody cate_id,
                                      @Part("last_name") RequestBody subcate_id,
                                      @Part("email") RequestBody subsubcate_id,
                                      @Part("phone") RequestBody service_name,
                                      @Part("city") RequestBody price,
                                      @Part("area") RequestBody dis_price,
                                      @Part("address1") RequestBody dis_offer);

}
