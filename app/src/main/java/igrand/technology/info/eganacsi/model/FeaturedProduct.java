package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 4/21/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FeaturedProduct implements Serializable{

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name ")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("discount_price")
    @Expose
    private String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    private String subsubcategoryId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("item_code")
    @Expose
    private String itemCode;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("availability")
    @Expose
    private String availability;

    @SerializedName("subsubcategory_name")
    @Expose
    public String subsubcategoryName;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("category_name")
    @Expose
    public String categoryName;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubsubcategoryId() {
        return subsubcategoryId;
    }

    public void setSubsubcategoryId(String subsubcategoryId) {
        this.subsubcategoryId = subsubcategoryId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getSubsubcategoryName() {
        return subsubcategoryName;
    }

    public void setSubsubcategoryName(String subsubcategoryName) {
        this.subsubcategoryName = subsubcategoryName;
    }

    public String getSubcategoryName() {
        return subcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        this.subcategoryName = subcategoryName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}