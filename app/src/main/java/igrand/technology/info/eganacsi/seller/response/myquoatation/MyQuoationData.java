package igrand.technology.info.eganacsi.seller.response.myquoatation;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyQuoationData
{
    @SerializedName("enquiry")
    @Expose
    private List<MyQuotationEnquiry> enquiry = null;

    public List<MyQuotationEnquiry> getEnquiry() {
        return enquiry;
    }

    public void setEnquiry(List<MyQuotationEnquiry> enquiry) {
        this.enquiry = enquiry;
    }
}
