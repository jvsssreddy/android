package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.activity.BookServiceActivity;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.adapter.HomeAdapter;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.BrowsCategoryAdapter;
import igrand.technology.info.eganacsi.model.AllCategory;
import igrand.technology.info.eganacsi.model.CategoryBean;
import igrand.technology.info.eganacsi.network.NetworkUtils;

/**
 * Created by kamlesh on 4/11/2018.
 */

@SuppressLint("ValidFragment")
public class BrowseCategoryFragment extends Fragment implements View.OnClickListener, NetworkUtils.NetworkListner ,HomeAdapter.OnClickListener {

    View view;
    Context ctx;
    RecyclerView recyclerView;
    Button get_quote_bt, bookservice_bt;
    Spinner location_sp;
    EditText et_double_search;

    SessionManager sessionManager;
    private AllCategory all_category;
    private ArrayList<CategoryBean> mAllcatList;
    private NetworkUtils networkUtils;


    @SuppressLint("ValidFragment")
    public BrowseCategoryFragment(Context ctx) {
        this.ctx = ctx;
        sessionManager = new SessionManager(ctx);
//        sessionManager.setData(SessionManager.KEY_PAGE, "other");

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkUtils = new NetworkUtils(ctx,this);
        networkUtils.call(CategoryReq);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_browse_by_bategory, null);
        initXml();
        setLocation();
        return view;
    }

    private void setLocation() {
        String location = sessionManager.getData(SessionManager.LOCATION_DATA);
        if (!location.equals(null)){

            ArrayList<String> city_list = new ArrayList<>();
            city_list.add("Anywhere");
            try {
                JSONObject object = new JSONObject(location);
                String status = object.getString("status");

                if (status.equals("1")) {

                    JSONArray array_location = object.getJSONArray("Locations");
                    for (int i = 0; i < array_location.length(); i++) {

                        JSONObject data_object = array_location.getJSONObject(i);
                        String city = data_object.getString("city_name");
                        city_list.add(city);
                    }
                    ArrayAdapter<String> city_adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                    location_sp.setAdapter(city_adapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initXml() {
        et_double_search = view.findViewById(R.id.et_double_search);
        location_sp = view.findViewById(R.id.sp_double_location);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_browsecate_allcate);
        get_quote_bt = (Button) view.findViewById(R.id.bt_browsecate_getquote);
        bookservice_bt = (Button) view.findViewById(R.id.bt_browsecate_bookservices);

        LinearLayoutManager manager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(manager);
        get_quote_bt.setOnClickListener(this);
        bookservice_bt.setOnClickListener(this);
        et_double_search.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.bt_browsecate_getquote:
                startActivity(new Intent(ctx, GetQuoteActivity.class));
                break;

            case R.id.bt_browsecate_bookservices:
                startActivity(new Intent(ctx, BookServiceActivity.class));
                break;

            case R.id.et_double_search:
                SearchFragment searchFragment = new SearchFragment(ctx);
                setFragment(searchFragment);
                break;
        }
    }

    @Override
    public void onResult(String result, int req_code) {
        if (req_code == NetworkUtils.NetworkListner.CategoryReq) {
            Gson gson = new Gson();
            all_category = gson.fromJson(result, AllCategory.class);
            mAllcatList = new ArrayList<>(all_category.getData());
            setAdapter();
            Log.e("onPostExecute: ", all_category.getMessage());
        }
    }

    private void setAdapter() {
        BrowsCategoryAdapter adapter  = new BrowsCategoryAdapter(ctx,mAllcatList,this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onAdapterClick(View view, int position, int viewType) {

        SubAllCategoryFragment subAllCategoryFragment = new SubAllCategoryFragment(ctx, "All category", position);
        setFragment(subAllCategoryFragment);
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }
}
