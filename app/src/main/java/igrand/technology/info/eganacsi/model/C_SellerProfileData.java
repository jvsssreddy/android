package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/26/2018.
 */

public class C_SellerProfileData {




    @SerializedName("s_id")
    @Expose
    public String sId;
    @SerializedName("customer_id")
    @Expose
    public String customerId;
    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("address1")
    @Expose
    public String address1;
    @SerializedName("address2")
    @Expose
    public String address2;
    @SerializedName("pin")
    @Expose
    public String pin;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("buisness_name")
    @Expose
    public String buisnessName;
    @SerializedName("buisness_address")
    @Expose
    public String buisnessAddress;
    @SerializedName("buisness_city")
    @Expose
    public String buisnessCity;
    @SerializedName("website_address")
    @Expose
    public String websiteAddress;
    @SerializedName("membership")
    @Expose
    public String membership;
    @SerializedName("months")
    @Expose
    public String months;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("credits")
    @Expose
    public String credits;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sId", sId).append("customerId", customerId).append("firstName", firstName).append("lastName", lastName).append("email", email).append("phone", phone).append("city", city).append("area", area).append("address1", address1).append("address2", address2).append("pin", pin).append("image", image).append("status", status).append("buisnessName", buisnessName).append("buisnessAddress", buisnessAddress).append("buisnessCity", buisnessCity).append("websiteAddress", websiteAddress).append("membership", membership).append("months", months).append("price", price).append("credits", credits).toString();
    }
}
