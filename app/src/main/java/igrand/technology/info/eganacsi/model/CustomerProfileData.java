package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/26/2018.
 */

public class CustomerProfileData {


    @SerializedName("customer_details")
    @Expose

    public CustomerDetails customerDetails;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("customerDetails", customerDetails).toString();
    }
}
