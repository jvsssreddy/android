package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.Wishlist;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.WishlistAdapter;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 5/2/2018.
 */

@SuppressLint("ValidFragment")
public class WishListFragment extends Fragment
{
    Context ctx;
    RecyclerView recyclerView;
    Spinner location_sp;
    EditText et_double_search;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @SuppressLint("ValidFragment")
    public WishListFragment(Context ctx) {
        this.ctx = ctx;
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wishlist,null);
        initXml(view);
        String cust_id = sessionManager.getData(SessionManager.KEY_CID);
        setLocation();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            getWishlist(cust_id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
        return view;
    }

    private void getWishlist(String cust_id) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setLocation() {
        String location = sessionManager.getData(SessionManager.LOCATION_DATA);
        if (!location.equals(null)){

            ArrayList<String> city_list = new ArrayList<>();
            city_list.add("Anywhere");
            try {
                JSONObject object = new JSONObject(location);
                String status = object.getString("status");

                if (status.equals("1")) {

                    JSONArray array_location = object.getJSONArray("Locations");
                    for (int i = 0; i < array_location.length(); i++) {

                        JSONObject data_object = array_location.getJSONObject(i);
                        String city = data_object.getString("city_name");
                        city_list.add(city);
                    }
                    ArrayAdapter<String> city_adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                    location_sp.setAdapter(city_adapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void setResponse(String s) {

        ArrayList<Wishlist> wishlists_list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(s);
            JSONObject data_obj = object.getJSONObject("data");
            JSONArray wish_arry = data_obj.getJSONArray("wishlist");
            for (int i = 0; i < wish_arry.length(); i++) {

                JSONObject datajson = wish_arry.getJSONObject(i);
                String id = datajson.getString("wishlist_id");
                String p_id = datajson.getString("p_id");
                String name = datajson.getString("pname");
                String image = datajson.getString("pimage");
                String type = datajson.getString("type");
                String c_name = datajson.getString("company_name");
                String c_address = datajson.getString("address");

                wishlists_list.add(new Wishlist(id,p_id,name,image,type,c_name,c_address));
            }
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
            recyclerView.setLayoutManager(layoutManager);
            WishlistAdapter adapter = new WishlistAdapter(ctx,wishlists_list);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml(View view) {
        et_double_search = view.findViewById(R.id.et_double_search);
        location_sp = view.findViewById(R.id.sp_double_location);
        recyclerView = view.findViewById(R.id.wishlist_recycler);

        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SearchFragment searchFragment = new SearchFragment(ctx);
                Utility.setFragment(searchFragment,ctx);
            }
        });
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }
}
