package igrand.technology.info.eganacsi.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import igrand.technology.info.eganacsi.seller.response.myquoatation.MyQuotationEnquiry;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.seller.activity.MyleadViewmoreActivity;

/**
 * Created by kamlesh on 5/14/2018.
 */

public class MyQuoatationAdapter extends RecyclerView.Adapter<MyQuoatationAdapter.ViewHolder> {
    List<MyQuotationEnquiry> quote_list;
    Context ctx;
    int viewtype;

    public MyQuoatationAdapter(List<MyQuotationEnquiry> quote_list, Context ctx, int ViewType) {
        this.quote_list = quote_list;
        this.ctx = ctx;
        this.viewtype = ViewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_mylead, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.cust_name_tv.setText(quote_list.get(position).getCName());
        holder.service_name_tv.setText(quote_list.get(position).getServiceName());
        holder.date_name_tv.setText(quote_list.get(position).getDatetime());
        String pro_name = quote_list.get(position).getProductName();
        if (pro_name.equals("")){
            holder.service_name_tv.setText(quote_list.get(position).getServiceName());
        }else {
            holder.service_name_tv.setText(quote_list.get(position).getProductName());
        }
        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyQuotationEnquiry enquiry = quote_list.get(position);
                ctx.startActivity(new Intent(ctx, MyleadViewmoreActivity.class)
                        .putExtra("type", "Quotation")
                        .putExtra("data", enquiry)
                        .putExtra("title", "My Quotation Request View More"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return quote_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView cust_name_tv, service_name_tv, date_name_tv, viewmore_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            cust_name_tv = itemView.findViewById(R.id.tv_adplead_proname);
            service_name_tv = itemView.findViewById(R.id.tv_adplead_servname);
            date_name_tv = itemView.findViewById(R.id.tv_adplead_date);
            viewmore_tv = itemView.findViewById(R.id.tv_adplead_viewmore);
        }
    }
}
