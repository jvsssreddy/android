package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.BookServiceActivity;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.model.AllCategory;
import igrand.technology.info.eganacsi.model.FeaturedService;
import igrand.technology.info.eganacsi.model.HomeBean;
import igrand.technology.info.eganacsi.model.PopularService;
import igrand.technology.info.eganacsi.model.PopularService1;
import igrand.technology.info.eganacsi.model.Slider;
import igrand.technology.info.eganacsi.util.SessionManager;
import me.relex.circleindicator.CircleIndicator;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.Holder> implements View.OnClickListener {

    Context context;
    HomeBean homeBean;
    AllCategory allCategoryBean;
    private OnClickListener listener;
    SessionManager sessionManager;

    public HomeAdapter(Context context, HomeBean homeBean, AllCategory allCategory, OnClickListener listener) {
        this.context = context;
        this.homeBean = homeBean;
        this.listener = listener;
        allCategoryBean =allCategory;
        sessionManager = new SessionManager(context);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (viewType == ViewHandler.slider_view) {
            view = inflater.inflate(R.layout.home_slider,parent ,false);
        } else {
            view = inflater.inflate(R.layout.item_adapter, parent,false);
        }
        Holder holder = new Holder(view, viewType);
        return holder;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            listener.onAdapterClick(v, pos, getItemViewType(pos));
        }
    };

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        if (getItemViewType(position) == ViewHandler.slider_view) {
            holder.bt_home_bookservice.setTag(position);
            holder.bt_home_bookservice.setOnClickListener(clickListener);

            holder.bt_home_getquote.setTag(position);
            holder.bt_home_getquote.setOnClickListener(clickListener);
            HomePagerAdapter pagerAdapter = new HomePagerAdapter(new ArrayList<Slider>(homeBean.getData().getSlider()), context);
            holder.viewPager.setAdapter(pagerAdapter);
            holder.circle_page_indicator.setViewPager(holder.viewPager);


        }

        else if (getItemViewType(position) == ViewHandler.popular_product) {

            setPopularProduct(holder,position);

        }
        else if (getItemViewType(position) == ViewHandler.popular_service) {

            List<PopularService> pop_list = homeBean.getData().getPopularService();
            if (pop_list.size()!=0){
                HorizontalReclerAdapter horizontalReclerAdapter = new HorizontalReclerAdapter(context, ViewHandler.popular_service, new ArrayList(homeBean.getData().getPopularService()), listener);
                holder.recyclerView.setAdapter(horizontalReclerAdapter);
                holder.category_title.setText("Popular Service");
                holder.view_all_tv.setTag(position);
                holder.view_all_tv.setOnClickListener(clickListener);
            }
        }
        else if (getItemViewType(position) == ViewHandler.featured_product) {

            List<FeaturedService> list = homeBean.getData().getFeaturedService();
            if (list.size()!=0){
                HorizontalReclerAdapter horizontalReclerAdapter = new HorizontalReclerAdapter(context, ViewHandler.featured_product, new ArrayList(homeBean.getData().getFeaturedProduct()), listener);
                holder.recyclerView.setAdapter(horizontalReclerAdapter);
                holder.category_title.setText("Featured Product");
                holder.view_all_tv.setTag(position);
                holder.view_all_tv.setOnClickListener(clickListener);
            }

        } else if (getItemViewType(position) == ViewHandler.featured_sevice) {

            HorizontalReclerAdapter horizontalReclerAdapter = new HorizontalReclerAdapter(context, ViewHandler.featured_sevice, new ArrayList(homeBean.getData().getFeaturedService()), listener);
            holder.recyclerView.setAdapter(horizontalReclerAdapter);
            holder.category_title.setText("Featured Service");
            holder.view_all_tv.setTag(position);
            holder.view_all_tv.setOnClickListener(clickListener);
        } else if (getItemViewType(position) == ViewHandler.all_category) {

            GridAdapter gridAdapter = new GridAdapter(context, ViewHandler.all_category, new ArrayList(allCategoryBean.getData()), listener);
            holder.recyclerView.setBackgroundResource(R.color.white);
            holder.recyclerView.setAdapter(gridAdapter);
            holder.category_title.setText("All Category");
            holder.view_all_tv.setTag(position);
            holder.view_all_tv.setOnClickListener(clickListener);
        }
    }

    private void setPopularProduct(Holder holder, int position) {

        HorizontalReclerAdapter horizontalReclerAdapter = new HorizontalReclerAdapter(context, ViewHandler.popular_product, new ArrayList(homeBean.getData().getPopularProduct()), listener);
        holder.recyclerView.setAdapter(horizontalReclerAdapter);
        holder.category_title.setText("Popular Product");
        holder.view_all_tv.setTag(position);
        holder.view_all_tv.setOnClickListener(clickListener);
    }


    @Override
    public int getItemCount() {
        return ViewHandler.total_view_count;
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return ViewHandler.slider_view;
        }

        if (position == 1) {
            return ViewHandler.popular_product;
        }

        if (position == 2) {
            return ViewHandler.popular_service;
        }

        if (position == 3) {
            return ViewHandler.featured_product;
        }

        if (position == 4) {
            return ViewHandler.featured_sevice;
        }
        if (position == 5) {
            return ViewHandler.all_category;
        }

        return super.getItemViewType(position);
    }

    @Override
    public void onClick(View v) {

        final Intent intent;
        switch (v.getId()) {

            case R.id.bt_home_bookservice:
                intent = new Intent(context, BookServiceActivity.class);
                break;
            case R.id.bt_home_getquote:
                intent = new Intent(context, GetQuoteActivity.class);
                break;
        }
    }

    class Holder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView category_title, view_all_tv;
        AutoScrollViewPager viewPager;
        CircleIndicator circle_page_indicator;
        Button bt_home_getquote, bt_home_bookservice;

        public Holder(View itemView, int viewType) {

            super(itemView);
            if (viewType == ViewHandler.slider_view) {
                viewPager = itemView.findViewById(R.id.view_pager);
                bt_home_getquote = itemView.findViewById(R.id.bt_home_getquote);
                circle_page_indicator = itemView.findViewById(R.id.circle_page_indicator);
                viewPager.startAutoScroll(1000);
                viewPager.setInterval(2500);
                viewPager.setDirection(AutoScrollViewPager.LEFT);
                bt_home_bookservice = itemView.findViewById(R.id.bt_home_bookservice);

            } else if (viewType == ViewHandler.all_category) {
                category_title = itemView.findViewById(R.id.category_title);
                view_all_tv = itemView.findViewById(R.id.view_all_tv);
                recyclerView = itemView.findViewById(R.id.horizontal_recycler);

                view_all_tv.setVisibility(View.GONE);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
                recyclerView.setLayoutManager(gridLayoutManager);
                int padding = (int) context.getResources().getDimension(R.dimen._1sdp);
                recyclerView.setPadding(padding, padding, padding, padding);

            } else {
                category_title = itemView.findViewById(R.id.category_title);
                view_all_tv = itemView.findViewById(R.id.view_all_tv);
                recyclerView = itemView.findViewById(R.id.horizontal_recycler);
                LinearLayoutManager manager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
                recyclerView.setLayoutManager(manager);
            }

        }
    }


    public interface ViewHandler {

        int total_view_count = 6;
        int slider_view = 0;
        int popular_product = 1;
        int popular_service = 2;
        int featured_product = 3;
        int featured_sevice = 4;
        int all_category = 5;
    }


    public interface OnClickListener {
        void onAdapterClick(View view, int position, int viewType);
    }
}
