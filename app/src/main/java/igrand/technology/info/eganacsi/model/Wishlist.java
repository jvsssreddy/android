package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 5/2/2018.
 */

public class Wishlist
{
    private String id;
    private String p_id;
    private String name;
    private String image;
    private String type;
    private String comp_name;
    private String comp_address;

    public Wishlist(String id, String p_id, String name, String image, String type,String comp_name,String comp_address) {
        this.id = id;
        this.p_id = p_id;
        this.name = name;
        this.image = image;
        this.type = type;
        this.comp_name = comp_name;
        this.comp_address= comp_address;
    }

    public String getComp_name() {
        return comp_name;
    }

    public void setComp_name(String comp_name) {
        this.comp_name = comp_name;
    }

    public String getComp_address() {
        return comp_address;
    }

    public void setComp_address(String comp_address) {
        this.comp_address = comp_address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getP_id() {
        return p_id;
    }

    public void setP_id(String p_id) {
        this.p_id = p_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
