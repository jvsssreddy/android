package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.BookServiceActivity;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.adapter.HomeAdapter;
import igrand.technology.info.eganacsi.model.AllCategory;
import igrand.technology.info.eganacsi.model.FeaturedProduct;
import igrand.technology.info.eganacsi.model.FeaturedService;
import igrand.technology.info.eganacsi.model.HomeBean;
import igrand.technology.info.eganacsi.model.PopularProduct;
import igrand.technology.info.eganacsi.model.PopularService;
import igrand.technology.info.eganacsi.model.Slider;
import igrand.technology.info.eganacsi.network.NetworkUtils;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;

/**
 * Created by kamlesh on 4/4/2018.
 */

@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment implements HomeAdapter.OnClickListener, NetworkUtils.NetworkListner {
    View view;
    Context ctx;
    private AutoScrollViewPager viewPager;
    private ArrayList<Slider> sliderList;
    private HomeBean home_bean;
    private RecyclerView recyclerView;
    private ArrayList<PopularProduct> popular_list;
    private ArrayList<PopularService> popular_service_list;
    private ArrayList<FeaturedProduct> feature_product_list;
    private ArrayList<FeaturedService> feature_service_list;
    private ArrayList<AllCategory> all_category_list;

    SessionManager sessionManager;
    NetworkUtils networkUtils;
    private AllCategory all_category;

    EditText et_double_search;
    Spinner location_sp;
    String cust_id;

    @SuppressLint("ValidFragment")
    public HomeFragment(Context ctx) {
        this.ctx = ctx;
        sessionManager = new SessionManager(ctx);
        sessionManager.setData(SessionManager.KEY_PAGE, "home");
        cust_id = sessionManager.getData(SessionManager.KEY_CID);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkUtils = new NetworkUtils(ctx, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);
        initView(view);
        String name = sessionManager.getData(SessionManager.KEY_FNAME);
        Utility.setTitle(ctx, "Hi ," + name);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        getWishlist(cust_id);
        return view;

    }

    private void initView(View view) {

        et_double_search = view.findViewById(R.id.et_double_search);
        recyclerView = view.findViewById(R.id.home_recycler);
        location_sp = view.findViewById(R.id.sp_double_location);
        LinearLayoutManager manager = new LinearLayoutManager(ctx);
        recyclerView.setLayoutManager(manager);

        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SearchFragment searchFragment = new SearchFragment(ctx);
                setFragment(searchFragment);
            }
        });
    }

    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }

    private void setAdapter() {

        HomeAdapter adapter = new HomeAdapter(ctx, home_bean, all_category, this);
        recyclerView.setAdapter(adapter);
//        getLocation();
    }

    private void getLocation() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_LOCATION)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                        setAdapter();
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });

    }

    private void setResponse(String s) {
        sessionManager.setData(SessionManager.LOCATION_DATA, s);
        ArrayList<String> city_list = new ArrayList<>();
        city_list.add("Anywhere");
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");

            if (status.equals("1")) {

                JSONArray array_location = object.getJSONArray("Locations");
                for (int i = 0; i < array_location.length(); i++) {

                    JSONObject data_object = array_location.getJSONObject(i);
                    String city = data_object.getString("city_name");
                    city_list.add(city);
                }
                ArrayAdapter<String> city_adapter =
                        new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                location_sp.setAdapter(city_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAdapterClick(View view, int position, int viewType) {

        Log.e("onAdapterClick: ", position + "position view Type" + viewType);
        if (viewType == HomeAdapter.ViewHandler.slider_view) {

            sliderList = new ArrayList<Slider>(home_bean.getData().getSlider());
            handleSliderHolderClick(sliderList, view, position);
        } else if (viewType == HomeAdapter.ViewHandler.popular_product) {
            popular_list = new ArrayList<PopularProduct>(home_bean.getData().getPopularProduct());
            int id = view.getId();
            Intent intent;
            switch (id) {

                case R.id.detail_bt:
                    PopularProduct product_bean = popular_list.get(position);
                    String product_id = product_bean.getProductId();
                    intent = new Intent(ctx, ProductDetailActivity.class);
                    intent.putExtra("product_id", product_id);
                    startActivity(intent);
                    break;

                case R.id.view_all_tv:
                    ViewAllFragment viewAllFragment = new ViewAllFragment(ctx, "popular product");
                    setFragment(viewAllFragment);
                    break;

                case R.id.getquot_bt:
                    startActivity(new Intent(ctx, GetQuoteActivity.class));
                    break;

                case R.id.like_image:
                    PopularProduct product_bean1 = popular_list.get(position);
                    String product_id1 = product_bean1.getProductId();
                    if (wishlist_idlist.contains(product_id1 + "p")) {
                        dislikeproductandService(cust_id, product_id1, "", view, "p");
                    } else {
                        likeproductandService(cust_id, product_id1, "", view, "p");
                    }
                    break;
            }

        } else if (viewType == HomeAdapter.ViewHandler.popular_service) {

            popular_service_list = new ArrayList<PopularService>(home_bean.getData().getPopularService());
            int id = view.getId();
            Intent intent;
            switch (id) {

                case R.id.getquot_bt:
                    startActivity(new Intent(ctx, BookServiceActivity.class));
                    break;

                case R.id.detail_bt:
                    PopularService service = popular_service_list.get(position);
                    intent = new Intent(ctx, ServiceDetailActivity.class);
                    intent.putExtra("Service", service.getServiceId());
                    startActivity(intent);
                    break;

                case R.id.view_all_tv:
                    ViewAllFragment viewAllFragment = new ViewAllFragment(ctx, "popular service");
                    setFragment(viewAllFragment);
                    break;

                case R.id.like_image:
                    String service_id1 = popular_service_list.get(position).getServiceId();
                    if (wishlist_idlist.contains(service_id1 + "s")) {
                        dislikeproductandService(cust_id, "", service_id1, view, "s");
                    } else {
                        likeproductandService(cust_id, "", service_id1, view, "s");
                    }
                    break;
            }
        } else if (viewType == HomeAdapter.ViewHandler.featured_product) {

            feature_product_list = new ArrayList<FeaturedProduct>(home_bean.getData().getFeaturedProduct());
            int id = view.getId();
            switch (id) {

                case R.id.view_all_tv:
                    ViewAllFragment viewAllFragment = new ViewAllFragment(ctx, "Featured Product");
                    setFragment(viewAllFragment);
                    break;

                case R.id.detail_bt:
                    FeaturedProduct featuredProduct = feature_product_list.get(position);
                    String product_id = featuredProduct.getProductId();
                    startActivity(new Intent(ctx, ProductDetailActivity.class)
                            .putExtra("product_id", product_id));
                    break;

                case R.id.getquot_bt:
                    startActivity(new Intent(ctx, GetQuoteActivity.class));
                    break;

                case R.id.like_image:
                    FeaturedProduct featuredProduct1 = feature_product_list.get(position);
                    String product_id1 = featuredProduct1.getProductId();
                    if (wishlist_idlist.contains(product_id1 + "p")) {
                        dislikeproductandService(cust_id, product_id1, "", view, "p");
                    } else {
                        likeproductandService(cust_id, product_id1, "", view, "p");
                    }
                    break;
            }

        } else if (viewType == HomeAdapter.ViewHandler.featured_sevice) {

            feature_service_list = new ArrayList<FeaturedService>(home_bean.getData().getFeaturedService());
            int id = view.getId();
            switch (id) {

                case R.id.getquot_bt:
                    startActivity(new Intent(ctx, BookServiceActivity.class));
                    break;

                case R.id.detail_bt:
                    FeaturedService service = feature_service_list.get(position);
                    Intent intent = new Intent(ctx, ServiceDetailActivity.class);
                    intent.putExtra("Service", service.getServiceId());
                    startActivity(intent);
                    break;

                case R.id.view_all_tv:
                    ViewAllFragment viewAllFragment = new ViewAllFragment(ctx, "Featured service");
                    setFragment(viewAllFragment);
                    break;

                case R.id.like_image:
                    String service_id1 = feature_service_list.get(position).getServiceId();
                    if (wishlist_idlist.contains(service_id1 + "s")) {
                        dislikeproductandService(cust_id, "", service_id1, view, "s");
                    } else {
                        likeproductandService(cust_id, "", service_id1, view, "s");
                    }
                    break;
            }

        } else if (viewType == HomeAdapter.ViewHandler.all_category) {

            SubAllCategoryFragment subAllCategoryFragment = new SubAllCategoryFragment(ctx, "All category", position);
            setFragment(subAllCategoryFragment);
        }
    }

    private void likeproductandService(String cust_id, final String product_id1, final String service_id1, final View view, final String type) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADD_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                Utility.toastView(ctx, "Added to wishlist");
                                if (type.equals("p")) {
                                    wishlist_idlist.add(product_id1 + "p");
                                } else {
                                    wishlist_idlist.add(service_id1 + "s");
                                }
                                ImageView like_iv = view.findViewById(R.id.like_image);
                                like_iv.setImageResource(R.drawable.like_red_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void dislikeproductandService(String cust_id, final String product_id1, final String service_id1, final View view, final String type) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_DELETE_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                if (type.equals("p")) {
                                    wishlist_idlist.remove(product_id1 + "p");
                                } else {
                                    wishlist_idlist.remove(service_id1 + "s");
                                }
                                Utility.toastView(ctx, "Remove to wishlist");
                                ImageView like_iv = view.findViewById(R.id.like_image);
                                like_iv.setImageResource(R.drawable.like_fill_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void handleSliderHolderClick(ArrayList<Slider> sliderList, View view, int position) {

        int id = view.getId();
        Intent intent;
        switch (id) {

            case R.id.bt_home_bookservice:
                intent = new Intent(ctx, BookServiceActivity.class);
                startActivity(intent);
                break;
            case R.id.bt_home_getquote:
                intent = new Intent(ctx, GetQuoteActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onResult(String result, int req_code) {

        Gson gson = new Gson();
        if (req_code == NetworkUtils.NetworkListner.Home_Data_Req) {

            home_bean = gson.fromJson(result, HomeBean.class);
            Log.e("onPostExecute: ", home_bean.getMessage());
            String all_cate = sessionManager.getData(SessionManager.HOME_API_ALL_CATE);
            try {
                onResult(all_cate, CategoryReq);
            } catch (Exception e) {
                e.printStackTrace();
                networkUtils.call(CategoryReq);
            }
        } else if (req_code == NetworkUtils.NetworkListner.CategoryReq) {
            all_category = gson.fromJson(result, AllCategory.class);
            Log.e("onPostExecute: ", all_category.getMessage());
//            setAdapter();
            getLocation();
        }
    }

    private void getWishlist(String cust_id) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        sessionManager.setData(SessionManager.KEY_WISHLIST_DATA, response.toString());
                        setWishlistResponse(response.toString());
                        String products = sessionManager.getData(SessionManager.HOME_API_DATA);
                        try {
                            onResult(products, Home_Data_Req);
                        } catch (Exception e) {
                            e.printStackTrace();
                            networkUtils.call(Home_Data_Req);
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                        String products = sessionManager.getData(SessionManager.HOME_API_DATA);
                        try {
                            onResult(products, Home_Data_Req);
                        } catch (Exception e) {
                            e.printStackTrace();
                            networkUtils.call(Home_Data_Req);
                        }
                    }
                });
    }

    ArrayList<String> wishlist_idlist;

    private void setWishlistResponse(String s) {

        wishlist_idlist = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")){
                JSONObject data_obj = object.getJSONObject("data");
                JSONArray wish_arry = data_obj.getJSONArray("wishlist");
                for (int i = 0; i < wish_arry.length(); i++) {

                    JSONObject datajson = wish_arry.getJSONObject(i);
                    String p_id = datajson.getString("p_id");
                    String type = datajson.getString("type");
                    if (type.equals("service")) {
                        wishlist_idlist.add(p_id + "s");
                    } else {
                        wishlist_idlist.add(p_id + "p");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
