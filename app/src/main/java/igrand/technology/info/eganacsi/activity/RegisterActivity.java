package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import igrand.technology.info.eganacsi.ApiClient;
import igrand.technology.info.eganacsi.ApiInterface;
import igrand.technology.info.eganacsi.model.RegisterResponse;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    EditText fname_et,lname_et,email_et,moble_et,password_et,conf_password_et;
    Button signu_bt;
    ImageView facebook_iv,gplus_iv;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    ApiInterface apiInterface;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    //    facebook login
    LoginButton facebbokloginButton;
    CallbackManager callbackManager;
    private static final String EMAIL = "email";

    // google login
    GoogleSignInClient mGoogleSignInClient;
    public static final int RC_SIGN_IN = 12345;

    String fname,lname,mobile,email,password,con_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initXml();
        intObj();
        googleLogin();
    }

    private void googleLogin() {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

//        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
//        updateUI(account);
    }

    private void intObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void initXml() {
        ctx = this;
        facebbokloginButton = (LoginButton) findViewById(R.id.login_button);
        fname_et = (EditText)findViewById(R.id.et_reg_fname);
        lname_et = (EditText)findViewById(R.id.et_reg_lname);
        email_et = (EditText)findViewById(R.id.et_reg_email);
        moble_et = (EditText)findViewById(R.id.et_reg_mobile);
        password_et = (EditText)findViewById(R.id.et_reg_password);
        conf_password_et = (EditText)findViewById(R.id.et_reg_conf_password);
        signu_bt = (Button) findViewById(R.id.bt_reg_signup);
        facebook_iv = (ImageView)findViewById(R.id.iv_reg_facebook);
        gplus_iv = (ImageView)findViewById(R.id.iv_reg_gplus);

        signu_bt.setOnClickListener(this);
        facebook_iv.setOnClickListener(this);
        gplus_iv.setOnClickListener(this);

        apiInterface= ApiClient.getClient1().create(ApiInterface.class);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.iv_reg_facebook:
                facebookLogin();
                break;

            case R.id.iv_reg_gplus:
                signIn();
                break;

            case R.id.bt_reg_signup:
                boolean internet = connectionDetector.isConnected();
                if (internet){
                    getData();
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.no_internet));
                }
                break;

        }
    }

    private void getData() {
        fname = fname_et.getText().toString();
       lname = lname_et.getText().toString();
        mobile = moble_et.getText().toString();
        email = email_et.getText().toString();
        password = password_et.getText().toString();
         con_password= conf_password_et.getText().toString();

        if (fname.equals("")||lname.equals("")||mobile.equals("")||email.equals("")||password.equals("")||con_password.equals("")){
//            Utility.toastView(ctx,"Enter All Fields");
            Toast.makeText(ctx, "Enter All Fields", Toast.LENGTH_SHORT).show();
        }else if (!email.matches(emailPattern)){
//            Utility.toastView(ctx,"Invalid Username");
            Toast.makeText(ctx, "Invalid Username", Toast.LENGTH_SHORT).show();
        }else if (!password.equals(con_password)){
//            Utility.toastView(ctx,"confirm Password is not matched");
            Toast.makeText(ctx,"confirm Password is not matched", Toast.LENGTH_SHORT).show();
        }else {
            login();

        }





//        getRegister(fname,lname,mobile,email,password,con_password);
//        if (fname.equals("")||lname.equals("")||mobile.equals("")||email.equals("")||password.equals("")||con_password.equals("")){
//            Utility.toastView(ctx,"Enter All Fields");
//        }else if (!email.matches(emailPattern)){
//            Utility.toastView(ctx,"Invalid Username");
//        }else if (!password.equals(con_password)){
//            Utility.toastView(ctx,"confirm Password is not matched");
//        }else {
//
//        }
    }

    private void login(){

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();
        Call<RegisterResponse> call=apiInterface.Regester(fname,lname,email,password,mobile);
        call.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                try {
                    if (response.isSuccessful());
                    RegisterResponse registerResponse=response.body();
                    if (registerResponse.status==1){
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, registerResponse.message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                    }else if (registerResponse.status==0){
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, registerResponse.message, Toast.LENGTH_SHORT).show();
                    }

                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, "error", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getRegister(String fname, String lname, String mobile, String email, String password, String con_password) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_REGISTER)
                .addBodyParameter("firstname", fname)
                .addBodyParameter("lastname", lname)
                .addBodyParameter("password", password)
                .addBodyParameter("mobile_no", mobile)
                .addBodyParameter("email", email)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        setResponse("error");
                    }
                });
    }

    private void setResponse(String s) {

        if (s.equals("error")){
            Utility.toastView(ctx,ctx.getString(R.string.something_went));
        }else if (!s.equals("")){
            try {
                JSONObject object = new JSONObject(s);
                String status = object.getString("status");
                String messgae= object.getString("message");

                if (messgae.equals("Email already existed")){
                    Utility.toastView(ctx,"Email already existed");
                }else if (messgae.equals("User Successfully Register")){

                    Utility.toastView(ctx,"Successfully Register");
                    startActivity(new Intent(ctx,LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_found));
        }
    }

    // facebook login

    private void facebookLogin() {

        callbackManager = CallbackManager.Factory.create();
        facebbokloginButton.setReadPermissions(Arrays.asList(EMAIL));
        facebbokloginButton.setReadPermissions("user_friends");
        facebbokloginButton.setReadPermissions("public_profile");
        facebbokloginButton.setReadPermissions("email");
        facebbokloginButton.setReadPermissions("user_birthday");
        // If you are using in a fragment, call facebbokloginButton.setFragment(this);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", EMAIL));

        // Callback registration
        facebbokloginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                Toast.makeText(ctx, "login button  success", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(getApplicationContext(), "cancel Login", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(getApplicationContext(), "error in Login", Toast.LENGTH_SHORT).show();
            }
        });


//        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                AccessToken.getCurrentAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        try {
//                                            Util.toastView(ctx,object.toString());
                                            Log.d("TAG", "fb json object: " + object);
                                            Log.d("TAG", "fb graph response: " + response);

                                            String facebook_id = object.getString("id");
                                            String first_name = object.getString("first_name");
                                            String last_name = object.getString("last_name");

                                            String email = "";
                                            if (object.has("email")) {
                                                email = object.getString("email");
                                            }
                                            getSocialLogin("fb",first_name,last_name,email,facebook_id,"");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,email,gender,birthday"); // id,first_name,last_name,email,gender,birthday,cover,picture.type(large)
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Toast.makeText(ctx, "facebook login cancel by user", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        String errt = exception.toString();
                        Toast.makeText(ctx, exception.toString() + "", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void getSocialLogin(String type,final String first_name, final String last_name, final String email, String fb_id,String gp_id) {

        String social_type ="";
        if (type.equals("fb")){
            social_type = "0";
        }else {
            social_type = "1";
        }
        String token ="";
        if (type.equals("fb")){
            social_type = "0";
            token = fb_id;
        }else {
            social_type = "1";
            token = email;
        }
        AndroidNetworking.post(WebApi.URL_SOCIAL_LOGIN)
                .addBodyParameter("social_type", social_type)
                .addBodyParameter("firstname", first_name)
                .addBodyParameter("email", email)
                .addBodyParameter("token", token)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")){

                                JSONObject object = response.getJSONObject("data");
                                String c_id= object.getString("c_id");
                                String customer_id= object.getString("customer_id");

                                sessionManager.createLoginSession(first_name, last_name, "", email, c_id, customer_id);
                                startActivity(new Intent(ctx, MainActivity.class));
                                finish();
                            }else {
                                Utility.toastView(ctx,ctx
                                        .getString(R.string.something_went));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    // google login
    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Register Activity", "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }

    private void updateUI(GoogleSignInAccount account) {

        Log.e("Register activity", "display name: " + account.getDisplayName());
        String personName = account.getDisplayName();
        String personPhotoUrl = account.getPhotoUrl().toString();
        String email = account.getEmail();
        String fname = account.getGivenName();
        String g_id = account.getId();
//        Utility.toastView(ctx,personName+" "+email);

        getSocialLogin("gp",fname,"",email,"",g_id);
    }
}
