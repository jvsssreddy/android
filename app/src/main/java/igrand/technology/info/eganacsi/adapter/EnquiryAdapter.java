package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.EnquriyProductDetails;
import igrand.technology.info.eganacsi.model.Enquiry;

/**
 * Created by kamlesh on 4/13/2018.
 */

public class EnquiryAdapter extends RecyclerView.Adapter<EnquiryAdapter.MyViewHolder>
{
    private List<Enquiry> enqiry_List;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView date_tv,time_tv,enq_id_tv,proname_tv,sellername_tv,viewmore_tv;

        public MyViewHolder(View view) {
            super(view);
            date_tv = view.findViewById(R.id.tv_adpmyenq_date);
            time_tv = view.findViewById(R.id.tv_adpmyenq_time);
            enq_id_tv = view.findViewById(R.id.tv_adpmyenq_enq_id);
            proname_tv = view.findViewById(R.id.tv_adpmyenq_proname);
            sellername_tv = view.findViewById(R.id.tv_adpmyenq_sellername);
            viewmore_tv = view.findViewById(R.id.tv_adpmyenq_viewmore);
        }
    }


    public EnquiryAdapter(List<Enquiry> enqiry_List, Context ctx) {
        this.enqiry_List = enqiry_List;
        this.context=ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adp_myenquries, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String date_time = enqiry_List.get(position).getDate();
        String date = date_time.substring(0,10);
        String time = date_time.substring(11,18);
        holder.date_tv.setText("Date : "+date);
        holder.time_tv.setText("Time : "+time);
        holder.enq_id_tv.setText("Enquiry ID : "+enqiry_List.get(position).getEn_id());
        holder.sellername_tv.setText("Seller Name : "+enqiry_List.get(position).getSeller_name());
        holder.proname_tv.setText("Product Name : "+enqiry_List.get(position).getProduct_name());



        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPreferences = context.getSharedPreferences("enquiry", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("eid",enqiry_List.get(position).getEn_id());
                editor.commit();

                Intent intent=new Intent(context,EnquriyProductDetails.class);
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return enqiry_List.size();
    }
}
