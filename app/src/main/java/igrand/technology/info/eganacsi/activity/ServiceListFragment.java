package igrand.technology.info.eganacsi.activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.service_list.ServiceListBean;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.ServiceListAdapter;
import igrand.technology.info.eganacsi.model.service_list.Service;
import igrand.technology.info.eganacsi.network.NetworkUtils;

/**
 * Created by kamlesh on 4/22/2018.
 */

public class ServiceListFragment extends Fragment implements NetworkUtils.NetworkListner {

    Context context;
    NetworkUtils networkUtils;
    private RecyclerView recycler;
    private ArrayList<Service> mList=new ArrayList<>();
    private ServiceListAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        networkUtils = new NetworkUtils(context, this);
        networkUtils.call(Service_List_Req);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.all_product_list, null);
        init(view);
        return view;
    }

    private void init(View view) {
        recycler =(RecyclerView) view.findViewById(R.id.recycler);
        LinearLayoutManager manager = new LinearLayoutManager(context);
        recycler.setLayoutManager(manager);
    }


    @Override
    public void onResult(String result, int req_code) {
        if (req_code == Service_List_Req){
            Gson gson = new Gson();
            ServiceListBean bean = gson.fromJson(result,ServiceListBean.class);
            mList = new ArrayList<>(bean.getData().getPopularService());
            adapter = new ServiceListAdapter(context,mList);
            recycler.setAdapter(adapter);
        }
    }


}
