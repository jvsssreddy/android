package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.MainActivity;
import igrand.technology.info.eganacsi.adapter.HomeAdapter;
import igrand.technology.info.eganacsi.adapter.Product_list_Adapter;
import igrand.technology.info.eganacsi.model.AllCategory;
import igrand.technology.info.eganacsi.model.HomeBean;
import igrand.technology.info.eganacsi.model.PopularPruducts;
import igrand.technology.info.eganacsi.model.ProductDetails.ProductDetail;
import igrand.technology.info.eganacsi.model.ProductDetails.ProductDetailBean;
import igrand.technology.info.eganacsi.model.ProductListData;
import igrand.technology.info.eganacsi.model.ProductListResponse;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.network.NetworkUtils;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static igrand.technology.info.eganacsi.network.NetworkUtils.NetworkListner.CategoryReq;

/**
 * Created by administator on 6/23/2018.
 */

@SuppressLint("ValidFragment")
public class ProductListingFragment extends Fragment implements HomeAdapter.OnClickListener, NetworkUtils.NetworkListner  {
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;
    igrand.technology.info.eganacsi.ApiInterface apiInterface;
    Bundle bundle;
    private HomeBean home_bean;
    private AllCategory all_category;
    String id;
    TextView textView1,textView2,textView3,textView4;
    Spinner location_sp;
    Context ctx;
    EditText et_double_search;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    NetworkUtils networkUtils;

    @SuppressLint("ValidFragment")
    public ProductListingFragment(Context ctx) {
        this.ctx = ctx;
        sessionManager = new SessionManager(ctx);
//         sessionManager.setData(SessionManager.KEY_PAGE, "prod");
        connectionDetector = new ConnectionDetector();

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        networkUtils = new NetworkUtils(ctx, this);
    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.productlisting,container,false);
        initView(v);
        setLocation();
        recyclerView=(RecyclerView)v.findViewById(R.id.product_list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("userData", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("subid","");
        String name1 = sharedPreferences.getString("namee","");
        Utility.setTitle(getContext(), name1);

        apiInterface = igrand.technology.info.eganacsi.ApiClient.getClient1().create(igrand.technology.info.eganacsi.ApiInterface.class);
        Call<ProductListResponse> call = apiInterface.productListing(name);
        call.enqueue(new Callback<ProductListResponse>() {
            @Override
            public void onResponse(Call<ProductListResponse> call, Response<ProductListResponse> response) {
                try{
                    if (response.isSuccessful());
                    ProductListResponse listResponse = response.body();
                    if (listResponse.status== 1) {
                        List<ProductListData> productListData = listResponse.data;
                        recyclerView.setAdapter(new Product_list_Adapter(productListData, getContext()));
                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<ProductListResponse> call, Throwable t) {
                Toast.makeText(getContext(), "ERROR", Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }

    private void setAdapter() {

        HomeAdapter adapter = new HomeAdapter(ctx, home_bean, all_category, this);
        recyclerView.setAdapter(adapter);
//        getLocation();
    }

//    private void getLocation() {
//
//        final ProgressDialog progressDialog = new ProgressDialog(ctx);
//        progressDialog.setMessage("wait...");
//        progressDialog.show();
//
//        AndroidNetworking.get(WebApi.URL_LOCATION)
//                .setTag("test")
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .getAsJSONObject(new JSONObjectRequestListener() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        progressDialog.dismiss();
//                        setResponse(response.toString());
//                        setAdapter();
//                    }
//
//                    @Override
//                    public void onError(ANError error) {
//                        progressDialog.dismiss();
//                        Utility.toastView(ctx, error.toString());
//                        // handle error
//                    }
//                });
//
//    }

    private void setLocation() {
        String location = sessionManager.getData(SessionManager.LOCATION_DATA);
        if (!location.equals(null)){

            ArrayList<String> city_list = new ArrayList<>();
            city_list.add("Anywhere");
            try {
                JSONObject object = new JSONObject(location);
                String status = object.getString("status");

                if (status.equals("1")) {

                    JSONArray array_location = object.getJSONArray("Locations");
                    for (int i = 0; i < array_location.length(); i++) {

                        JSONObject data_object = array_location.getJSONObject(i);
                        String city = data_object.getString("city_name");
                        city_list.add(city);
                    }
                    ArrayAdapter<String> city_adapter =
                            new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                    location_sp.setAdapter(city_adapter);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
//    private void setResponse(String s) {
//        sessionManager.setData(SessionManager.LOCATION_DATA, s);
//        ArrayList<String> city_list = new ArrayList<>();
//        city_list.add("Anywhere");
//        try {
//            JSONObject object = new JSONObject(s);
//            String status = object.getString("status");
//
//            if (status.equals("1")) {
//
//                JSONArray array_location = object.getJSONArray("Locations");
//                for (int i = 0; i < array_location.length(); i++) {
//
//                    JSONObject data_object = array_location.getJSONObject(i);
//                    String city = data_object.getString("city_name");
//                    Toast.makeText(ctx, city, Toast.LENGTH_SHORT).show();
//                    city_list.add(city);
//                }
//                ArrayAdapter<String> city_adapter =
//                        new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);
//
//                location_sp.setAdapter(city_adapter);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }
    private void setFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fl_main_framelayout, fragment);
        fragmentTransaction.addToBackStack("");
        fragmentTransaction.commit();
    }


    @Override
    public void onAdapterClick(View view, int position, int viewType) {

    }

    @Override
    public void onResult(String result, int req_code) {

        Gson gson = new Gson();
        if (req_code == NetworkUtils.NetworkListner.Home_Data_Req) {

            home_bean = gson.fromJson(result, HomeBean.class);
            Log.e("onPostExecute: ", home_bean.getMessage());
            String all_cate = sessionManager.getData(SessionManager.HOME_API_ALL_CATE);
            try {
                onResult(all_cate, CategoryReq);
            } catch (Exception e) {
                e.printStackTrace();
                networkUtils.call(CategoryReq);
            }
        } else if (req_code == CategoryReq) {
            all_category = gson.fromJson(result, AllCategory.class);
            Log.e("onPostExecute: ", all_category.getMessage());
//            setAdapter();
//            getLocation();
        }
    }

    private void initView(View view) {

        et_double_search = view.findViewById(R.id.et_double_search);

        location_sp = view.findViewById(R.id.sp_double_location);

        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SearchFragment searchFragment = new SearchFragment(ctx);
                setFragment(searchFragment);
            }
        });
    }
}
