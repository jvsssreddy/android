package igrand.technology.info.eganacsi.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.model.MyProductsList;


/**
 * Created by kamlesh on 5/14/2018.
 */

public class MySellerProductAdapter extends RecyclerView.Adapter<MySellerProductAdapter.ViewHolder> {
    List<MyProductsList>  products_list;
    Context ctx;
    int viewtype;
    private OnClickListener listener;

    public MySellerProductAdapter(List<MyProductsList> products_list, Context ctx, int viewtype) {
        this.products_list = products_list;
        this.ctx = ctx;
        this.listener= listener;
        this.viewtype= viewtype;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_sellerproduct, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.decr_tv.setText(products_list.get(position).productName);
        Glide.with(ctx).load(products_list.get(position).image)
                .into(holder.product_image)
                .onLoadFailed(null, ctx.getDrawable(R.drawable.no_image));

        holder.edit_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                MyProductsList popularProduct = products_list.get(position);
//                ctx.startActivity(new Intent(ctx, SellerAddProductActivity.class)
//                        .putExtra("type","update")
//                        .putExtra("data", (Parcelable) popularProduct));
            }
        });
        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, ProductDetailActivity.class)
                .putExtra("product_id",products_list.get(position).productId));
            }
        });
    }

    @Override
    public int getItemCount() {
        return products_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView decr_tv, edit_tv, viewmore_tv;
        ImageView product_image;


        public ViewHolder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.iv_adpsellerproduct_image);
            decr_tv = itemView.findViewById(R.id.tv_adpsellerproduct_descr);
            edit_tv = itemView.findViewById(R.id.tv_adpsellerproduct_edit);
            viewmore_tv = itemView.findViewById(R.id.tv_adpsellerproduct_viewmore);
        }
    }

    public interface OnClickListener{
        void OnSellerClicklistener(View view,int position,int Viewtype);
    }
}
