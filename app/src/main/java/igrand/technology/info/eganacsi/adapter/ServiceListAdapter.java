package igrand.technology.info.eganacsi.adapter;



import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.service_list.Service;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 4/22/2018.
 */
public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.Holder> {


    Context context;
    ArrayList<Service> mList;

    public ServiceListAdapter(Context context, ArrayList<Service> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_service_wishlist, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {

        Service model = mList.get(position);
        Glide.with(context).load(model.getImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
        holder.product_title.setText(model.getTitle());
        holder.product_man.setText(model.getProvider());
        holder.product_address.setText(model.getLocation());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        ImageView product_image;
        TextView product_title;
        TextView product_man;
        TextView product_address;
        ImageView like_iamge;
        TextView get_quote;
        TextView view_detail;

        public Holder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);
            product_man = itemView.findViewById(R.id.product_man);
            product_address = itemView.findViewById(R.id.product_address);
//            like_iamge = itemView.findViewById(R.id.like_iamge);
            get_quote = itemView.findViewById(R.id.get_quote);
            view_detail = itemView.findViewById(R.id.view_detail);
        }
    }
}

