package igrand.technology.info.eganacsi.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.model.ProductDetails.ProductDetail;
import igrand.technology.info.eganacsi.model.ProductDetails.ProductDetailBean;
import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.network.NetworkUtils;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamlesh on 4/13/2018.
 */
public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    boolean like = false;
    ImageView back_iv, search_iv, menu_iv, main_iv, like_iv;
    TextView name_tv, new_price_tv, old_price_tv, save_tv, dealer_tv,
            address_tv, rating_tv, no_person_tv, sellername_tv, call_tv, availble_tv, getquote_tv, add_wishlist_tv;
    ArrayList<String> wishlists_list;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    ProductDetail detail;
    NetworkUtils networkUtils;
    public static final int PRODUCT_DETAILS_REQ = 1;
    String product_id, cust_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);
        initXml();
        initObj();
        product_id = getIntent().getStringExtra("product_id");
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getProductDetails(product_id);
//          getDetailsUsingRetro(product_id);
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);

        cust_id = sessionManager.getData(SessionManager.KEY_CID);
    }

    private void getProductDetails(String product_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("loading...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_PRODUCT_DETAILS)
                .addBodyParameter("product_id", product_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject object = new JSONObject(response.toString());
                            String status = object.getString("status");
                            if (status.equals("1")) {
                                setResponse(response.toString());
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, anError.toString());
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void setResponse(String s) {

        try {
            JSONObject response = new JSONObject(s);
            String status = response.getString("status");

            if (status.equals("1")) {

                JSONObject data_object = response.getJSONObject("data");

                String id = data_object.getString("product_id");
                String name = data_object.getString("product_name ");
                String image = data_object.getString("product_image");
                String product_price = data_object.getString("product_price");
                String discount_price = data_object.getString("discount_price");
                String discount_percent = data_object.getString("discount_percent");
//                String decription = data_object.getString("description");
                String address = data_object.getString("address");
                String seller_name = data_object.getString("seller_name");
                String phone = data_object.getString("phone");
                String city = data_object.getString("city");
                String area = data_object.getString("area");
                String rating = data_object.getString("rating");
                String noofperson = data_object.getString("no_of_person");
                String availblity = data_object.getString("availability");

                product_id = id;
                setData(name, image, product_price, discount_price, discount_percent, address, seller_name, phone, city, area, rating, noofperson, availblity);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            getWishlist(cust_id);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ResourceAsColor")
    private void setData(String name, String image, String product_price, String discount_price, String discount_percent,
                         String address, String seller_name, String phone, String city, String area, String rating,
                         String noofperson, String availblity) {

        name_tv.setText(name);
        Glide.with(ctx).load(image).into(main_iv).onLoadFailed(null, ctx.getDrawable(R.drawable.no_image));
        old_price_tv.setText(product_price);
        new_price_tv.setText(discount_price + "/piece");
        address_tv.setText(address + " " + area + " " + city);
        sellername_tv.setText(seller_name);
        rating_tv.setText(rating + "/5");
        no_person_tv.setText(noofperson);
        call_tv.setText(phone);

        old_price_tv.setPaintFlags(old_price_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (availblity.equals("in_stock")) {
            availble_tv.setText(availblity);
        } else {
            availble_tv.setTextColor(R.color.red_color);
            availble_tv.setText("Out of stock");
        }

        if (!product_price.equals("") || !discount_price.equals("")) {

            float p_pr = Float.parseFloat(product_price);
            float d_pr = Float.parseFloat(discount_price);
            float save = p_pr - d_pr;
            save_tv.setText("save ₹ " + save);
        }
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_productdetail_back);
        search_iv = findViewById(R.id.iv_productdetail_search);
        like_iv = findViewById(R.id.iv_productdetail_like);
        menu_iv = findViewById(R.id.iv_productdetail_menu);
        main_iv = findViewById(R.id.iv_productdetail_image);
        name_tv = findViewById(R.id.tv_productdetail_name);
        new_price_tv = findViewById(R.id.tv_productdetail_price);
        old_price_tv = findViewById(R.id.old_price_tv);
        save_tv = findViewById(R.id.tv_productdetail_savemoney);
        dealer_tv = findViewById(R.id.tv_productdetail_dealer);
        address_tv = findViewById(R.id.tv_productdetail_address);
        rating_tv = findViewById(R.id.tv_productdetail_rating);
        no_person_tv = findViewById(R.id.tv_productdetail_noofperson);
        sellername_tv = findViewById(R.id.tv_productdetail_sellername);
        call_tv = findViewById(R.id.tv_productdetail_sellercontact);
        availble_tv = findViewById(R.id.tv_productdetail_instock);
        getquote_tv = findViewById(R.id.tv_productdetail_getquote);
        add_wishlist_tv = findViewById(R.id.tv_productdetail_addwishlist);

        call_tv.setOnClickListener(this);
        getquote_tv.setOnClickListener(this);
        back_iv.setOnClickListener(this);
        like_iv.setOnClickListener(this);
        add_wishlist_tv.setOnClickListener(this);
        search_iv.setOnClickListener(this);
    }

    public void getDetailsUsingRetro(String product_id) {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ProductDetailBean> call = apiService.getNewProductDetail(product_id);
        call.enqueue(new Callback<ProductDetailBean>() {
            @Override
            public void onResponse(Call<ProductDetailBean> call, Response<ProductDetailBean> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                detail = response.body().getData();
                Log.e("", "onResponse: " + detail.getProductId());
                Toast.makeText(ctx, detail.getProductId(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ProductDetailBean> call, Throwable t) {

                Toast.makeText(ctx, "error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestCallPermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            calling();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void calling() {
        String no = call_tv.getText().toString();
        Intent inte = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + no));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(inte);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.tv_productdetail_sellercontact:
                requestCallPermission();
                break;

            case R.id.tv_productdetail_getquote:
                startActivity(new Intent(ctx, GetQuoteActivity.class));
                break;

            case R.id.iv_productdetail_back:
                finish();
                break;

            case R.id.iv_productdetail_search:
                sessionManager.setBooleanData(SessionManager.KEY_MAIN_ACTIVITY_TYPE,true);
                startActivity(new Intent(ctx,MainActivity.class));
                break;

            case R.id.tv_productdetail_addwishlist:
                String cust_id = sessionManager.getData(SessionManager.KEY_CID);

                if (like) {
                    dislikeproductandService(cust_id, product_id, "", v);
                } else {
                    likeproductandService(cust_id, product_id, "", v);
                }
                break;

            case R.id.iv_productdetail_like:
                String c_id = sessionManager.getData(SessionManager.KEY_CID);

                if (like) {
                    dislikeproductandService(c_id, product_id, "", v);
                } else {
                    likeproductandService(c_id, product_id, "", v);
                }
                break;
        }
    }

    private void dislikeproductandService(String cust_id, final String product_id1, final String service_id1, final View view) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_DELETE_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                like = false;
                                Utility.toastView(ctx, "Remove to wishlist");
                                like_iv.setImageResource(R.drawable.like_fill_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void likeproductandService(String cust_id, String product_id1, String service_id1, final View view) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADD_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                like = true;
                                Utility.toastView(ctx, "Added to wishlist");
                                like_iv.setImageResource(R.drawable.like_red_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void getWishlist(String cust_id) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setwishResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setwishResponse(String s) {

        wishlists_list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONObject data_obj = object.getJSONObject("data");
            JSONArray wish_arry = data_obj.getJSONArray("wishlist");
            for (int i = 0; i < wish_arry.length(); i++) {

                JSONObject datajson = wish_arry.getJSONObject(i);
                String p_id = datajson.getString("p_id");

                wishlists_list.add(p_id);
            }
            if (wishlists_list.contains(product_id)) {
                like_iv.setImageResource(R.drawable.like_red_icon);
                like = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
