package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 5/1/2018.
 */

public class AddressBook
{
    private String address_id;
    private String user_id;
    private String title;
    private String firstname;
    private String lastname;
    private String email;
    private String address;
    private String city;
    private String state;
    private String country;
    private String phone;
    private String def_addres;
    private String coments;
    private String add_status;

    public AddressBook(String address_id, String user_id, String title, String firstname, String lastname, String email, String address, String city,
                       String state, String country, String phone, String def_addres, String coments, String add_status) {
        this.address_id = address_id;
        this.user_id = user_id;
        this.title = title;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.phone = phone;
        this.def_addres = def_addres;
        this.coments = coments;
        this.add_status = add_status;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDef_addres() {
        return def_addres;
    }

    public void setDef_addres(String def_addres) {
        this.def_addres = def_addres;
    }

    public String getComents() {
        return coments;
    }

    public void setComents(String coments) {
        this.coments = coments;
    }

    public String getAdd_status() {
        return add_status;
    }

    public void setAdd_status(String add_status) {
        this.add_status = add_status;
    }
}
