package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.Wishlist;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.SearchAdapter;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 4/11/2018.
 */

@SuppressLint("ValidFragment")
public class SearchFragment extends Fragment
{
    View view;
    Context ctx;
    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    ImageView seacrh_iv;
    RecyclerView recyclerView;
    EditText input_et;

    @SuppressLint("ValidFragment")
    public SearchFragment(Context ctx) {
        this.ctx = ctx;
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
//        sessionManager.setData(SessionManager.KEY_PAGE,"other");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_search,null);
        Utility.setTitle(ctx,"Search For Products & Services");
        initXml();
        return view;
    }

    private void initXml() {
        seacrh_iv = view.findViewById(R.id.iv_search_icon);
        recyclerView = view.findViewById(R.id.rv_search_recyclerview);
        input_et = view.findViewById(R.id.et_search_input);

        seacrh_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = input_et.getText().toString();
                if (!text.equals("")){
                    getSearching(text);
                }
            }
        });
    }

    private void getSearching(String text) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_SEARCHING)
                .addBodyParameter("search", text)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        ArrayList<Wishlist> wishlists_list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")){

                JSONObject data_obj = object.getJSONObject("data");
                JSONArray seacrh_product_arr = data_obj.getJSONArray("search_product");
                JSONArray search_service_arr = data_obj.getJSONArray("search_services");
                if (!seacrh_product_arr.equals(null)){
                    for (int i = 0; i < seacrh_product_arr.length(); i++) {

                        JSONObject datajson = seacrh_product_arr.getJSONObject(i);
                        String p_id = datajson.getString("product_id");
                        String name = datajson.getString("product_name ");
                        String image = datajson.getString("product_image");
                        String c_name = datajson.getString("seller_name");
                        String c_address = datajson.getString("city");

                        wishlists_list.add(new Wishlist("",p_id,name,image,"product",c_name,c_address));
                    }

                }
                if (!search_service_arr.equals(null)){
                    for (int i = 0; i < search_service_arr.length(); i++) {

                        JSONObject datajson = search_service_arr.getJSONObject(i);
                        String p_id = datajson.getString("id");
                        String name = datajson.getString("title");
                        String image = datajson.getString("image");
                        String c_name = datajson.getString("provider");
                        String c_address = datajson.getString("location");

                        wishlists_list.add(new Wishlist("",p_id,name,image,"service",c_name,c_address));
                    }
                }
            }
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager layoutManager = new LinearLayoutManager(ctx);
            recyclerView.setLayoutManager(layoutManager);
            SearchAdapter adapter = new SearchAdapter(ctx,wishlists_list);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
