package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/23/2018.
 */

public class PopularPruducts {

    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("product_price")
    @Expose
    public String productPrice;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    public String discountPercent;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("seller_id")
    @Expose
    public String sellerId;
    @SerializedName("item_code")
    @Expose
    public String itemCode;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("address")
    @Expose
    public String address;
    @SerializedName("seller_name")
    @Expose
    public String sellerName;
    @SerializedName("company_name")
    @Expose
    public String companyName;
    @SerializedName("phone")
    @Expose
    public String phone;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("rating")
    @Expose
    public String rating;
    @SerializedName("no_of_person")
    @Expose
    public String noOfPerson;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("service_name")
    @Expose
    public String serviceName;
    @SerializedName("image")
    @Expose
    public String image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("productImage", productImage).append("productPrice", productPrice).append("discountPrice", discountPrice).append("discountPercent", discountPercent).append("categoryId", categoryId).append("subcategoryId", subcategoryId).append("subsubcategoryId", subsubcategoryId).append("sellerId", sellerId).append("itemCode", itemCode).append("sortOrder", sortOrder).append("description", description).append("address", address).append("sellerName", sellerName).append("companyName", companyName).append("phone", phone).append("city", city).append("area", area).append("rating", rating).append("noOfPerson", noOfPerson).append("availability", availability).append("serviceId", serviceId).append("serviceName", serviceName).append("image", image).toString();
    }

}
