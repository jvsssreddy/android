package igrand.technology.info.eganacsi.model;

/**
 * Created by kamlesh on 4/23/2018.
 */

public class ViewAll
{
    private String type;
    private String id;
    private String name;
    private String image;

    public ViewAll(String type, String id, String name,String image) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.image = image;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
