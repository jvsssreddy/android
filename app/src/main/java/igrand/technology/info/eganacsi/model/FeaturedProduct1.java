package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/28/2018.
 */

public class FeaturedProduct1 {

    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name ")
    @Expose
    public String productName;
    @SerializedName("product_image")
    @Expose
    public String productImage;
    @SerializedName("product_price")
    @Expose
    public String productPrice;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    public String discountPercent;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("subsubcategory_name")
    @Expose
    public String subsubcategoryName;
    @SerializedName("seller_id")
    @Expose
    public String sellerId;
    @SerializedName("item_code")
    @Expose
    public String itemCode;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;
    @SerializedName("availability")
    @Expose
    public String availability;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("productImage", productImage).append("productPrice", productPrice).append("discountPrice", discountPrice).append("discountPercent", discountPercent).append("categoryId", categoryId).append("categoryName", categoryName).append("subcategoryId", subcategoryId).append("subcategoryName", subcategoryName).append("subsubcategoryId", subsubcategoryId).append("subsubcategoryName", subsubcategoryName).append("sellerId", sellerId).append("itemCode", itemCode).append("sortOrder", sortOrder).append("availability", availability).toString();
    }
}
