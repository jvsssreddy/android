package igrand.technology.info.eganacsi.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 5/4/2018.
 */

public class ViewAllRecyclerAdapter extends RecyclerView.Adapter<ViewAllRecyclerAdapter.Holder> {
    Context ctx;
    ArrayList<ViewAll> viewAll_list;
    int myviewtype;
    String cust_id;

    public static final int PRODUCT_VIEW = 1;
    public static final int SERVICE_VIEW = 2;

    ArrayList<String> wishlist_idlist;
    SessionManager sessionManager;
    String data;

    public ViewAllRecyclerAdapter(Context ctx, ArrayList<ViewAll> viewAll_list, int myviewtype, String cust_id) {
        this.ctx = ctx;
        this.viewAll_list = viewAll_list;
        this.myviewtype = myviewtype;
        this.cust_id = cust_id;
        wishlist_idlist = new ArrayList<>();
        sessionManager = new SessionManager(ctx);
        data = sessionManager.getData(SessionManager.KEY_WISHLIST_DATA);
        setWishlistResponse(data);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
        if (myviewtype == PRODUCT_VIEW) {
            view = inflater.inflate(R.layout.product_view, null);
        } else {
            view = inflater.inflate(R.layout.service_view, null);
        }
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {

        Picasso.with(ctx).load(viewAll_list.get(position).getImage())
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(holder.main_iv);
        holder.name_tv.setText(viewAll_list.get(position).getName());

        if (myviewtype==PRODUCT_VIEW){
            if (wishlist_idlist.contains(viewAll_list.get(position).getId()+"p")){
                holder.like_iv.setImageResource(R.drawable.like_red_icon);
            }
        }else {
            if (wishlist_idlist.contains(viewAll_list.get(position).getId()+"s")){
                holder.like_iv.setImageResource(R.drawable.like_red_icon);
            }
        }

        holder.like_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myviewtype==PRODUCT_VIEW){
                    String product_id1 = viewAll_list.get(position).getId();
                    if (wishlist_idlist.contains(product_id1+"p")){
                        dislikeproductandService(cust_id, "", product_id1, holder,"p");
                    }else {
                        likeproductandService(cust_id, "", product_id1, holder);
                    }
                }else {
                    String product_id1 = viewAll_list.get(position).getId();
                    if (wishlist_idlist.contains(product_id1+"s")){
                        dislikeproductandService(cust_id, product_id1,"", holder,"s");
                    }else {
                        likeproductandService(cust_id,product_id1,"", holder);
                    }
                }

            }
        });
        holder.detials_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myviewtype==PRODUCT_VIEW){
                    ctx.startActivity(new Intent(ctx, ProductDetailActivity.class)
                    .putExtra("product_id",viewAll_list.get(position).getId()));
                }else {
                    ctx.startActivity(new Intent(ctx, ServiceDetailActivity.class)
                    .putExtra("Service",viewAll_list.get(position).getId()));
                }
            }
        });
        holder.get_book_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, GetQuoteActivity.class));
            }
        });
    }

    private void dislikeproductandService(String cust_id, final String service_id1, final String product_id1, final Holder holder, final String type) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_DELETE_WISHLIST)
                .addBodyParameter("customer_id", this.cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                if (type.equals("p")){
                                    wishlist_idlist.remove(product_id1+"p");
                                }else {
                                    wishlist_idlist.remove(service_id1+"s");
                                }
                                Utility.toastView(ctx, "Remove to wishlist");
                                holder.like_iv.setImageResource(R.drawable.like_fill_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void likeproductandService(String cust_id, final String service_id1, final String product_id1, final Holder holder) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADD_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                Utility.toastView(ctx, "Added to wishlist");
//                                ImageView like_iv = view.findViewById(R.id.like_image);
                                holder.like_iv.setImageResource(R.drawable.like_red_icon);
                                if (!product_id1.equals("")){
                                    wishlist_idlist.add(product_id1+"p");
                                }else {
                                    wishlist_idlist.add(service_id1+"s");
                                }
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });

    }

    @Override
    public int getItemCount() {
        return viewAll_list.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        ImageView main_iv, like_iv;
        Button get_book_bt, detials_bt;
        TextView name_tv;

        public Holder(View itemView) {
            super(itemView);
            main_iv = itemView.findViewById(R.id.product_image);
            like_iv = itemView.findViewById(R.id.like_image);
            name_tv = itemView.findViewById(R.id.product_title);
            get_book_bt = itemView.findViewById(R.id.getquot_bt);
            detials_bt = itemView.findViewById(R.id.detail_bt);
        }
    }

    private void setWishlistResponse(String s) {

        wishlist_idlist = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONObject data_obj = object.getJSONObject("data");
            JSONArray wish_arry = data_obj.getJSONArray("wishlist");
            for (int i = 0; i < wish_arry.length(); i++) {

                JSONObject datajson = wish_arry.getJSONObject(i);
                String p_id = datajson.getString("p_id");
                String type = datajson.getString("type");
                if (type.equals("product")){
                    wishlist_idlist.add(p_id+"p");
                }else {
                    wishlist_idlist.add(p_id+"s");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

