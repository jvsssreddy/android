package igrand.technology.info.eganacsi.seller.response.camp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampResponse
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private CampData data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CampData getData() {
        return data;
    }

    public void setData(CampData data) {
        this.data = data;
    }
}
