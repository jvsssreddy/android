package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.AdapterFAQs;
import igrand.technology.info.eganacsi.util.Utility;

public class FAQsActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    ExpandableListView expandableListView;

    List<String> listDataHeader,listDiscrptionHeader;
    HashMap<String, List<String>> listDataChild;

    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);
        initXml();
        initobj();
        boolean internet = connectionDetector.isConnected();
        if (internet){
//            getFaqs();
            getData();
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
//        prepareListData();
    }

    private void getFaqs() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("loading...");
        progressDialog.show();
        AndroidNetworking.get(WebApi.URL_FAQS)
                .addQueryParameter("limit", "3")
                .setTag("test")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                        progressDialog.dismiss();
                        Utility.toastView(ctx,response.toString());
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Utility.toastView(ctx,"error show"+error.toString());
                        setResponse("error");
                    }
                });
    }

    private void setResponse(String response) {


        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        listDiscrptionHeader = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);

            String msg = jsonObject.getString("message");
            if (msg.equals("Success")){

                JSONArray data_array = jsonObject.getJSONArray("data");

                for (int i = 0; i < data_array.length(); i++) {

                    JSONObject object = data_array.getJSONObject(i);
                    String title = object.getString("title");
                    String description = object.getString("description");

                    listDataHeader.add(title);
                    listDiscrptionHeader.add(description);

                    listDataChild.put(listDataHeader.get(i), listDiscrptionHeader); // Header, Child data
                    listDiscrptionHeader = new ArrayList<>();
                }
                AdapterFAQs listAdapter = new AdapterFAQs(this, listDataHeader, listDataChild);
                expandableListView.setAdapter(listAdapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initobj() {
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx =this;
        back_iv = (ImageView)findViewById(R.id.iv_faqs_back);
        expandableListView = findViewById(R.id.lvexp_faq_expandable);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add("Why has re-launched its Desktop site ?");
        listDataHeader.add("What id try and buy service?");
        listDataHeader.add("how do i cancel the order ?");

        // Adding child data
        List<String> top250 = new ArrayList<String>();
        top250.add(ctx.getString(R.string.answer));

        List<String> nowShowing = new ArrayList<String>();
        nowShowing.add(ctx.getString(R.string.answer));

        List<String> comingSoon = new ArrayList<String>();
        comingSoon.add(ctx.getString(R.string.answer));

        listDataChild.put(listDataHeader.get(0), top250); // Header, Child data
        listDataChild.put(listDataHeader.get(1), nowShowing);
        listDataChild.put(listDataHeader.get(2), comingSoon);

        AdapterFAQs listAdapter = new AdapterFAQs(this, listDataHeader, listDataChild);
        expandableListView.setAdapter(listAdapter);
    }

    private void getData() {
        //getting the progressbar
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("loading...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebApi.URL_FAQS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressDialog.dismiss();
                        setResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
