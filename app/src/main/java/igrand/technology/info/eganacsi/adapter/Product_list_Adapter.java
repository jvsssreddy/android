package igrand.technology.info.eganacsi.adapter;

import android.content.Context;

import android.content.Intent;
import android.support.annotation.NonNull;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.BookServiceActivity;
import igrand.technology.info.eganacsi.activity.BookServiceActivity1;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity1;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.model.ProductListData;


/**
 * Created by administator on 6/23/2018.
 */

public class Product_list_Adapter extends RecyclerView.Adapter<Product_list_Adapter.ViewHolder>{
    List<ProductListData> subSubCategoryDataResponse;
    Context context;

    public static final int PRODUCT_VIEW = 1;
    public static final int SERVICE_VIEW = 2;

    public Product_list_Adapter(List<ProductListData> subSubCategoryDataResponse, Context context) {
        this.context=context;
        this.subSubCategoryDataResponse=subSubCategoryDataResponse;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        if (viewType == PRODUCT_VIEW) {
            view = inflater.inflate(R.layout.row_product_listing, parent, false);
        } else {
            view = inflater.inflate(R.layout.row_product_listing2, parent, false);
        }
        ViewHolder vh = new ViewHolder(view,viewType);
            return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        if(subSubCategoryDataResponse.get(position).productImage!= null){
            Picasso.with(context).load(subSubCategoryDataResponse.get(position).productImage).into(holder.imageView);
        }else {
            Picasso.with(context).load(subSubCategoryDataResponse.get(position).serviceImage).into(holder.imageView);
        }

        if (subSubCategoryDataResponse.get(position).productName!= null){
            holder.name.setText(subSubCategoryDataResponse.get(position).productName);

        }else{
            holder.name.setText(subSubCategoryDataResponse.get(position).serviceName);
        }

        if (subSubCategoryDataResponse.get(position).productPrice!= null){
            holder.price.setText(subSubCategoryDataResponse.get(position).productPrice);

        }else{
            holder.price.setText(subSubCategoryDataResponse.get(position).price);
        }
        holder.location.setText(subSubCategoryDataResponse.get(position).address);
        holder.address.setText(subSubCategoryDataResponse.get(position).companyName);

        holder.linear_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItemViewType(position) == PRODUCT_VIEW){
                    Intent intent = new Intent(context, GetQuoteActivity1.class);
                    intent.putExtra("pcategoryname",subSubCategoryDataResponse.get(position).categoryName);
                    intent.putExtra("psubcategoryname",subSubCategoryDataResponse.get(position).subcategoryName);
                    intent.putExtra("psubsubcategoryname",subSubCategoryDataResponse.get(position).subsubcategoryName);
                    intent.putExtra("productname",subSubCategoryDataResponse.get(position).productName);

                    intent.putExtra("pcategoryid",subSubCategoryDataResponse.get(position).categoryId);
                    intent.putExtra("psubcategoryid",subSubCategoryDataResponse.get(position).subcategoryId);
                    intent.putExtra("psubsubcategoryid",subSubCategoryDataResponse.get(position).subsubcategoryId);
                    intent.putExtra("productid",subSubCategoryDataResponse.get(position).productId);
                    context.startActivity(intent);


                }
                if (getItemViewType(position) == SERVICE_VIEW){
                    Intent intent = new Intent(context, BookServiceActivity1.class);
                    intent.putExtra("categoryname",subSubCategoryDataResponse.get(position).categoryName);
                    intent.putExtra("subcategoryname",subSubCategoryDataResponse.get(position).subcategoryName);
                    intent.putExtra("subsubcategoryname",subSubCategoryDataResponse.get(position).subsubcategoryName);
                    intent.putExtra("servicename",subSubCategoryDataResponse.get(position).serviceName);

                    Toast.makeText(context, subSubCategoryDataResponse.get(position).categoryName, Toast.LENGTH_SHORT).show();

                    intent.putExtra("pcategoryid",subSubCategoryDataResponse.get(position).categoryId);
                    intent.putExtra("psubcategoryid",subSubCategoryDataResponse.get(position).subcategoryId);
                    intent.putExtra("psubsubcategoryid",subSubCategoryDataResponse.get(position).subsubcategoryId);
                    intent.putExtra("productid",subSubCategoryDataResponse.get(position).serviceId);
                    context.startActivity(intent);
                }
            }
        });

        holder.linear_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getItemViewType(position) == PRODUCT_VIEW){
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra("product_id",subSubCategoryDataResponse.get(position).productId);
                    context.startActivity(intent);
                }
                if (getItemViewType(position) == SERVICE_VIEW){
                    Intent intent = new Intent(context, ServiceDetailActivity.class);
                    intent.putExtra("Service",subSubCategoryDataResponse.get(position).serviceId);
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return subSubCategoryDataResponse.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder{
        TextView name;
        TextView address;
        TextView location;
        TextView price;
        ImageView imageView;
        LinearLayout linear_order;
        LinearLayout linear_details;
        public ViewHolder(View itemView, int viewtype) {
            super(itemView);
            name = itemView.findViewById(R.id.company_name);
            address = itemView.findViewById(R.id.address);
            location = itemView.findViewById(R.id.location);
            imageView = itemView.findViewById(R.id.product_image1);
            price=itemView.findViewById(R.id.price);
            linear_order=itemView.findViewById(R.id.linear_order);
            linear_details=itemView.findViewById(R.id.linear_details);
        }
    }
    @Override
    public int getItemViewType(int position) {

        int type = subSubCategoryDataResponse.get(position).type;
        int viewtype = 0;
        if (type==1) {
            viewtype = PRODUCT_VIEW;
        }
        if (type==0) {
            viewtype = SERVICE_VIEW;
        }
        return viewtype;
    }
}

