package igrand.technology.info.eganacsi.model.service_list;

/**
 * Created by kamlesh on 4/22/2018.
 */


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceData {

    @SerializedName("popular_service")
    @Expose
    private List<Service> popularService = null;

    public List<Service> getPopularService() {
        return popularService;
    }

    public void setPopularService(List<Service> popularService) {
        this.popularService = popularService;
    }

}