package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

public class ContactUsActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    TextView staore_addres_tv;
    EditText name_et, email_et, enquiry_et;
    Button submit_bt;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initXml();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getAddress();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void getAddress() {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_CONTACT_US)
                .setTag("test")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {

                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, anError.toString());
                    }
                });
    }

    private void setResponse(String s) {

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONObject data_object = object.getJSONObject("data");
                String desc = data_object.getString("description");
                String address = data_object.getString("address");
                String contact = data_object.getString("phone");

                staore_addres_tv.setText(address + "\n Phone : " + contact + "\n\n" + desc);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_contus_back);
        enquiry_et = findViewById(R.id.et_contus_enquiry);
        name_et = findViewById(R.id.et_contus_name);
        email_et = findViewById(R.id.et_contus_email);
        submit_bt = findViewById(R.id.bt_conus_submit);
        staore_addres_tv = findViewById(R.id.tv_contus_storeaddress);

        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);

        back_iv.setOnClickListener(this);
        submit_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_contus_back:
                finish();
                break;
            case R.id.bt_conus_submit:
                boolean internet = connectionDetector.isConnected();
                if (internet) {
                    getData();
                } else {
                    Utility.toastView(ctx, ctx.getString(R.string.no_internet));
                }
                getData();
                break;
        }
    }

    private void getData() {

        String u_id = sessionManager.getData(SessionManager.KEY_CID);
        String name = name_et.getText().toString();
        String email = email_et.getText().toString();
        String enquiry = enquiry_et.getText().toString();

        if (name.equals("") || email.equals("") || enquiry.equals("")) {
            Utility.toastView(ctx, "Enter all field");
        }else if (!Utility.checkMailPatteren(email)){
            Utility.toastView(ctx, "Invalid Email");
        }else {
            submitContact(name, email, enquiry, u_id);
        }
    }

    private void submitContact(String name, String email, String enquiry, String u_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_CONTACT_FILL)
                .addBodyParameter("name", name)
                .addBodyParameter("email", email)
                .addBodyParameter("enuiry", enquiry)
                .addBodyParameter("userid", u_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                Utility.toastView(ctx, "Successfully Submit");
                                clear();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        clear();
                    }
                });
    }

    private void clear() {
        name_et.setText("");
        email_et.setText("");
        enquiry_et.setText("");
    }
}
