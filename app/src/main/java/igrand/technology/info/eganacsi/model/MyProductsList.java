package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/28/2018.
 */

public class MyProductsList {
    @SerializedName("product_id")
    @Expose
    public String productId;
    @SerializedName("product_name")
    @Expose
    public String productName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("product_price")
    @Expose
    public String productPrice;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("item_code")
    @Expose
    public String itemCode;
    @SerializedName("availability")
    @Expose
    public String availability;
    @SerializedName("created_date")
    @Expose
    public String createdDate;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("productId", productId).append("productName", productName).append("image", image).append("productPrice", productPrice).append("categoryId", categoryId).append("subcategoryId", subcategoryId).append("subsubcategoryId", subsubcategoryId).append("itemCode", itemCode).append("availability", availability).append("createdDate", createdDate).toString();
    }
}
