package igrand.technology.info.eganacsi.seller.response.myquoatation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyQuotationEnquiry implements Parcelable
{
    @SerializedName("en_id")
    @Expose
    private String enId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("c_name")
    @Expose
    private String cName;
    @SerializedName("c_phone")
    @Expose
    private String cPhone;
    @SerializedName("c_email")
    @Expose
    private String cEmail;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("appointment")
    @Expose
    private String appointment;

    protected MyQuotationEnquiry(Parcel in) {
        enId = in.readString();
        customerId = in.readString();
        sellerId = in.readString();
        productId = in.readString();
        serviceId = in.readString();
        productName = in.readString();
        serviceName = in.readString();
        cName = in.readString();
        cPhone = in.readString();
        cEmail = in.readString();
        message = in.readString();
        datetime = in.readString();
        appointment = in.readString();
    }

    public static final Creator<MyQuotationEnquiry> CREATOR = new Creator<MyQuotationEnquiry>() {
        @Override
        public MyQuotationEnquiry createFromParcel(Parcel in) {
            return new MyQuotationEnquiry(in);
        }

        @Override
        public MyQuotationEnquiry[] newArray(int size) {
            return new MyQuotationEnquiry[size];
        }
    };

    public String getEnId() {
        return enId;
    }

    public void setEnId(String enId) {
        this.enId = enId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public String getCPhone() {
        return cPhone;
    }

    public void setCPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getCEmail() {
        return cEmail;
    }

    public void setCEmail(String cEmail) {
        this.cEmail = cEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(enId);
        dest.writeString(customerId);
        dest.writeString(sellerId);
        dest.writeString(productId);
        dest.writeString(serviceId);
        dest.writeString(productName);
        dest.writeString(serviceName);
        dest.writeString(cName);
        dest.writeString(cPhone);
        dest.writeString(cEmail);
        dest.writeString(message);
        dest.writeString(datetime);
        dest.writeString(appointment);
    }
}
