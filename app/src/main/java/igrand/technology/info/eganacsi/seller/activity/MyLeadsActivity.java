package igrand.technology.info.eganacsi.seller.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.model.C_SellerProfileData;
import igrand.technology.info.eganacsi.model.C_SellerProfileResponse;
import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.seller.adapter.MyQuoatationAdapter;
import igrand.technology.info.eganacsi.seller.response.MyLeads.MyLeadResponse;
import igrand.technology.info.eganacsi.seller.response.myquoatation.MyQuotationEnquiry;
import igrand.technology.info.eganacsi.seller.response.myquoatation.MyQuotationResponse;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.Notification;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.seller.adapter.MyLeadAdapter;
import igrand.technology.info.eganacsi.seller.response.MyLeads.Lead;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyLeadsActivity extends AppCompatActivity {

    Context ctx;
    RecyclerView recyclerView;
    ImageView back_iv;
    TextView title_tv;
    RadioButton service_rb, product_rb, all_rb;
    String type;
    public static final int LEAD_VIEW = 1;
    public static final int QUOTE_VIEW = 2;
    igrand.technology.info.eganacsi.ApiInterface apiInterface;
    String Cid;

    MyLeadAdapter myLeadAdapter;
    ConnectionDetector connectionDetector;
    SessionManager sessionManager;
    List<Notification> notification_List;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_leads);

        SharedPreferences sharedPreferences =getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email","");
        String password = sharedPreferences.getString("password","");

        apiInterface= igrand.technology.info.eganacsi.ApiClient.getClient1().create(igrand.technology.info.eganacsi.ApiInterface.class);
        Call<C_SellerProfileResponse> call=apiInterface.CustomerSellerProfile(email,password);
        call.enqueue(new Callback<C_SellerProfileResponse>() {
            @Override
            public void onResponse(Call<C_SellerProfileResponse> call, Response<C_SellerProfileResponse> response) {
                try {
                    if (response.isSuccessful());
                    C_SellerProfileResponse c_sellerProfileResponse=response.body();
                    if (c_sellerProfileResponse.status.equals("1")) {
                        C_SellerProfileData c_sellerProfileData = c_sellerProfileResponse.data;
                        Cid = c_sellerProfileData.sId;


                    }else if (c_sellerProfileResponse.status.equals("0")){

                    }
                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<C_SellerProfileResponse> call, Throwable t) {

            }
        });
        initXml();
        initObj();
        String c_id = sessionManager.getData(SessionManager.KEY_CID);
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            if (getIntent().getStringExtra("type").equals("leads")) {
                getLeads(c_id);
            } else {
                getQuote(Cid);
            }
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void getQuote(String seller_id) {

        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<MyQuotationResponse> call = apiService.getMyQuote(seller_id);
        call.enqueue(new Callback<MyQuotationResponse>() {
            @Override
            public void onResponse(Call<MyQuotationResponse> call, Response<MyQuotationResponse> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                String message = response.body().getMessage();
                if (message.equals("Success")) {
                    setQuoteAdapter(response);
                } else {
                    Utility.toastView(ctx, "Please try again");
                }
            }

            @Override
            public void onFailure(Call<MyQuotationResponse> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void setQuoteAdapter(Response<MyQuotationResponse> response) {

        List<MyQuotationEnquiry> my_quote_list = response.body().getData().getEnquiry();
        MyQuoatationAdapter myQuoatationAdapter = new MyQuoatationAdapter(my_quote_list, ctx, QUOTE_VIEW);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(myQuoatationAdapter);
    }

    private void getLeads(String seller_id) {

        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<MyLeadResponse> call = apiService.myLead(seller_id);
        call.enqueue(new Callback<MyLeadResponse>() {
            @Override
            public void onResponse(Call<MyLeadResponse> call, Response<MyLeadResponse> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                String message = response.body().getMessage();
                if (message.equals("Success")) {
                    setAdapter(response);
                } else {
                    Utility.toastView(ctx, "Please try again");
                }
            }

            @Override
            public void onFailure(Call<MyLeadResponse> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(Response<MyLeadResponse> response) {

        List<Lead> lead_demo_ist = response.body().getData().getLeads();
        myLeadAdapter = new MyLeadAdapter(lead_demo_ist, ctx, LEAD_VIEW);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(myLeadAdapter);
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);

        notification_List = new ArrayList<>();
    }

    private void initXml() {
        ctx = this;
        title_tv = findViewById(R.id.tv_mylead_title);
        recyclerView = findViewById(R.id.rv_myleads_recyclerview);
        back_iv = findViewById(R.id.iv_mylead_back);
        service_rb = findViewById(R.id.rb_mylead_service);
        product_rb = findViewById(R.id.rb_mylead_product);
        all_rb = findViewById(R.id.rb_mylead_all);

        Intent intent = getIntent();
        if (!intent.equals(null)) {
            String title = intent.getStringExtra("title");
            type = intent.getStringExtra("type");
            title_tv.setText(title);
        }

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
