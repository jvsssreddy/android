package igrand.technology.info.eganacsi.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import igrand.technology.info.eganacsi.ApiClient;
import igrand.technology.info.eganacsi.ApiInterface;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.EnquiryDetailsData;
import igrand.technology.info.eganacsi.model.EnquiryDetailsResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by administator on 6/27/2018.
 */

public class EnquriyProductDetails extends AppCompatActivity {
    ApiInterface apiInterface;
    ImageView iv_myaccount_back;
    TextView id,p_name,c_name,name,email,mobile,appointment,description,s_name,s_mail;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.enquiry_product_details);
        id=findViewById(R.id.enquiryid);
        p_name=findViewById(R.id.p_name);
        c_name=findViewById(R.id.c_name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        name=findViewById(R.id.name);
        appointment=findViewById(R.id.appointment);
        description=findViewById(R.id.description);
        s_name=findViewById(R.id.seller_name);
        s_mail=findViewById(R.id.seller_gmail);
        iv_myaccount_back=findViewById(R.id.iv_myaccount_back);

        iv_myaccount_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        SharedPreferences sharedPreferences =getSharedPreferences("enquiry", Context.MODE_PRIVATE);
        String e_id = sharedPreferences.getString("eid","");
        apiInterface= ApiClient.getClient1().create(ApiInterface.class);
        Call<EnquiryDetailsResponse> call=apiInterface.EnquiryDetails(e_id);
        call.enqueue(new Callback<EnquiryDetailsResponse>() {
            @Override
            public void onResponse(Call<EnquiryDetailsResponse> call, Response<EnquiryDetailsResponse> response) {
                try {
                    if (response.isSuccessful());
                    EnquiryDetailsResponse enquiryDetailsResponse=response.body();
                    if (enquiryDetailsResponse.status.contains("1")) {
                        EnquiryDetailsData enquiryDetailsData = enquiryDetailsResponse.data;
                        id.setText(enquiryDetailsData.enId);
                        p_name.setText(enquiryDetailsData.productName);
                        c_name.setText(enquiryDetailsData.companyName);
                        email.setText(enquiryDetailsData.email);
                        mobile.setText(enquiryDetailsData.phone);
                        appointment.setText(enquiryDetailsData.appointment);
                        description.setText(enquiryDetailsData.description);
                        s_name.setText(enquiryDetailsData.name);
                        name.setText(enquiryDetailsData.name);
                        s_mail.setText(enquiryDetailsData.email);


                    }




                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<EnquiryDetailsResponse> call, Throwable t) {

            }
        });



    }
}
