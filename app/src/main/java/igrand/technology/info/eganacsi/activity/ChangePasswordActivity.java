package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    EditText old_pass_et,new_pass_et,conf_pass_et;
    Button update_bt;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initXml();
        initObj();
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_changepassword_back);
        old_pass_et = findViewById(R.id.et_changepass_old);
        new_pass_et = findViewById(R.id.et_changepass_new);
        conf_pass_et = findViewById(R.id.et_changepass_conf);
        update_bt = findViewById(R.id.bt_changepass_update);

        back_iv.setOnClickListener(this);
        update_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.iv_changepassword_back:
                finish();
                break;

            case R.id.bt_changepass_update:
                getData();
                break;
        }
    }

    private void getData() {

        String c_id = sessionManager.getData(SessionManager.KEY_CID);
        String old_pass = old_pass_et.getText().toString();
        String new_pass = new_pass_et.getText().toString();
        String conf_pass = conf_pass_et.getText().toString();

        if (old_pass.equals("")||new_pass.equals("")||conf_pass.equals("")){
            Utility.toastView(ctx,"Enter all field");
        }else if (new_pass.length() >= 8){
            Utility.toastView(ctx,"New Password must be atleast 8 character");
        }else if (!new_pass.equals(conf_pass)){
            Utility.toastView(ctx,"Confirm password not matched");
        }else{
            boolean internet = connectionDetector.isConnected();
            if (internet){
                updatePassword(c_id,old_pass,new_pass);
            }else {
                Utility.toastView(ctx,ctx.getString(R.string.no_internet));
            }
        }
    }

    private void updatePassword(String c_id, String old_pass, String new_pass) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_CHANGE_PASSWORD)
                .addBodyParameter("customer_id", c_id)
                .addBodyParameter("old_password", old_pass)
                .addBodyParameter("new_password", new_pass)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        progressDialog.dismiss();
                        setResponse("error");
                    }
                });
    }

    private void setResponse(String s) {

        if (!s.equals("error")){
            try {
                JSONObject jsonObject = new JSONObject(s);
                String status = jsonObject.getString("status");
                String msg = jsonObject.getString("message");
                if (status.equals("1")){
                    Utility.toastView(ctx,"Password changed Successfully");
                    finish();
                }else if (msg.equals("Please enter correct password")){
                    Utility.toastView(ctx,"Please enter correct password");
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.something_went));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.something_went));
        }
    }

}
