package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.R;

public class ViewAllAdapter extends BaseAdapter {

    Context ctx;
    ArrayList<ViewAll> viewAllArrayList;
    int viewtype;

    public ViewAllAdapter(Context ctx,ArrayList<ViewAll> viewAllArrayList,int viewtype) {
        this.ctx = ctx;
        this.viewtype = viewtype;
        this.viewAllArrayList= viewAllArrayList;
    }

    @Override
    public int getCount() {
        return viewAllArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;
        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (viewtype==1){
            view = inflater.inflate(R.layout.product_view,null);
            ImageView imageView = view.findViewById(R.id.product_image);
            ImageView like_image = view.findViewById(R.id.like_image);
            Button get_quote_tv = view.findViewById(R.id.getquot_bt);
            Button detail_bt = view.findViewById(R.id.detail_bt);
            TextView name = view.findViewById(R.id.product_title);

            if (viewAllArrayList.get(position).getImage().isEmpty() ) {

                Picasso.with(ctx).load(R.drawable.no_image)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }else {
                Picasso.with(ctx).load(viewAllArrayList.get(position).getImage())
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }
            name.setText(viewAllArrayList.get(position).getName());

        }else if (viewtype==2){
            view = inflater.inflate(R.layout.service_view,null);
            ImageView imageView = view.findViewById(R.id.product_image);
            TextView name = view.findViewById(R.id.product_title);


            if (viewAllArrayList.get(position).getImage().isEmpty() ) {

                Picasso.with(ctx).load(R.drawable.no_image)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }else {
                Picasso.with(ctx).load(viewAllArrayList.get(position).getImage())
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }
//            if (viewAllArrayList.get(position).getImage()!=null) {
//
//                Picasso.with(ctx).load(viewAllArrayList.get(position).getImage())
//                        .placeholder(R.drawable.no_image)
//                        .error(R.drawable.no_image)
//                        .into(imageView);
//            }
            name.setText(viewAllArrayList.get(position).getName());
        }else {
            view = inflater.inflate(R.layout.adp_viewall_grid,null);
            ImageView imageView = view.findViewById(R.id.product_image);
            TextView name = view.findViewById(R.id.product_title);


            if (viewAllArrayList.get(position).getImage().isEmpty() ) {

                Picasso.with(ctx).load(R.drawable.no_image)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }else {
                Picasso.with(ctx).load(viewAllArrayList.get(position).getImage())
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(imageView);
            }
//            if (viewAllArrayList.get(position).getImage()!=nullhttp://igrand.info/egn/uploads/subcategories/category_1529928386.jpg) {
//
//                Picasso.with(ctx).load(viewAllArrayList.get(position).getImage())
//                        .placeholder(R.drawable.no_image)
//                        .error(R.drawable.no_image)
//                        .into(imageView);
//            }
            name.setText(viewAllArrayList.get(position).getName());

        }
        return view;
    }

}