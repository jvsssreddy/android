package igrand.technology.info.eganacsi.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import igrand.technology.info.eganacsi.model.Notification;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 4/13/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder>
{
    private List<Notification> notification_List;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView seller_tv,date_tv,msg_tv,link_tv;

        public MyViewHolder(View view) {
            super(view);
            seller_tv = view.findViewById(R.id.tv_noti_sellername);
            date_tv = view.findViewById(R.id.tv_noti_date);
            msg_tv = view.findViewById(R.id.tv_noti_msg);
            link_tv = view.findViewById(R.id.tv_noti_link);
        }
    }


    public NotificationAdapter(List<Notification> enqiry_List) {
        this.notification_List = enqiry_List;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adp_notification, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.link_tv.setText(notification_List.get(position).getLink());
        holder.seller_tv.setText("Seller Name : "+notification_List.get(position).getName());
        holder.msg_tv.setText(notification_List.get(position).getMessage());
        holder.date_tv.setText("Date : "+notification_List.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return notification_List.size();
    }
}
