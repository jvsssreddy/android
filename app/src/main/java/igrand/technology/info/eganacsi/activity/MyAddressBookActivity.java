package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.AddressBook;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.adapter.AddressBookAdapter;
import igrand.technology.info.eganacsi.util.Utility;

public class MyAddressBookActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    Button add_new_bt;
    ListView listView;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address_book);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            String user_id = sessionManager.getData(SessionManager.KEY_CID);
            getAddress(user_id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
    }

    private void getAddress(String user_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADDRESS_BOOK)
                .addBodyParameter("customer_id", user_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        ArrayList<AddressBook> adrBooks_list = new ArrayList<>();

        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("0")){
                Utility.toastView(ctx,"No address found");
            }else if (status.equals("1")){

                JSONObject data_obj = object.getJSONObject("data");
                JSONArray address_array = data_obj.getJSONArray("Address");
                for (int i = 0; i < address_array.length(); i++) {

                    JSONObject  adres_obj = address_array.getJSONObject(i);

                    String address_id = adres_obj.getString("address_id");
                    String user_id = adres_obj.getString("user_id");
                    String title = adres_obj.getString("title");
                    String firstname = adres_obj.getString("firstname");
                    String lastname = adres_obj.getString("lastname");
                    String email = adres_obj.getString("email");
                    String address = adres_obj.getString("address");
                    String city = adres_obj.getString("city");
                    String state = adres_obj.getString("state");
                    String country = adres_obj.getString("country");
                    String phone = adres_obj.getString("phone");
                    String def_addres = adres_obj.getString("default_address");
                    String coments = adres_obj.getString("comments");
                    String add_status = adres_obj.getString("status");

                    adrBooks_list.add(new AddressBook(address_id,user_id,title,firstname,lastname,email,address,
                            city,state,country,phone,def_addres,coments,add_status));
                }
                AddressBookAdapter addressBookAdapter = new AddressBookAdapter(ctx,adrBooks_list);
                listView.setAdapter(addressBookAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_addressbook_back);
        add_new_bt = findViewById(R.id.bt_addressbook_adnew);
        listView = findViewById(R.id.lv_addressbook_listview);

        add_new_bt.setOnClickListener(this);
        back_iv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.bt_addressbook_adnew:
                startActivity(new Intent(ctx,AddNewAddressActivity.class));
                finish();
                break;

            case R.id.iv_addressbook_back:
                finish();
                break;
        }
    }
}
