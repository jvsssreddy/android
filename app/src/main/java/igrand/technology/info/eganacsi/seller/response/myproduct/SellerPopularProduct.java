package igrand.technology.info.eganacsi.seller.response.myproduct;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamlesh on 5/16/2018.
 */

public class SellerPopularProduct implements Parcelable
{

    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("product_price")
    @Expose
    private String productPrice;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    private String subsubcategoryId;
    @SerializedName("item_code")
    @Expose
    private String itemCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("created_date")
    @Expose
    private String createdDate;

    protected SellerPopularProduct(Parcel in) {
        productId = in.readString();
        productName = in.readString();
        image = in.readString();
        productPrice = in.readString();
        categoryId = in.readString();
        subcategoryId = in.readString();
        subsubcategoryId = in.readString();
        itemCode = in.readString();
        description = in.readString();
        availability = in.readString();
        createdDate = in.readString();
    }

    public static final Creator<SellerPopularProduct> CREATOR = new Creator<SellerPopularProduct>() {
        @Override
        public SellerPopularProduct createFromParcel(Parcel in) {
            return new SellerPopularProduct(in);
        }

        @Override
        public SellerPopularProduct[] newArray(int size) {
            return new SellerPopularProduct[size];
        }
    };

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubsubcategoryId() {
        return subsubcategoryId;
    }

    public void setSubsubcategoryId(String subsubcategoryId) {
        this.subsubcategoryId = subsubcategoryId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productId);
        dest.writeString(productName);
        dest.writeString(image);
        dest.writeString(productPrice);
        dest.writeString(categoryId);
        dest.writeString(subcategoryId);
        dest.writeString(subsubcategoryId);
        dest.writeString(itemCode);
        dest.writeString(description);
        dest.writeString(availability);
        dest.writeString(createdDate);
    }
}
