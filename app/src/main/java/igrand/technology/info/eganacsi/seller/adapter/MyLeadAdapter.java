package igrand.technology.info.eganacsi.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import igrand.technology.info.eganacsi.seller.activity.MyleadViewmoreActivity;
import igrand.technology.info.eganacsi.seller.response.MyLeads.Lead;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 5/14/2018.
 */

public class MyLeadAdapter extends RecyclerView.Adapter<MyLeadAdapter.ViewHolder> {
    List<Lead> lead_list;
    Context ctx;
    int viewtype;

    public MyLeadAdapter(List<Lead> lead_list, Context ctx, int ViewType) {
        this.lead_list = lead_list;
        this.ctx = ctx;
        this.viewtype = ViewType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_mylead, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.cust_name_tv.setText(lead_list.get(position).getCName());
        holder.service_name_tv.setText(lead_list.get(position).getServiceName());
        holder.date_name_tv.setText(lead_list.get(position).getDatetime());

        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Lead lead = lead_list.get(position);
                ctx.startActivity(new Intent(ctx, MyleadViewmoreActivity.class)
                        .putExtra("type", "Lead")
                        .putExtra("data", lead)
                        .putExtra("title", "My Lead View More"));
            }
        });
    }

    @Override
    public int getItemCount() {
        return lead_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView cust_name_tv, service_name_tv, date_name_tv, viewmore_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            cust_name_tv = itemView.findViewById(R.id.tv_adplead_proname);
            service_name_tv = itemView.findViewById(R.id.tv_adplead_servname);
            date_name_tv = itemView.findViewById(R.id.tv_adplead_date);
            viewmore_tv = itemView.findViewById(R.id.tv_adplead_viewmore);
        }
    }
}
