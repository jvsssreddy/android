package igrand.technology.info.eganacsi.seller.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.seller.UploadImageInterface;
import igrand.technology.info.eganacsi.seller.response.AddService;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.seller.response.UpdateResponse;
import igrand.technology.info.eganacsi.seller.response.myproduct.SellerPopularProduct;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Admin on 11-May-18.
 */

public class SellerAddProductActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    Spinner cate_sp, subcate_sp, subsubcate_sp, status_sp;
    EditText pname_et, mprice_et, oprice_et, percent_et, desc_et;
    RadioButton instock_rb, outstock_rb;
    Button submit_bt;
    TextView browse_tv,filename_tv,title_tv;

    ArrayList<String> cate_list, cate_idlist, subcate_idlist, subsubcate_list, subsubcate_idlist, status_list;
    private String data, subdata;
    String v_cate_id, v_subcate_id, v_subsubcate_id="not", v_status, v_pname, v_mprice,
            v_oprice, v_percent, v_desc, v_seller_id,v_stcok,filename="";
    String v_product_id;

    File file;
    String type;
    String TAG = "Seller Image Upload";

    public static final int PICK_IMAGE_REQUEST = 1;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_add_product);
        initXml();
        initObj();
        checkType();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            setStatus();
            getCategoryData();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void checkType() {
        type = getIntent().getStringExtra("type");
        if (type.equals("update")) {
            SellerPopularProduct sellerPopularProduct = (SellerPopularProduct) getIntent().getParcelableExtra("data");
            if (sellerPopularProduct!= null) {
                v_product_id = sellerPopularProduct.getProductId();
                title_tv.setText("Seller My Service Edit Product");
                submit_bt.setText("Update");
                desc_et.setText(sellerPopularProduct.getDescription());
                pname_et.setText(sellerPopularProduct.getProductName());
                mprice_et.setText(sellerPopularProduct.getProductPrice());
                String available = sellerPopularProduct.getAvailability();
                if (available.equals("in_stock")){
                    instock_rb.setChecked(true);
                }else {
                    outstock_rb.setChecked(true);
                }
            }
        }
    }

    private void setStatus() {

        status_list = new ArrayList<>();
        status_list.add("Active");
        status_list.add("InActive");
        ArrayAdapter<String> status_adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, status_list);
        status_sp.setAdapter(status_adapter);
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_selladdproduct_back);
        cate_sp = findViewById(R.id.sp_selladdproduct_category);
        subcate_sp = findViewById(R.id.sp_selladdproduct_subcategory);
        subsubcate_sp = findViewById(R.id.sp_selladdproduct_subsubcategory);
        status_sp = findViewById(R.id.sp_selladdproduct_status);
        pname_et = findViewById(R.id.et_selladdproduct_pname);
        mprice_et = findViewById(R.id.et_selladdproduct_mprice);
        oprice_et = findViewById(R.id.et_selladdproduct_oprice);
        percent_et = findViewById(R.id.et_selladdproduct_percent);
        desc_et = findViewById(R.id.et_selladdproduct_description);
        instock_rb = findViewById(R.id.rb_selladdproduct_in);
        outstock_rb= findViewById(R.id.rb_selladdproduct_out);
        submit_bt = findViewById(R.id.bt_selladdproduct_submit);
        browse_tv = findViewById(R.id.tv_selladdproduct_browse);
        filename_tv = findViewById(R.id.tv_selladdproduct_filename);
        title_tv = findViewById(R.id.tv_selladdproduct_title);

        cate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                setSubCateSP(pos);
                if (pos!=-1){
                    v_cate_id = cate_idlist.get(pos);
//                    Utility.toastView(ctx,v_cate_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        subcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    int pos = position - 1;
                    getsubSubCate(pos);
                    if (pos!=-1){
                        v_subcate_id = subcate_idlist.get(pos);
//                        Utility.toastView(ctx,v_subcate_id);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subsubcate_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - 1;
                if (pos!=-1){
                    v_subsubcate_id = subsubcate_idlist.get(pos);
                    Utility.toastView(ctx,v_subsubcate_id);
                }else {
                    v_subsubcate_id = "not";
//                    Utility.toastView(ctx,v_subsubcate_id);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                v_subsubcate_id = "not";
//                Utility.toastView(ctx,v_subsubcate_id);
            }
        });

        back_iv.setOnClickListener(this);
        browse_tv.setOnClickListener(this);
        submit_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_selladdproduct_back:
                finish();
                break;
            case R.id.bt_selladdproduct_submit:
                boolean internet = connectionDetector.isConnected();
                if (internet){
                    if (type.equals("update")){
                        submitData();
                    }else {
                        submitData();
                    }
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.no_internet));
                }
                break;

            case R.id.tv_selladdproduct_browse:
                requestStoragePermission();
                break;
        }
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            browseImage();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void submitData() {

//        v_seller_id = sessionManager.getData(SessionManager.KEY_SELLER_ID);
        v_seller_id = "2";
        v_status = status_sp.getSelectedItem().toString();
        v_pname = pname_et.getText().toString();
        v_mprice = mprice_et.getText().toString();
        v_oprice = oprice_et.getText().toString();
        v_percent = percent_et.getText().toString();
        v_desc = desc_et.getText().toString();

        if (instock_rb.isChecked()){
            v_stcok = "In-Stock";
        }else {
            v_stcok = "Out of Stock";
        }

        if (v_pname.equals("")||v_mprice.equals("")||v_oprice.equals("")||v_percent.equals("")||v_desc.equals("")){
            Utility.toastView(ctx,"Enter All Filed");
        }else if (v_subsubcate_id.equals("not")){
            Utility.toastView(ctx,"Selact All Service Category");
        }else if (filename.equals("")){
            Utility.toastView(ctx,"Please select file");
        }else {
            if (type.equals("update")){
                updateApi();
            }else {
                callApi();
            }
        }
    }

    private void callApi() {

        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("uploading...");
        pd.show();

        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody seller_id = RequestBody.create(MediaType.parse("text/plain"), v_seller_id);
        RequestBody cate_id = RequestBody.create(MediaType.parse("text/plain"), v_cate_id);
        RequestBody subcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subcate_id);
        RequestBody subsubcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subsubcate_id);
        RequestBody product_name = RequestBody.create(MediaType.parse("text/plain"), v_pname);
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), v_mprice);
        RequestBody d_price = RequestBody.create(MediaType.parse("text/plain"), v_oprice);
        RequestBody d_percent = RequestBody.create(MediaType.parse("text/plain"), v_percent);
        RequestBody stock = RequestBody.create(MediaType.parse("text/plain"), v_stcok);
        RequestBody descr = RequestBody.create(MediaType.parse("text/plain"), v_desc);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), v_status);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
        Call<AddService> fileUpload = uploadImage.uploadProductFile(fileToUpload,
                seller_id,cate_id,subcate_id,subsubcate_id,product_name
                ,price,d_price,d_percent,descr,stock,status);
        fileUpload.enqueue(new Callback<AddService>() {
            @Override
            public void onResponse(Call<AddService> call, Response<AddService> response) {
                pd.dismiss();
                Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_LONG).show();
                clear();
                finish();
            }

            @Override
            public void onFailure(Call<AddService> call, Throwable t) {
                pd.dismiss();
                Utility.toastView(ctx,t.getMessage());
                Log.d(TAG, "Error " + t.getMessage());
            }
        });
    }

    private void updateApi() {

        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("updating...");
        pd.show();

        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody seller_id = RequestBody.create(MediaType.parse("text/plain"), v_seller_id);
        RequestBody cate_id = RequestBody.create(MediaType.parse("text/plain"), v_cate_id);
        RequestBody subcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subcate_id);
        RequestBody subsubcate_id = RequestBody.create(MediaType.parse("text/plain"), v_subsubcate_id);
        RequestBody product_name = RequestBody.create(MediaType.parse("text/plain"), v_pname);
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), v_mprice);
        RequestBody d_price = RequestBody.create(MediaType.parse("text/plain"), v_oprice);
        RequestBody d_percent = RequestBody.create(MediaType.parse("text/plain"), v_percent);
        RequestBody stock = RequestBody.create(MediaType.parse("text/plain"), v_stcok);
        RequestBody descr = RequestBody.create(MediaType.parse("text/plain"), v_desc);
        RequestBody status = RequestBody.create(MediaType.parse("text/plain"), v_status);
        RequestBody product_id = RequestBody.create(MediaType.parse("text/plain"), v_product_id);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);
        Call<UpdateResponse> fileUpload = uploadImage.upDateProductDetails(fileToUpload,
                seller_id,cate_id,subcate_id,subsubcate_id,product_name
                ,price,d_price,d_percent,descr,stock,status,product_id);
        fileUpload.enqueue(new Callback<UpdateResponse>() {
            @Override
            public void onResponse(Call<UpdateResponse> call, Response<UpdateResponse> response) {
                pd.dismiss();
                Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_LONG).show();
                clear();
                finish();
            }

            @Override
            public void onFailure(Call<UpdateResponse> call, Throwable t) {
                pd.dismiss();
                Utility.toastView(ctx,t.getMessage());
                Log.d(TAG, "Error " + t.getMessage());
            }
        });
    }

    private void clear() {
        pname_et.setText("");
        mprice_et.setText("");
        oprice_et.setText("");
        percent_et.setText("");
        desc_et.setText("");
        filename_tv.setText("");
    }

    private void browseImage() {
        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image/*");
        startActivityForResult(openGalleryIntent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, SellerAddProductActivity.this);
            file = new File(filePath);
            Log.d(TAG, "Filename " + file.getName());
            filename = file.getName();
            filename_tv.setText(filename);
            //RequestBody mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void getCategoryData() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_GET_QUOTE_DATA_CATE)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        data = response.toString();
                        setCateResponse(response.toString());
                    }


                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setCateResponse(String s) {

        cate_idlist = new ArrayList<>();
        cate_list = new ArrayList<>();
        cate_list.add("Select Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String cate_name = cate_obj.getString("category_name");
                    String cate_id = cate_obj.getString("category_id");
                    cate_list.add(cate_name);
                    cate_idlist.add(cate_id);
                }
                ArrayAdapter<String> cate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cate_list);
                cate_sp.setAdapter(cate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setSubCateSP(int pos) {

        subcate_idlist = new ArrayList<>();
        ArrayList<String> subcate_list = new ArrayList<>();
        subcate_list.add("Select sub category");
        try {
            JSONObject jsonObject = new JSONObject(data);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                JSONObject cate_obj = data_array.getJSONObject(pos);

                JSONArray subcate_array = cate_obj.getJSONArray("sub_category");
                for (int i = 0; i < subcate_array.length(); i++) {
                    JSONObject sub_object = subcate_array.getJSONObject(i);

                    String sub_category_name = sub_object.getString("sub_category_name");
                    String sub_category_id = sub_object.getString("sub_category_id");
                    subcate_list.add(sub_category_name);
                    subcate_idlist.add(sub_category_id);
                }
                ArrayAdapter<String> subcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subcate_list);
                subcate_sp.setAdapter(subcate_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getsubSubCate(int pos) {

        String id = subcate_idlist.get(pos);
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_QUOTE_DATA_SUBSUBCATE)
                .addBodyParameter("subcategory_id", id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        subdata = response.toString();
                        setsubsubCateResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setsubsubCateResponse(String s) {

        subsubcate_idlist = new ArrayList<>();
        subsubcate_list = new ArrayList<>();
        subsubcate_list.add("Select sub sub Category");
        try {
            JSONObject jsonObject = new JSONObject(s);
            String status = jsonObject.getString("status");
            if (status.equals("1")) {
                JSONArray data_array = jsonObject.getJSONArray("data");
                for (int i = 0; i < data_array.length(); i++) {
                    JSONObject cate_obj = data_array.getJSONObject(i);

                    String subsubcate_name = cate_obj.getString("subsubcategory_name");
                    String subsubcate_id= cate_obj.getString("subsubcategory_id");
                    subsubcate_list.add(subsubcate_name);
                    subsubcate_idlist.add(subsubcate_id);
                }
                ArrayAdapter<String> subsubcate_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, subsubcate_list);
                subsubcate_sp.setAdapter(subsubcate_adapter);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
