package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.ApiClient;
import igrand.technology.info.eganacsi.ApiInterface;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.C_SellerProfileResponse;
import igrand.technology.info.eganacsi.model.Navigation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamlesh on 2/17/2018.
 */
public class NavigationAdapter extends BaseAdapter {
    ApiInterface apiInterface;
    C_SellerProfileResponse c_sellerProfileResponse;
    Context ctx;
    ArrayList<Navigation> nav_list;
    String stauts;

    public NavigationAdapter(Context ctx, ArrayList<Navigation> nav_list) {
        this.ctx = ctx;
        this.nav_list = nav_list;
    }

    @Override
    public int getCount() {
        return nav_list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_navigation, null);

        LinearLayout linearLayout = view.findViewById(R.id.ll_adpnavigation_ll);
        ImageView imageView = (ImageView) view.findViewById(R.id.iv_nav_image);
        TextView name_tv = (TextView) view.findViewById(R.id.tv_adpnav_name);

        String name = nav_list.get(8).getName();
        SharedPreferences sharedPreferences =ctx.getSharedPreferences("loginDetails", Context.MODE_PRIVATE);
        String email = sharedPreferences.getString("email","");
        String password = sharedPreferences.getString("password","");

        apiInterface= ApiClient.getClient1().create(ApiInterface.class);
        Call<C_SellerProfileResponse> call=apiInterface.CustomerSellerProfile(email,password);
        call.enqueue(new Callback<C_SellerProfileResponse>() {
            @Override
            public void onResponse(Call<C_SellerProfileResponse> call, Response<C_SellerProfileResponse> response) {
                try {
                    if (response.isSuccessful());
                    c_sellerProfileResponse=response.body();
                    stauts=c_sellerProfileResponse.status.toString();


                }catch (NullPointerException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<C_SellerProfileResponse> call, Throwable t) {

            }
        });


        if (position==7){
            if (stauts=="1"){

                name_tv.setText("Seller Tools");
                linearLayout.setVisibility(View.GONE);
            }else if (stauts=="0"){
                name_tv.setText("Sell on Eganasci");
                linearLayout.setVisibility(View.GONE);

            }



        }
//        if (position==8||position==9||position==10||position==11||position==12||position==13){
//            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//
//            layoutParams.setMargins(20, 00, 10, 0);
//            linearLayout.setLayoutParams(layoutParams);
//            name_tv.setText(nav_list.get(position).getName());
//            imageView.setImageResource(nav_list.get(position).getImage());
//
//        }else
            if (position==14){
                imageView.setVisibility(View.GONE);
            name_tv.setTextSize(15);
            name_tv.setText(nav_list.get(position).getName());
        }else {
            imageView.setImageResource(nav_list.get(position).getImage());
            name_tv.setText(nav_list.get(position).getName());
        }
        return view;
    }
}