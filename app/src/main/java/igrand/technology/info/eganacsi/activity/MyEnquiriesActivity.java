package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.adapter.EnquiryAdapter;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.Enquiry;
import igrand.technology.info.eganacsi.util.Utility;

public class MyEnquiriesActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    RecyclerView recyclerView;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_enquiries);
        initXml();
        setAdapter();
        String cust_id = sessionManager.getData(SessionManager.KEY_CID);
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getEnquiry(cust_id);
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void setAdapter() {

    }

    private void initXml() {
        ctx = this;
        back_iv = (ImageView) findViewById(R.id.iv_myenquiries_back);
        recyclerView = (RecyclerView) findViewById(R.id.rv_myenquiries_recyclerview);

        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getEnquiry(String cust_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_ENQUIRY)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        ArrayList<Enquiry> enquiry_list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONObject data_obj = object.getJSONObject("data");

            JSONArray array_enq = data_obj.getJSONArray("enquiry");
            for (int i = 0; i < array_enq.length(); i++) {
                JSONObject jsonObject = array_enq.getJSONObject(i);

                String en_id = jsonObject.getString("en_id");
                String c_id = jsonObject.getString("customer_id");
                String seller_id = jsonObject.getString("seller_id");
                String product_id = jsonObject.getString("product_id");
                String service_id = jsonObject.getString("service_id");
                String pro_name = jsonObject.getString("product_name");
                String service_name = jsonObject.getString("service_name");
                String c_name = jsonObject.getString("c_name");
                String c_phone = jsonObject.getString("c_phone");
                String c_email = jsonObject.getString("c_email");
                String msg = jsonObject.getString("message");
                String date = jsonObject.getString("datetime");
                String appointment = jsonObject.getString("appointment");
                String seller_name=jsonObject.getString("seller_name");



                enquiry_list.add(new Enquiry(en_id, c_id, seller_id, product_id, pro_name,service_name, c_name, c_phone, c_email, msg, date, appointment,seller_name));

            }
            EnquiryAdapter mAdapter = new EnquiryAdapter(enquiry_list,ctx);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
