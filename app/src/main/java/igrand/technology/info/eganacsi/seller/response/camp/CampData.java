package igrand.technology.info.eganacsi.seller.response.camp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CampData
{
    @SerializedName("campaign")
    @Expose
    private List<Campaign> campaign = null;

    public List<Campaign> getCampaign() {
        return campaign;
    }

    public void setCampaign(List<Campaign> campaign) {
        this.campaign = campaign;
    }
}
