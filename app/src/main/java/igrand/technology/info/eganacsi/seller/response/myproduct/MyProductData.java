package igrand.technology.info.eganacsi.seller.response.myproduct;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by kamlesh on 5/16/2018.
 */

public class MyProductData
{
    @SerializedName("popular_product")
    @Expose
    private List<SellerPopularProduct> sellerPopularProduct = null;

    public List<SellerPopularProduct> getPopularProduct() {
        return sellerPopularProduct;
    }

    public void setPopularProduct(List<SellerPopularProduct> sellerPopularProduct) {
        this.sellerPopularProduct = sellerPopularProduct;
    }

}
