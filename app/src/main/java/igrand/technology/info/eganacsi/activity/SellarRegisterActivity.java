package igrand.technology.info.eganacsi.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import igrand.technology.info.eganacsi.model.seller_register.SellerRegistrationBean;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.R;

/**
 * Created by Admin on 06-May-18.
 */
public class SellarRegisterActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String SELLER_DETAIL = "seller_detail";
    private EditText fnameET, lnameET, mobile_et, mailET, city_et, areaET, address_line1, addess_line2, pincodeET;
    EditText bussiness_name_ET, bussiness_address_ET, bussiness_city_ET, website_ET;
    Button next_BT;
    private Context context;
    private ImageView back_iv;
    private EditText passET;
    private SellerRegistrationBean bean;
    private EditText cpassET;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_sellar_register);
        initView();
    }

    private void initView() {
        fnameET = findViewById(R.id.fnameET);
        lnameET = findViewById(R.id.lnameET);
        mobile_et = findViewById(R.id.mobile_et);
        mailET = findViewById(R.id.mailET);
        city_et = findViewById(R.id.city_et);
        areaET = findViewById(R.id.areaET);
        address_line1 = findViewById(R.id.address_line1);
        addess_line2 = findViewById(R.id.addess_line2);
        pincodeET = findViewById(R.id.pincodeET);
        bussiness_name_ET = findViewById(R.id.bussiness_name_ET);
        bussiness_address_ET = findViewById(R.id.bussiness_address_ET);
        bussiness_city_ET = findViewById(R.id.bussiness_city_ET);
        website_ET = findViewById(R.id.website_ET);
        next_BT = findViewById(R.id.next_BT);
        back_iv = findViewById(R.id.back_iv);
        passET = findViewById(R.id.passET);
        cpassET = findViewById(R.id.cpassET);
        back_iv.setOnClickListener(this);
        next_BT.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.next_BT:
                if (isValidate() && bean != null) {
                    Intent intent = new Intent(context, SubscriptionPlanActivity.class);
                    intent.putExtra("type","first");
                    intent.putExtra(SELLER_DETAIL, bean);
                    startActivity(intent);
                }
                break;

            case R.id.back_iv:
                finish();
                break;
        }

    }


    public boolean isValidate() {
        String name = fnameET.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Utility.toastView(context, "Please enter first name");
            return false;
        }
        String lname = lnameET.getText().toString();
        if (TextUtils.isEmpty(lname)) {
            Utility.toastView(context, "Please enter last name");
            return false;
        }

        String mob = mobile_et.getText().toString();
        if (TextUtils.isEmpty(mob)) {
            Utility.toastView(context, "Please enter mobile number");
            return false;
        }
        String mail = mailET.getText().toString();
        if (TextUtils.isEmpty(mail)) {
            Utility.toastView(context, "Please enter mail id");
            return false;
        }


        String password = passET.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Utility.toastView(context, "Please enter password");
            return false;
        }

        String city = city_et.getText().toString();
        if (TextUtils.isEmpty(city)) {
            Utility.toastView(context, "Please enter city name");
            return false;
        }

        String area = areaET.getText().toString();
        if (TextUtils.isEmpty(area)) {
            Utility.toastView(context, "Please enter area");
            return false;
        }

        String address = address_line1.getText().toString() + " " + addess_line2.getText().toString();
        if (TextUtils.isEmpty(address)) {
            Utility.toastView(context, "Please enter address");
            return false;
        }
        String picode = pincodeET.getText().toString();

        if (TextUtils.isEmpty(picode)) {
            Utility.toastView(context, "Please enter pincode");
            return false;
        }

        String bname = bussiness_name_ET.getText().toString();

        if (TextUtils.isEmpty(bname)) {
            Utility.toastView(context, "Please enter bussiness name");
            return false;
        }
        String baddess = bussiness_address_ET.getText().toString();

        if (TextUtils.isEmpty(baddess)) {
            Utility.toastView(context, "Please enter address");
            return false;
        }
        String bcity = bussiness_city_ET.getText().toString();

        if (TextUtils.isEmpty(bcity)) {
            Utility.toastView(context, "Please enter city");
            return false;
        }

        String website = website_ET.getText().toString();

        if (TextUtils.isEmpty(website)) {
            Utility.toastView(context, "Please enter website");
            return false;
        }

        bean = new SellerRegistrationBean(name, lname, mob,
                password, mail, city, area, address, picode, bname, baddess, bcity, website, "", "", "", "");
        return true;
    }


}
