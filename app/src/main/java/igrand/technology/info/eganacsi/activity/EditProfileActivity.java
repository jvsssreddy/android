package igrand.technology.info.eganacsi.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import igrand.technology.info.eganacsi.seller.UploadImageInterface;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.api_response.UpdateProfile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by kamlesh on 4/12/2018.
 */

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    ImageView circleImageView;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    EditText fname_et, lname_et, email_et, mobile_et,address,area,city,pincode;
    CheckBox send_cb;
    Button submit_bt;

    public static final int PICK_IMAGE_REQUEST = 1;
    String TAG = "profile Image Upload";
    File file;
    String filename = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            String cust_id = sessionManager.getData(SessionManager.KEY_CID);
            getUserDetails(cust_id);
//            String prof_data = sessionManager.getData(SessionManager.PROFILE_DATA);
//            setResponse(prof_data);
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void getUserDetails(String user_id) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_PROFILE)
                .addBodyParameter("customer_id", user_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        sessionManager.setData(SessionManager.PROFILE_DATA,response.toString());
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_editprofile_back);
        circleImageView = findViewById(R.id.profile_image);
        fname_et = findViewById(R.id.et_update_fname);
        lname_et = findViewById(R.id.et_update_lname);
        email_et = findViewById(R.id.et_update_email);
        mobile_et = findViewById(R.id.et_update_mobile);
        address = findViewById(R.id.et_update_address);
        area = findViewById(R.id.et_update_area);
        city = findViewById(R.id.et_update_city);
        pincode = findViewById(R.id.et_update_pincode);
        send_cb = findViewById(R.id.cb_update_sendmail);
        submit_bt = findViewById(R.id.bt_update_submit);
        submit_bt.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        back_iv.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_editprofile_back:
                finish();
                break;

            case R.id.bt_update_submit:
                getData();
                break;

            case R.id.profile_image:
                requestStoragePermission();
                break;
        }
    }

    private void requestStoragePermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Utility.toastView(ctx, "All permissions are granted!");
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            browseImage();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Utility.toastView(ctx, "Error occurred! ");
//                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void browseImage() {
        Intent openGalleryIntent = new Intent(Intent.ACTION_PICK);
        openGalleryIntent.setType("image/*");
        startActivityForResult(openGalleryIntent, PICK_IMAGE_REQUEST);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();
            String filePath = getRealPathFromURIPath(uri, this);
            file = new File(filePath);
            filename = file.getName();
            Log.d(TAG, "Filename " + file.getName());

            Bitmap bmp = null;
            try {
                bmp = Utility.getBitmapFromUri(uri,ctx);
                circleImageView.setImageBitmap(bmp);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
/*
            try {
                Uri imageUri = data.getData();
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                selectedImage = getResizedBitmap(selectedImage, 400);
                img_base_64 = encodefromBitmap(selectedImage);
                int length = img_base_64.length();

                byte[] decodedString = decode(img_base_64);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                circleImageView.setImageBitmap(decodedByte);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
*/
        }
    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void getData() {
//        Toast.makeText(ctx, "error", Toast.LENGTH_SHORT).show();
        String c_id = sessionManager.getData(SessionManager.KEY_CID);
        String fname = fname_et.getText().toString();
        String lname = lname_et.getText().toString();
        String email = email_et.getText().toString();
        String mobile = mobile_et.getText().toString();
        String add = address.getText().toString();
        String areaa = area.getText().toString();
        String cityy = city.getText().toString();
        String pin = pincode.getText().toString();
        if (fname.equals("")) {
            Utility.toastView(ctx, "Enter First name");
        } else if (lname.equals("")) {
            Utility.toastView(ctx, "Enter Last name");
        } else if (!Utility.checkMailPatteren(email)) {
            Utility.toastView(ctx, "Invalid email");
        } else {
            boolean internet = connectionDetector.isConnected();
            if (internet) {
                if (!filename.equals("")){
                    updateProfilewithretro(c_id, fname, lname, mobile, email, add,areaa,cityy,pin);
                }else {
                    Toast.makeText(ctx,"Please select image",Toast.LENGTH_LONG).show();
                }
            } else {
                Utility.toastView(ctx, ctx.getString(R.string.no_internet));
            }
        }
    }

    private void updateProfilewithretro(String c_id, String fname, String lname, String mobile, String email, String add,String areaa,String cityy,String pin) {



        final ProgressDialog pd = new ProgressDialog(ctx);
        pd.setMessage("updating...");
        pd.show();

        File red_file = Utility.reduceImagefile(file);

        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), red_file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), mFile);
//            RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());
        RequestBody cust_id = RequestBody.create(MediaType.parse("text/plain"), c_id);
        RequestBody fi_name = RequestBody.create(MediaType.parse("text/plain"), fname);
        RequestBody la_name = RequestBody.create(MediaType.parse("text/plain"), lname);
        RequestBody emailid = RequestBody.create(MediaType.parse("text/plain"), email);
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), mobile);
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"),cityy);
        RequestBody area = RequestBody.create(MediaType.parse("text/plain"), areaa);
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), add);
        RequestBody pinn=RequestBody.create(MediaType.parse("text/plain"),pin);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        UploadImageInterface uploadImage = retrofit.create(UploadImageInterface.class);


        Call<UpdateProfile> fileUpload = uploadImage.updateprofile(fileToUpload, cust_id, fi_name,
                la_name, emailid, phone, city, area, address);
        fileUpload.enqueue(new Callback<UpdateProfile>() {
            @Override
            public void onResponse(Call<UpdateProfile> call, Response<UpdateProfile> response) {
                if (response.isSuccessful());
                UpdateProfile updateProfile=response.body();
                if (updateProfile.getStatus()==1){
                    pd.dismiss();
                    String msg = response.body().getMessage();
                    Toast.makeText(ctx, "Success " + msg, Toast.LENGTH_LONG).show();
                    startActivity(new Intent(ctx,MainActivity.class));
                    finish();
                }else if (updateProfile.getStatus()==0){
                    Toast.makeText(ctx, "error", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<UpdateProfile> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG, "Error " + t.getMessage());
                Utility.toastView(ctx, t.getMessage());
            }
        });
    }

    private void setResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONObject data_obj = object.getJSONObject("data");
                JSONObject customer_details_object = data_obj.getJSONObject("customer_details");

                String fname = customer_details_object.getString("first_name");
                String lname = customer_details_object.getString("last_name ");
                String email = customer_details_object.getString("email");
                String city = customer_details_object.getString("city");
                String area = customer_details_object.getString("area");
                String address1 = customer_details_object.getString("address1");
                String address2 = customer_details_object.getString("address2");
                String pin = customer_details_object.getString("pin");
                String image = customer_details_object.getString("image");

                fname_et.setText(fname);
                lname_et.setText(lname);
//                mobile_et.setText(mobile);
                email_et.setText(email);


                if (!image.equals("http://igrand.info/egns/")){
                    Utility.loadImage(ctx,circleImageView,image);
                }

                /*if (!image.equals("http://igrand.info/egns/")) {

                    Glide.with(ctx)
                            .load(image)
                            .into(circleImageView);
                }*/
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
