package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/28/2018.
 */

public class PopularService1 {


    @SerializedName("service_id")
    @Expose
    public String serviceId;
    @SerializedName("service_name ")
    @Expose
    public String serviceName;
    @SerializedName("image")
    @Expose
    public String image;
    @SerializedName("price")
    @Expose
    public String price;
    @SerializedName("discount_price")
    @Expose
    public String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    public String discountPercent;
    @SerializedName("category_id")
    @Expose
    public String categoryId;
    @SerializedName("category_name")
    @Expose
    public String categoryName;
    @SerializedName("subcategory_id")
    @Expose
    public String subcategoryId;
    @SerializedName("subcategory_name")
    @Expose
    public String subcategoryName;
    @SerializedName("subsubcategory_id")
    @Expose
    public String subsubcategoryId;
    @SerializedName("subsubcategory_name")
    @Expose
    public String subsubcategoryName;
    @SerializedName("seller_id")
    @Expose
    public String sellerId;
    @SerializedName("sort_order")
    @Expose
    public String sortOrder;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("serviceId", serviceId).append("serviceName", serviceName).append("image", image).append("price", price).append("discountPrice", discountPrice).append("discountPercent", discountPercent).append("categoryId", categoryId).append("categoryName", categoryName).append("subcategoryId", subcategoryId).append("subcategoryName", subcategoryName).append("subsubcategoryId", subsubcategoryId).append("subsubcategoryName", subsubcategoryName).append("sellerId", sellerId).append("sortOrder", sortOrder).toString();
    }
}
