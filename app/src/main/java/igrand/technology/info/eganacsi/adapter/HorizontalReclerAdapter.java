package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.activity.BookServiceActivity;
import igrand.technology.info.eganacsi.activity.BookServiceActivity1;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.activity.GetQuoteActivity1;
import igrand.technology.info.eganacsi.model.FeaturedProduct;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.FeaturedService;
import igrand.technology.info.eganacsi.model.PopularProduct;
import igrand.technology.info.eganacsi.model.PopularProduct1;
import igrand.technology.info.eganacsi.model.PopularService;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;


public class HorizontalReclerAdapter extends RecyclerView.Adapter<HorizontalReclerAdapter.Holder> {

    private HomeAdapter.OnClickListener listener;
    Context context;
    int viewtype;
    ArrayList list;

    String data;
    SessionManager sessionManager;
    ArrayList<String>wishlist_idlist;

    public HorizontalReclerAdapter(Context context, int viewtype, ArrayList list, HomeAdapter.OnClickListener listener) {
        this.context = context;
        this.viewtype = viewtype;
        this.list = list;
        this.listener = listener;
        wishlist_idlist = new ArrayList<>();
        sessionManager = new SessionManager(context);
        data = sessionManager.getData(SessionManager.KEY_WISHLIST_DATA);
        setWishlistResponse(data);
    }



    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (viewtype == HomeAdapter.ViewHandler.popular_service || viewtype == HomeAdapter.ViewHandler.featured_sevice) {
            view = inflater.inflate(R.layout.service_view, null);
        } else {
            view = inflater.inflate(R.layout.product_view, null);
        }
        Holder holder = new Holder(view);
        return holder;
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int pos = (int) view.getTag();
            listener.onAdapterClick(view, pos, viewtype);

        }
    };

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        holder.like_image.setTag(position);
//        holder.getquot_bt.setTag(position);
        holder.detail_bt.setTag(position);
//        holder.bookserv.setTag(position);

        holder.product_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAdapterClick(view, position, viewtype);
            }
        });
        holder.like_image.setOnClickListener(clickListener);
        holder.detail_bt.setOnClickListener(clickListener);

        if (viewtype == HomeAdapter.ViewHandler.slider_view) {

            PopularProduct popularProduct = (PopularProduct) list.get(0);
//            Glide.with(context).load(popularProduct.getProductImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
            Utility.loadImage(context,holder.product_image,popularProduct.getProductImage());
            holder.product_title.setText(popularProduct.getProductName()
            );

        } else if (viewtype == HomeAdapter.ViewHandler.popular_product) {
            final PopularProduct popularProduct = (PopularProduct) list.get(position);
            String image = popularProduct.getProductImage();
            Picasso.with(context).load(image)
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.error)
                    .into(holder.product_image);

            String name =popularProduct.getProductName();
            holder.product_title.setText(name);
            String pricee = popularProduct.getProductPrice();
            holder.price.setText("$ "+pricee);

            holder.getquot_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, GetQuoteActivity1.class);

                    intent.putExtra("pcategoryname",popularProduct.getCategoryName());
                    intent.putExtra("psubcategoryname",popularProduct.getSubcategoryName());
                    intent.putExtra("psubsubcategoryname",popularProduct.getSubsubcategoryName());
                    intent.putExtra("productname",popularProduct.getProductName());

                    intent.putExtra("pcategoryid",popularProduct.getCategoryId());
                    intent.putExtra("psubcategoryid",popularProduct.getSubcategoryId());
                    intent.putExtra("psubsubcategoryid",popularProduct.getSubsubcategoryId());
                    intent.putExtra("productid",popularProduct.getProductId());

                    context.startActivity(intent);
                }
            });

            if (wishlist_idlist.contains(popularProduct.getProductId()+"p")){
                holder.like_image.setImageResource(R.drawable.like_red_icon);
            }

        } else if (viewtype == HomeAdapter.ViewHandler.popular_service) {

            final PopularService popularService = (PopularService) list.get(position);
            if (wishlist_idlist.contains(popularService.getServiceId()+"s")){
                holder.like_image.setImageResource(R.drawable.like_red_icon);
            }
            Glide.with(context)
                    .load(popularService.getImage())
                    .into(holder.product_image)
                    .onLoadFailed(null, context.getDrawable(R.drawable.error));
            holder.product_title.setText(popularService.getServiceName());
            String pricee = popularService.getPrice();
            holder.price.setText("$ "+pricee);

            holder.getquot_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BookServiceActivity1.class);
                    intent.putExtra("categoryname",popularService.getCategoryName());
                    intent.putExtra("subcategoryname",popularService.getSubcategoryName());
                    intent.putExtra("subsubcategoryname",popularService.getSubsubcategoryName());
                    intent.putExtra("servicename",popularService.getServiceName());

                    intent.putExtra("pcategoryid",popularService.getCategoryId());
                    intent.putExtra("psubcategoryid",popularService.getSubcategoryId());
                    intent.putExtra("psubsubcategoryid",popularService.getSubsubcategoryId());
                    intent.putExtra("productid",popularService.getServiceId());
                    context.startActivity(intent);
                }
            });


        } else if (viewtype == HomeAdapter.ViewHandler.featured_product) {

            final FeaturedProduct featuredProduct = (FeaturedProduct) list.get(position);

            holder.getquot_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, GetQuoteActivity1.class);
//
                    intent.putExtra("pcategoryname",featuredProduct.getCategoryName());
                    intent.putExtra("psubcategoryname",featuredProduct.getSubcategoryName());
                    intent.putExtra("psubsubcategoryname",featuredProduct.getSubsubcategoryName());
                    intent.putExtra("productname",featuredProduct.getProductName());

                    intent.putExtra("pcategoryid",featuredProduct.getCategoryId());
                    intent.putExtra("psubcategoryid",featuredProduct.getSubcategoryId());
                    intent.putExtra("psubsubcategoryid",featuredProduct.getSubsubcategoryId());
                    intent.putExtra("productid",featuredProduct.getProductId());
                    context.startActivity(intent);
                }
            });

            if (wishlist_idlist.contains(featuredProduct.getProductId()+"p")){
                holder.like_image.setImageResource(R.drawable.like_red_icon);
            }
            Glide.with(context).load(featuredProduct.getProductImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
            holder.product_title.setText(featuredProduct.getProductName());
            String pricee = featuredProduct.getProductPrice();
            holder.price.setText("$ "+pricee);


        } else if (viewtype == HomeAdapter.ViewHandler.featured_sevice) {

            final FeaturedService featuredService = (FeaturedService) list.get(position);

            holder.getquot_bt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BookServiceActivity1.class);
                    intent.putExtra("categoryname",featuredService.getCategoryName());
                    intent.putExtra("subcategoryname",featuredService.getSubcategoryName());
                    intent.putExtra("subsubcategoryname",featuredService.getSubsubcategoryName());
                    intent.putExtra("servicename",featuredService.getServiceName());

                    intent.putExtra("pcategoryid",featuredService.getCategoryId());
                    intent.putExtra("psubcategoryid",featuredService.getSubcategoryId());
                    intent.putExtra("psubsubcategoryid",featuredService.getSubsubcategoryId());
                    intent.putExtra("productid",featuredService.getServiceId());
                    context.startActivity(intent);

                }
            });

            if (wishlist_idlist.contains(featuredService.getServiceId()+"s")){
                holder.like_image.setImageResource(R.drawable.like_red_icon);
            }
            Glide.with(context).load(featuredService.getImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
            holder.product_title.setText(featuredService.getServiceName());
            String pricee = featuredService.getPrice();
            holder.price.setText("$ "+pricee);

        } else if (viewtype == HomeAdapter.ViewHandler.all_category) {

//            AllCategory allCategory = (AllCategory) list.get(0);
//            Glide.with(context).load(allCategory.getImage()).into(holder.product_image).onLoadFailed(null, context.getDrawable(R.drawable.no_image));
//            ;
//            holder.product_title.setText(allCategory.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if (list.size()>6){
            return 6;
        }else {
            return list.size();
        }
    }

    class Holder extends RecyclerView.ViewHolder {

        TextView product_title,price;
        ImageView product_image;
        ImageView like_image;
        Button detail_bt, getquot_bt;

        public Holder(View itemView) {
            super(itemView);
            product_image = itemView.findViewById(R.id.product_image);
            product_title = itemView.findViewById(R.id.product_title);
            like_image = itemView.findViewById(R.id.like_image);
            price = itemView.findViewById(R.id.product_price);
            getquot_bt = itemView.findViewById(R.id.getquot_bt);
            detail_bt = itemView.findViewById(R.id.detail_bt);



        }
    }

    private void setWishlistResponse(String s) {

        wishlist_idlist = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")){
                JSONObject data_obj = object.getJSONObject("data");
                JSONArray wish_arry = data_obj.getJSONArray("wishlist");
                for (int i = 0; i < wish_arry.length(); i++) {

                    JSONObject datajson = wish_arry.getJSONObject(i);
                    String p_id = datajson.getString("p_id");
                    String type = datajson.getString("type");
                    if (type.equals("product")){
                        wishlist_idlist.add(p_id+"p");
                    }else {
                        wishlist_idlist.add(p_id+"s");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
