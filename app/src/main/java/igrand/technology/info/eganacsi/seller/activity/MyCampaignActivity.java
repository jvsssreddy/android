package igrand.technology.info.eganacsi.seller.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.seller.response.camp.CampResponse;
import igrand.technology.info.eganacsi.seller.response.camp.Campaign;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.seller.adapter.MyCampaignAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCampaignActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    Button new_camp_bt;
    RecyclerView recyclerView;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_campaign);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getMycampaign("5");
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_mycamp_back);
        new_camp_bt = findViewById(R.id.bt_mycamp_newcamp);
        recyclerView = findViewById(R.id.rv_mycamp_recyclerview);

        back_iv.setOnClickListener(this);
        new_camp_bt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.iv_mycamp_back:
                finish();
                break;

            case R.id.bt_mycamp_newcamp:
                boolean internet = connectionDetector.isConnected();
                if (internet){
                    startActivity(new Intent(ctx,NewCampaignActivity.class));
                }else {
                    Utility.toastView(ctx,ctx.getString(R.string.no_internet));
                }
                break;
        }
    }

    private void getMycampaign(String seller_id) {

        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<CampResponse> call = apiService.getCampaign(seller_id);
        call.enqueue(new Callback<CampResponse>() {
            @Override
            public void onResponse(Call<CampResponse> call, Response<CampResponse> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                String message = response.body().getMessage();
                if (message.equals("Success")) {
                    setAdapter(response);
                } else {
                    Utility.toastView(ctx, "Please try again");
                }
            }

            @Override
            public void onFailure(Call<CampResponse> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Toast.makeText(ctx, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setAdapter(Response<CampResponse> response) {

        List<Campaign> camp_list = response.body().getData().getCampaign();
        MyCampaignAdapter adapter = new MyCampaignAdapter(camp_list, ctx);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }
}
