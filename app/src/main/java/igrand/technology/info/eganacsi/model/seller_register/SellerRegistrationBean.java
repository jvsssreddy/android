package igrand.technology.info.eganacsi.model.seller_register;

import java.io.Serializable;

/**
 * Created by Admin on 06-May-18.
 */

public class SellerRegistrationBean implements Serializable {


   public String firstname;
   public String lastname;
   public String mobile;
   public String password;
   public String email;
   public String city;
   public String area;
   public String addressline1;
   public String addressline2;
   public String pincode;
   public String buisness_name;
   public String buisness_address;
   public String buisness_city;
   public String website_address;
   public String membership;
    public String months;
    public String price;
    public String credits;

    public String getMonths() {
        return months;
    }

    public void setMonths(String months) {
        this.months = months;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public SellerRegistrationBean(String firstname, String lastname, String mobile, String password, String email, String city, String area, String addressline1, String pincode, String buisness_name, String buisness_address, String buisness_city, String website_address, String membership, String months, String price, String credits) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.mobile = mobile;
        this.password = password;
        this.email = email;
        this.city = city;
        this.area = area;
        this.addressline1 = addressline1;
        this.addressline2 = addressline2;
        this.pincode = pincode;
        this.buisness_name = buisness_name;
        this.buisness_address = buisness_address;
        this.buisness_city = buisness_city;
        this.website_address = website_address;
        this.membership = membership;
        this.months = months;
        this.price = price;
        this.credits = credits;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddressline1() {
        return addressline1;
    }

    public void setAddressline1(String addressline1) {
        this.addressline1 = addressline1;
    }

    public String getAddressline2() {
        return addressline2;
    }

    public void setAddressline2(String addressline2) {
        this.addressline2 = addressline2;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getBuisness_name() {
        return buisness_name;
    }

    public void setBuisness_name(String buisness_name) {
        this.buisness_name = buisness_name;
    }

    public String getBuisness_address() {
        return buisness_address;
    }

    public void setBuisness_address(String buisness_address) {
        this.buisness_address = buisness_address;
    }

    public String getBuisness_city() {
        return buisness_city;
    }

    public void setBuisness_city(String buisness_city) {
        this.buisness_city = buisness_city;
    }

    public String getWebsite_address() {
        return website_address;
    }

    public void setWebsite_address(String website_address) {
        this.website_address = website_address;
    }

    public String getMembership() {
        return membership;
    }

    public void setMembership(String membership) {
        this.membership = membership;
    }

}
