package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by administator on 6/26/2018.
 */

public class CustomerDetails {


    @SerializedName("first_name")
    @Expose
    public String firstName;
    @SerializedName("last_name ")
    @Expose
    public String lastName;
    @SerializedName("email")
    @Expose
    public String email;
    @SerializedName("city")
    @Expose
    public String city;
    @SerializedName("area")
    @Expose
    public String area;
    @SerializedName("address1")
    @Expose
    public String address1;
    @SerializedName("address2")
    @Expose
    public String address2;
    @SerializedName("pin")
    @Expose
    public String pin;
    @SerializedName("image")
    @Expose
    public String image;

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("firstName", firstName).append("lastName", lastName).append("email", email).append("city", city).append("area", area).append("address1", address1).append("address2", address2).append("pin", pin).append("image", image).toString();
    }
}
