package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.seller_register.SellerRegistrationBean;
import igrand.technology.info.eganacsi.util.Utility;

import static igrand.technology.info.eganacsi.activity.SellarRegisterActivity.SELLER_DETAIL;

public class SubscriptionPlanActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    ImageView back_iv;
    TextView free_proceed_tv, regular_proceed_tv, premium_proceed_tv;
    Spinner free_sp, regular_sp, premium_sp;

    String subscription_data = "";
    ConnectionDetector connectionDetector;

    public static final String MEMBERSHIP = "membership";
    public static final String MONTH= "month";
    public static final String PRICE = "price";
    String membership, month, price;
    ArrayList<String> free_list, regular_list, premium_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription_plan);
        initXml();
        initObj();
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getplans();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void getplans() {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.get(WebApi.URL_SUBSCRIPTION)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });

    }

    private void setResponse(String s) {
        subscription_data = s;
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONObject data_obj = object.getJSONObject("data");
                JSONArray subs_array = data_obj.getJSONArray("subscriptions");
                for (int i = 0; i < subs_array.length(); i++) {

                    JSONObject membership_object = subs_array.getJSONObject(i);
                    String one_month = membership_object.getString("1_month");
                    String three_month = membership_object.getString("3_month");
                    String six_month = membership_object.getString("6_month");
                    String year = membership_object.getString("12_month");

                    if (i == 0) {
                        free_list.add("1 Month -  $" + one_month);
                        free_list.add("3 Month -  $" + three_month);
                        free_list.add("6 Month -  $" + six_month);
                        free_list.add("12 Month -  $" + year);
                    }
                    if (i == 1) {
                        regular_list.add("1 Month -  $" + one_month);
                        regular_list.add("3 Month -  $" + three_month);
                        regular_list.add("6 Month -  $" + six_month);
                        regular_list.add("12 Month -  $" + year);
                    }
                    if (i == 2) {
                        premium_list.add("1 Month -  $" + one_month);
                        premium_list.add("3 Month -  $" + three_month);
                        premium_list.add("6 Month -  $" + six_month);
                        premium_list.add("12 Month -  $" + year);
                    }
                }
                ArrayAdapter<String> free_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, free_list);
                ArrayAdapter<String> regular_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, regular_list);
                ArrayAdapter<String> premium_adapter =
                        new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, premium_list);

                free_sp.setAdapter(free_adapter);
                regular_sp.setAdapter(regular_adapter);
                premium_sp.setAdapter(premium_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();

        back_iv.setOnClickListener(this);
        free_proceed_tv.setOnClickListener(this);
        regular_proceed_tv.setOnClickListener(this);
        premium_proceed_tv.setOnClickListener(this);

        free_list = new ArrayList<>();
        regular_list = new ArrayList<>();
        premium_list = new ArrayList<>();
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_subscription_back);
        free_proceed_tv = findViewById(R.id.tv_subscription_free_proceed);
        regular_proceed_tv = findViewById(R.id.tv_subscription_regular_proceed);
        premium_proceed_tv = findViewById(R.id.tv_subscription_prem_proceed);
        free_sp = findViewById(R.id.sp_subscription_free);
        regular_sp = findViewById(R.id.sp_subscription_regular);
        premium_sp = findViewById(R.id.sp_subscription_premium);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_subscription_back:
                finish();
                break;

            case R.id.tv_subscription_free_proceed:
                int pos = free_sp.getSelectedItemPosition();
                getData(0, pos);
                break;

            case R.id.tv_subscription_regular_proceed:
                int pos1 = regular_sp.getSelectedItemPosition();
                getData(1, pos1);
                break;

            case R.id.tv_subscription_prem_proceed:
                int pos2 = premium_sp.getSelectedItemPosition();
                getData(2, pos2);
                break;
        }
    }

    private void getData(int membership_pos, int month_pos) {

        try {
            JSONObject object = new JSONObject(subscription_data);
            String status = object.getString("status");
            if (status.equals("1")) {
                JSONObject data_obj = object.getJSONObject("data");
                JSONArray subs_array = data_obj.getJSONArray("subscriptions");

                JSONObject membership_object = subs_array.getJSONObject(membership_pos);
                membership = membership_object.getString("membership");
                if (month_pos == 0) {
                    month = "1 Month";
                    price = membership_object.getString("1_month");
                }
                if (month_pos == 1) {
                    month = "3 Month";
                    price = membership_object.getString("3_month");
                }
                if (month_pos == 2) {
                    month = "6 Month";
                    price = membership_object.getString("6_month");
                }
                if (month_pos == 3) {
                    month = "12 Month";
                    price = membership_object.getString("12_month");
                }
                String type = getIntent().getStringExtra("type");
                if (type.equals("main")){
                    startActivity(new Intent(ctx,SellerRegistrationThirdActivity.class)
                            .putExtra("type",type)
                            .putExtra(MEMBERSHIP,membership)
                            .putExtra(MONTH,month)
                            .putExtra(PRICE,price));
                }else {
                    SellerRegistrationBean bean =  (SellerRegistrationBean) getIntent().getSerializableExtra(SellarRegisterActivity.SELLER_DETAIL);
                    String fname = bean.firstname;
                    startActivity(new Intent(ctx,SellerRegistrationThirdActivity.class)
                            .putExtra("type",type)
                            .putExtra(SELLER_DETAIL,bean)
                            .putExtra(MEMBERSHIP,membership)
                            .putExtra(MONTH,month)
                            .putExtra(PRICE,price));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
