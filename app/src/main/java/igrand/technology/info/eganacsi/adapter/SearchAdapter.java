package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.activity.GetQuoteActivity;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.activity.ServiceDetailActivity;
import igrand.technology.info.eganacsi.model.Wishlist;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.R;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    Context ctx;
    ArrayList<Wishlist> wish_list;
    SessionManager sessionManager;
    String cust_id;

    public static final int PRODUCT_VIEW = 1;
    public static final int SERVICE_VIEW = 2;

    public SearchAdapter(Context ctx, ArrayList<Wishlist> img_list) {
        this.ctx = ctx;
        this.wish_list = img_list;
        sessionManager = new SessionManager(ctx);
        cust_id = sessionManager.getData(SessionManager.KEY_CID);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (viewType == PRODUCT_VIEW) {
            v = inflater.inflate(R.layout.row_product_listing, parent, false);
        } else {
            v = inflater.inflate(R.layout.row_product_listing2, parent, false);
        }
        ViewHolder vh = new ViewHolder(v, viewType);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        Picasso.with(ctx)
                .load(wish_list.get(position).getImage())
                .into(holder.imageView);
        holder.name.setText(wish_list.get(position).getName());
        holder.address.setText(wish_list.get(position).getComp_name());
        holder.location.setText(wish_list.get(position).getComp_address());

        holder.linear_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getItemViewType(position) == PRODUCT_VIEW) {
                    ctx.startActivity(new Intent(ctx, ProductDetailActivity.class)
                            .putExtra("product_id", wish_list.get(position).getP_id()));
                }
                if (getItemViewType(position) == SERVICE_VIEW) {
                    ctx.startActivity(new Intent(ctx, ServiceDetailActivity.class)
                            .putExtra("Service", wish_list.get(position).getP_id()));
                }
            }
        });
        holder.linear_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx, GetQuoteActivity.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return wish_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        TextView address;
        TextView location;
        TextView price;
        ImageView imageView;
        LinearLayout linear_order;
        LinearLayout linear_details;
        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            name = itemView.findViewById(R.id.company_name);
            address = itemView.findViewById(R.id.address);
            location = itemView.findViewById(R.id.location);
            imageView = itemView.findViewById(R.id.product_image1);
            price=itemView.findViewById(R.id.price);
            linear_order=itemView.findViewById(R.id.linear_order);
            linear_details=itemView.findViewById(R.id.linear_details);
        }
    }

    @Override
    public int getItemViewType(int position) {

        String type = wish_list.get(position).getType();
        int viewtype = 0;
        if (type.equals("product")) {
            viewtype = PRODUCT_VIEW;
        }
        if (type.equals("service")) {
            viewtype = SERVICE_VIEW;
        }
        return viewtype;
    }
}