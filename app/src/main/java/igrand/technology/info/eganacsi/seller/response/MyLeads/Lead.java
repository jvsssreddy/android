package igrand.technology.info.eganacsi.seller.response.MyLeads;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lead implements Parcelable
{
    @SerializedName("en_id")
    @Expose
    private String enId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("seller_id")
    @Expose
    private String sellerId;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    private String subsubcategoryId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("c_name")
    @Expose
    private String cName;
    @SerializedName("c_phone")
    @Expose
    private String cPhone;
    @SerializedName("c_email")
    @Expose
    private String cEmail;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("datetime")
    @Expose
    private String datetime;
    @SerializedName("appointment")
    @Expose
    private String appointment;
    @SerializedName("assign")
    @Expose
    private String assign;

    protected Lead(Parcel in) {
        enId = in.readString();
        customerId = in.readString();
        sellerId = in.readString();
        categoryId = in.readString();
        subcategoryId = in.readString();
        subsubcategoryId = in.readString();
        productId = in.readString();
        serviceId = in.readString();
        productName = in.readString();
        serviceName = in.readString();
        cName = in.readString();
        cPhone = in.readString();
        cEmail = in.readString();
        message = in.readString();
        datetime = in.readString();
        appointment = in.readString();
        assign = in.readString();
    }

    public static final Creator<Lead> CREATOR = new Creator<Lead>() {
        @Override
        public Lead createFromParcel(Parcel in) {
            return new Lead(in);
        }

        @Override
        public Lead[] newArray(int size) {
            return new Lead[size];
        }
    };

    public String getEnId() {
        return enId;
    }

    public void setEnId(String enId) {
        this.enId = enId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubsubcategoryId() {
        return subsubcategoryId;
    }

    public void setSubsubcategoryId(String subsubcategoryId) {
        this.subsubcategoryId = subsubcategoryId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public String getCPhone() {
        return cPhone;
    }

    public void setCPhone(String cPhone) {
        this.cPhone = cPhone;
    }

    public String getCEmail() {
        return cEmail;
    }

    public void setCEmail(String cEmail) {
        this.cEmail = cEmail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public String getAssign() {
        return assign;
    }

    public void setAssign(String assign) {
        this.assign = assign;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(enId);
        dest.writeString(customerId);
        dest.writeString(sellerId);
        dest.writeString(categoryId);
        dest.writeString(subcategoryId);
        dest.writeString(subsubcategoryId);
        dest.writeString(productId);
        dest.writeString(serviceId);
        dest.writeString(productName);
        dest.writeString(serviceName);
        dest.writeString(cName);
        dest.writeString(cPhone);
        dest.writeString(cEmail);
        dest.writeString(message);
        dest.writeString(datetime);
        dest.writeString(appointment);
        dest.writeString(assign);
    }
}
