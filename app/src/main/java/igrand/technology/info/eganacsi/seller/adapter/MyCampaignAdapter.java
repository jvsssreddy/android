package igrand.technology.info.eganacsi.seller.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import igrand.technology.info.eganacsi.seller.activity.MyCampViewmoreActivity;
import igrand.technology.info.eganacsi.seller.response.camp.Campaign;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 5/14/2018.
 */

public class MyCampaignAdapter extends RecyclerView.Adapter<MyCampaignAdapter.ViewHolder> {

    List<Campaign> camp_list;
    Context ctx;

    public MyCampaignAdapter(List<Campaign> camp_list, Context ctx) {
        this.camp_list = camp_list;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.adp_mycampaign, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.comp_name_tv.setText("Customer Name : "+camp_list.get(position).getCompaignName());
        holder.noti_tv.setText("No of Notification : "+camp_list.get(position).getNoOfCustomer());
        holder.date_name_tv.setText("Date : "+camp_list.get(position).getDate());

        holder.viewmore_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Campaign campaign = camp_list.get(position);
                ctx.startActivity(new Intent(ctx, MyCampViewmoreActivity.class)
                        .putExtra("data", campaign));
            }
        });
    }

    @Override
    public int getItemCount() {
        return camp_list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView comp_name_tv, noti_tv, date_name_tv, viewmore_tv;

        public ViewHolder(View itemView) {
            super(itemView);
            comp_name_tv = itemView.findViewById(R.id.tv_adpcamp_cname);
            noti_tv = itemView.findViewById(R.id.tv_adpcamp_no_of_noti);
            date_name_tv = itemView.findViewById(R.id.tv_adpcamp_date);
            viewmore_tv = itemView.findViewById(R.id.tv_adpcamp_viewmore);
        }
    }
}
