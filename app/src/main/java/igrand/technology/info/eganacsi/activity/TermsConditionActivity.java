package igrand.technology.info.eganacsi.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.util.Utility;

public class TermsConditionActivity extends AppCompatActivity {

    Context ctx;
    ImageView back_iv;
    TextView description_tv;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_condition);
        initXml();
        boolean internet = connectionDetector.isConnected();
        if (internet){
            getData();
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
    }

    private void getData() {
        //getting the progressbar
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("loading...");
        progressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, WebApi.URL_TERMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //hiding the progressbar after completion
                        progressDialog.dismiss();
                        setResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setResponse(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);

            String msg = jsonObject.getString("message");
            if (msg.equals("Success")){

                JSONObject object_data = jsonObject.getJSONObject("data");

                String title = object_data.getString("page_name");
                String description = object_data.getString("description");

                description_tv.setText(description);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        ctx =this;
        back_iv = (ImageView)findViewById(R.id.iv_terms_back);
        description_tv = findViewById(R.id.tv_terms_description);
        connectionDetector = new ConnectionDetector();

        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
