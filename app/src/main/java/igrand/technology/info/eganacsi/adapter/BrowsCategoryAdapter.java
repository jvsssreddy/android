package igrand.technology.info.eganacsi.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

import igrand.technology.info.eganacsi.model.CategoryBean;
import igrand.technology.info.eganacsi.R;

/**
 * Created by kamlesh on 4/26/2018.
 */

public class BrowsCategoryAdapter extends RecyclerView.Adapter<BrowsCategoryAdapter.ViewHolder> {


    Context context;
    ArrayList<CategoryBean> mAllcatList;
    private HomeAdapter.OnClickListener listener;

    public BrowsCategoryAdapter(Context context, ArrayList<CategoryBean> mAllcatList, HomeAdapter.OnClickListener listener) {
        this.context = context;
        this.mAllcatList = mAllcatList;
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.all_categories_grid_list, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            GridAdapter gridAdapter = new GridAdapter(context, HomeAdapter.ViewHandler.all_category, mAllcatList, listener);
            holder.recyclerView.setAdapter(gridAdapter);

//        else {
//
//            GridAdapter gridAdapter = new GridAdapter(context, HomeAdapter.ViewHandler.all_category,mAllcatList, listener);
//            holder.bookCatTv.setText("Service Category");
//            holder.allCatTV.setText("");
//            holder.bookCatTv.setTextColor(context.getColor(R.color.text_dark_red));
//            holder.recyclerView.setAdapter(gridAdapter);
//        }

    }

    interface ViewType {
        int ALLCATEGORY = 0;
        int BOOKCATEGORY = 1;
    }

    @Override
    public int getItemCount() {
        return 1;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        RecyclerView recyclerView;
        TextView allCatTV, bookCatTv;

        public ViewHolder(View itemView) {
            super(itemView);

//            bookCatTv = itemView.findViewById(R.id.book_category);
            allCatTV = itemView.findViewById(R.id.all_category);
            recyclerView = itemView.findViewById(R.id.recyclerview_category);
            GridLayoutManager manager = new GridLayoutManager(context, 3);
            recyclerView.setLayoutManager(manager);
        }
    }
}
