package igrand.technology.info.eganacsi.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import igrand.technology.info.eganacsi.network.ApiInterface;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.service_detail.ServiceDeatail;
import igrand.technology.info.eganacsi.model.service_detail.ServiceDetailBean;
import igrand.technology.info.eganacsi.network.ApiClient;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kamlesh on 4/13/2018.
 */

public class ServiceDetailActivity extends AppCompatActivity implements View.OnClickListener {

    Context ctx;
    boolean like;
    ImageView back_iv, search_iv, menu_iv, main_iv, like_iv;
    TextView name_tv, new_price_tv, old_price_tv, save_tv, dealer_tv,
            address_tv, rating_tv, no_person_tv, sellername_tv, call_tv, availble_tv, getquote_tv, add_wishlist_tv;

    TextView sun_tv, mon_tv, tues_tv, wed_tv, thurs_tv, fri_tv, sat_tv;
    ArrayList<String> wishlists_list;
    private ServiceDeatail detail;
    private String TAG = "ServiceDetailActivity";
    private String service_id, cust_id;

    SessionManager sessionManager;
    ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service_detail_layout);
        initXml();
        initObj();
        Intent intent = getIntent();
        if (intent != null) {
            service_id = intent.getStringExtra("Service");
        }
        boolean internet = connectionDetector.isConnected();
        if (internet) {
            getData();
        } else {
            Utility.toastView(ctx, ctx.getString(R.string.no_internet));
        }
    }

    private void initObj() {
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
    }

    private void setData() {

        name_tv.setText(detail.getServiceName());
        Glide.with(ctx).load(detail.getProductImage()).into(main_iv).onLoadFailed(null, ctx.getDrawable(R.drawable.no_image));
        old_price_tv.setText(detail.getPrice());
        new_price_tv.setText("₹ " + detail.getDiscountPrice());

        old_price_tv.setPaintFlags(old_price_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        float old_price = Float.parseFloat(detail.getPrice());
        float new_price = Float.parseFloat(detail.getDiscountPrice());

        float save = old_price - new_price;
        save_tv.setText("save " + save);
        address_tv.setText(detail.getArea() + "," + detail.getCity());
        sellername_tv.setText(detail.getSellerName());
        rating_tv.setText(detail.getRating() + "/5");
//        no_person_tv.setText(detail.get);
        call_tv.setText(detail.getPhone());

        if (detail.getSunday().equals("0")) {
            sun_tv.setVisibility(View.GONE);
        }
        if (detail.getMonday().equals("0")) {
            mon_tv.setVisibility(View.GONE);
        }
        if (detail.getTuesday().equals("0")) {
            tues_tv.setVisibility(View.GONE);
        }
        if (detail.getWednesday().equals("0")) {
            wed_tv.setVisibility(View.GONE);
        }
        if (detail.getThursday().equals("0")) {
            thurs_tv.setVisibility(View.GONE);
        }
        if (detail.getFriday().equals("0")) {
            fri_tv.setVisibility(View.GONE);
        }
        if (detail.getSaturday().equals("0")) {
            sat_tv.setVisibility(View.GONE);
        }
    }

    private void initXml() {
        ctx = this;
        back_iv = findViewById(R.id.iv_productdetail_back);
        search_iv = findViewById(R.id.iv_productdetail_search);
        like_iv = findViewById(R.id.iv_productdetail_like);
        menu_iv = findViewById(R.id.iv_productdetail_menu);
        main_iv = findViewById(R.id.iv_productdetail_image);
        name_tv = findViewById(R.id.tv_productdetail_name);
        new_price_tv = findViewById(R.id.tv_productdetail_offerprice);
        old_price_tv = findViewById(R.id.old_price_tv);
        save_tv = findViewById(R.id.tv_productdetail_savemoney);
        dealer_tv = findViewById(R.id.tv_productdetail_dealer);
        address_tv = findViewById(R.id.tv_productdetail_address);
        rating_tv = findViewById(R.id.tv_productdetail_rating);
        no_person_tv = findViewById(R.id.tv_productdetail_noofperson);
        sellername_tv = findViewById(R.id.tv_productdetail_sellername);
        call_tv = findViewById(R.id.tv_productdetail_sellercontact);
        availble_tv = findViewById(R.id.tv_productdetail_instock);
        getquote_tv = findViewById(R.id.tv_productdetail_book);
        add_wishlist_tv = findViewById(R.id.tv_productdetail_addwishlist);

        ///days
        sun_tv = findViewById(R.id.tv_servdet_sun);
        mon_tv = findViewById(R.id.tv_servdet_mon);
        tues_tv = findViewById(R.id.tv_servdet_tue);
        wed_tv = findViewById(R.id.tv_servdet_wed);
        thurs_tv = findViewById(R.id.tv_servdet_thurs);
        fri_tv = findViewById(R.id.tv_servdet_fri);
        sat_tv = findViewById(R.id.tv_servdet_sat);

        back_iv.setOnClickListener(this);
        menu_iv.setOnClickListener(this);
        like_iv.setOnClickListener(this);
        search_iv.setOnClickListener(this);
        getquote_tv.setOnClickListener(this);
        add_wishlist_tv.setOnClickListener(this);
        call_tv.setOnClickListener(this);
    }

    public void getData() {
        cust_id = sessionManager.getData(SessionManager.KEY_CID);
        Utility.showLoader(ctx);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ServiceDetailBean> call = apiService.getServiceDetail(service_id);
        call.enqueue(new Callback<ServiceDetailBean>() {
            @Override
            public void onResponse(Call<ServiceDetailBean> call, Response<ServiceDetailBean> response) {
                Utility.hideLoader();
                int statusCode = response.code();
                detail = response.body().getData();
                Log.e(TAG, "onResponse: " + detail.getServiceName());
//                Toast.makeText(ServiceDetailActivity.this, detail.getServiceName(), Toast.LENGTH_SHORT).show();
                getWishlist(cust_id);
            }

            @Override
            public void onFailure(Call<ServiceDetailBean> call, Throwable t) {
                // Log error here since request failed
                Utility.hideLoader();
                Log.e(TAG, t.toString());
                Toast.makeText(ServiceDetailActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.iv_productdetail_back:
                finish();
                break;
            case R.id.iv_productdetail_search:
                sessionManager.setBooleanData(SessionManager.KEY_MAIN_ACTIVITY_TYPE,true);
                startActivity(new Intent(ctx,MainActivity.class));
                break;

            case R.id.iv_productdetail_like:
                if (like) {
                    dislikeproductandService(cust_id, "", detail.getServiceId(), v);
                } else {
                    likeproductandService(cust_id, "", detail.getServiceId(), v);
                }
                break;

            case R.id.tv_productdetail_sellercontact:
                requestCallPermission();
                break;

            case R.id.iv_productdetail_menu:
                break;

            case R.id.tv_productdetail_book:
                startActivity(new Intent(ctx, BookServiceActivity.class));
                break;

            case R.id.tv_productdetail_addwishlist:
                if (wishlists_list.contains(detail.getServiceId())) {
                    like_iv.setImageResource(R.drawable.like_red_icon);
                    Utility.toastView(ctx, "already added in your wishlist");
                } else {
                    likeproductandService(cust_id, "", detail.getServiceId(), v);
                }
                break;
        }
    }

    private void requestCallPermission() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CALL_PHONE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                            Toast.makeText(getApplicationContext(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                            calling();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    private void calling() {
        String no = call_tv.getText().toString();
        Intent inte = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + no));
        startActivity(inte);
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void getWishlist(String cust_id) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        setData();
                        setResponse(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setResponse(String s) {

        wishlists_list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONObject data_obj = object.getJSONObject("data");
            JSONArray wish_arry = data_obj.getJSONArray("wishlist");
            for (int i = 0; i < wish_arry.length(); i++) {

                JSONObject datajson = wish_arry.getJSONObject(i);
                String p_id = datajson.getString("p_id");

                wishlists_list.add(p_id);
            }
            if (wishlists_list.contains(detail.getServiceId())) {
                like_iv.setImageResource(R.drawable.like_red_icon);
                like = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void dislikeproductandService(String cust_id, final String product_id1, final String service_id1, final View view) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_DELETE_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();

                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                like = false;
                                Utility.toastView(ctx, "Remove to wishlist");
                                like_iv.setImageResource(R.drawable.like_fill_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void likeproductandService(String cust_id, String product_id1, String service_id1, final View view) {

        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_ADD_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .addBodyParameter("product_id", product_id1)
                .addBodyParameter("service_id", service_id1)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        try {
                            String status = response.getString("status");
                            if (status.equals("1")) {
                                like = true;
                                Utility.toastView(ctx, "Added to wishlist");
                                like_iv.setImageResource(R.drawable.like_red_icon);
                            } else {
                                Utility.toastView(ctx, "Something went wrong please try again");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }
}