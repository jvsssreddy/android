package igrand.technology.info.eganacsi.network;


import igrand.technology.info.eganacsi.model.ProductListResponse;
import igrand.technology.info.eganacsi.seller.response.MyLeads.MyLeadResponse;
import igrand.technology.info.eganacsi.seller.response.myquoatation.MyQuotationResponse;
import igrand.technology.info.eganacsi.model.ProductDetails.ProductDetailBean;
import igrand.technology.info.eganacsi.model.api_response.NewCampaignResp;
import igrand.technology.info.eganacsi.model.seller_register.Seller_Reg_Reasponse;
import igrand.technology.info.eganacsi.model.service_detail.ServiceDetailBean;
import igrand.technology.info.eganacsi.seller.response.camp.CampResponse;
import igrand.technology.info.eganacsi.seller.response.myproduct.MyProductStatus;
import igrand.technology.info.eganacsi.seller.response.myservices.MyServiceStatus;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by kamlesh on 4/25/2018.
 */
public interface ApiInterface {

//    @GET("movie/top_rated")
//    Call<MoviesResponse> getTopRatedMovies(@Query("product_id") String apiKey);
    @FormUrlEncoded
    @POST("get-service-details.php")
    Call<ServiceDetailBean> getServiceDetail(@Field("service_id") String service_id);

    @FormUrlEncoded
    @POST("get-product-details.php")
    Call<ProductDetailBean> getNewProductDetail(@Field("product_id") String product_id);


    @FormUrlEncoded
    @POST("seller-register.php")
    Call<Seller_Reg_Reasponse> registerSeller(@Field("firstname") String firstname,
                                              @Field("lastname") String lastname,
                                              @Field("mobile") String mobile,
                                              @Field("password") String password,
                                              @Field("email") String email,
                                              @Field("city") String city,
                                              @Field("area") String area,
                                              @Field("addressline1") String addressline1,
                                              @Field("addressline2") String addressline2,
                                              @Field("pincode") String pincode,
                                              @Field("buisness_name") String buisness_name,
                                              @Field("buisness_address") String buisness_address,
                                              @Field("buisness_city") String buisness_city,
                                              @Field("website_address") String website_address,
                                              @Field("membership") String membership,
                                              @Field("months") String months,
                                              @Field("price") String price,
                                              @Field("credits") String credits);


    @FormUrlEncoded
    @POST("add-campaign.php")
    Call<NewCampaignResp> newcampaign(@Field("seller_id") String seller_id,
                                         @Field("city") String city,
                                         @Field("website") String website,
                                         @Field("message") String message,
                                         @Field("number") String number,
                                         @Field("customers") String customers);

    @FormUrlEncoded
    @POST("seller-product.php")
    Call<MyProductStatus> getSellerProducts(@Field("seller_id") String seller_id);

    @FormUrlEncoded
    @POST("seller-service.php")
    Call<MyServiceStatus> getSellerServices(@Field("seller_id") String seller_id);

    @FormUrlEncoded
    @POST("get-quotes.php")
    Call<MyLeadResponse> myLead(@Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("get-my-quotation-request.php")
    Call<MyQuotationResponse> getMyQuote(@Field("seller_id") String seller_id);

    @FormUrlEncoded
    @POST("get-compaign-list.php")
    Call<CampResponse> getCampaign(@Field("seller_id") String seller_id);


    @FormUrlEncoded
    @POST("egn/ss/subsub-category-details.php")
    Call<ProductListResponse> productListing(@Field("subcategory_id") String subcategory_id);

}