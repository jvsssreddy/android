package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.activity.ProductDetailActivity;
import igrand.technology.info.eganacsi.adapter.ViewAllAdapter;
import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 4/23/2018.
 */

@SuppressLint("ValidFragment")
public class SubSubCategoryFragment extends Fragment implements AdapterView.OnItemClickListener {
    View view;
    Context ctx;
    GridView gridView;
    RecyclerView recyclerView;
    EditText et_single_search;

    String service, data;
    int position;
    SessionManager sessionManager;
    ConnectionDetector connectionDetector;
    ArrayList<ViewAll> viewAllArrayList;

    public SubSubCategoryFragment(Context ctx, String service, String data, int position) {
        this.ctx = ctx;
        this.service = service;
        this.data = data;
        this.position = position;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewall, null);

        initXml();
        initObj();
        setResponse(data);
        return view;
    }

    private void initObj() {
        sessionManager = new SessionManager(ctx);
        connectionDetector = new ConnectionDetector();
        Utility.setTitle(ctx, service);

        viewAllArrayList = new ArrayList<>();
    }

    private void setResponse(String s) {
        try {
            JSONObject object = new JSONObject(s);
            String message = object.getString("message");
            if (message.equals("Success")) {
                JSONArray data_array = object.getJSONArray("data");

                JSONObject subsubCate_obj = data_array.getJSONObject(position);
                JSONObject sub_cate_object = subsubCate_obj.getJSONObject("sub_category");
                JSONArray product_array = sub_cate_object.getJSONArray("products");
                for (int i = 0; i < product_array.length(); i++) {

                    JSONObject subsubcategory_obj = product_array.getJSONObject(i);
                    String id = subsubcategory_obj.getString("product_id");
                    String name = subsubcategory_obj.getString("product_name");
                    String image = subsubcategory_obj.getString("image");
                    viewAllArrayList.add(new ViewAll("", id, name, image));
                }

                ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ctx, viewAllArrayList, 3);
                gridView.setAdapter(viewAllAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        et_single_search = view.findViewById(R.id.et_single_search);
        gridView = view.findViewById(R.id.gv_viewall_gridview);
        recyclerView = view.findViewById(R.id.rv_viewall_recyclerview);

        recyclerView.setVisibility(View.GONE);
        gridView.setVisibility(View.VISIBLE);

        View single_seacrh = (View) view.findViewById(R.id.include_viewall_single);
        View double_search = (View) view.findViewById(R.id.include_viewall_double);

        single_seacrh.setVisibility(View.VISIBLE);
        double_search.setVisibility(View.GONE);

        gridView.setOnItemClickListener(this);
        et_single_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFragment searchFragment = new SearchFragment(ctx);
                Utility.setFragment(searchFragment,ctx);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        startActivity(new Intent(ctx, ProductDetailActivity.class)
                .putExtra("product_id", viewAllArrayList.get(position).getId()));
    }
}
