package igrand.technology.info.eganacsi.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by administator on 6/28/2018.
 */

public class MyProductData {


        @SerializedName("popular_product")
        @Expose
        public List<MyProductsList> popularProduct = null;

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("popularProduct", popularProduct).toString();
        }
}

