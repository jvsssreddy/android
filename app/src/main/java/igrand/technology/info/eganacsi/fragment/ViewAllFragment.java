package igrand.technology.info.eganacsi.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import igrand.technology.info.eganacsi.adapter.ViewAllRecyclerAdapter;
import igrand.technology.info.eganacsi.adapter.ViewAllRecyclerAdapter1;
import igrand.technology.info.eganacsi.model.ViewAll1;
import igrand.technology.info.eganacsi.util.ConnectionDetector;
import igrand.technology.info.eganacsi.util.SessionManager;
import igrand.technology.info.eganacsi.util.WebApi;
import igrand.technology.info.eganacsi.R;
import igrand.technology.info.eganacsi.model.ViewAll;
import igrand.technology.info.eganacsi.util.Utility;

/**
 * Created by kamlesh on 4/23/2018.
 */

@SuppressLint("ValidFragment")
public class ViewAllFragment extends Fragment
{
    View view;
    Context ctx;
    GridView gridView;
    RecyclerView recyclerView;
    Spinner location_sp;
    EditText et_double_search;
    String service,cust_id;

    ConnectionDetector connectionDetector;
    SessionManager sessionManager;

    public ViewAllFragment(Context ctx,String service) {
        this.ctx = ctx;
        this.service= service;
        connectionDetector = new ConnectionDetector();
        sessionManager = new SessionManager(ctx);
//        sessionManager.setData(SessionManager.KEY_PAGE,"other");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_viewall,null);
        initXml();
        cust_id = sessionManager.getData(SessionManager.KEY_CID);
        boolean internet = connectionDetector.isConnected();
        setResponse();
        if (internet){
            getWishlist(cust_id);
        }else {
            Utility.toastView(ctx,ctx.getString(R.string.no_internet));
        }
        return view;
    }


    private void popularProducts(String data) {

        ArrayList<ViewAll1> viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject data_object = jsonObject.getJSONObject("data");

            JSONArray popular_product_array = data_object.getJSONArray("popular_product");
            for (int i = 0; i < popular_product_array.length(); i++) {

                JSONObject object_data = popular_product_array.getJSONObject(i);

                String id1=null,name1=null,name2=null,image1=null,categoryid=null,categoryname=null,subcategoryid=null,subcategoryname=null,subsubcategoryid=null,subsubcategoryname=null;
                if (object_data.has("product_id")){
                    id1 = object_data.getString("product_id");
                }if (object_data.has("product_image")){
                    image1 = object_data.getString("product_image");
                }if (object_data.has("product_price")) {
                    name2 = object_data.getString("product_price");
                }
                if (object_data.has("category_id")) {
                    categoryid = object_data.getString("category_id");
                }
                if (object_data.has("category_name")) {
                    categoryname = object_data.getString("category_name");
                }
                if (object_data.has("subcategory_id")) {
                    subcategoryid = object_data.getString("subcategory_id");
                }
                if (object_data.has("subcategory_name")) {
                    subcategoryname = object_data.getString("subcategory_name");
                }
                if (object_data.has("subsubcategory_id")) {
                    subsubcategoryid = object_data.getString("subsubcategory_id");
                }
                if (object_data.has("subsubcategory_name")) {
                    subsubcategoryname = object_data.getString("subsubcategory_name");
                }
                name1 = object_data.optString("product_name ");
                String type1= "popular product";

                viewAllArrayList.add(new ViewAll1(type1,id1,name1,name2,image1,categoryid,categoryname,subcategoryid,subcategoryname,
                        subsubcategoryid,subsubcategoryname  ));
            }
            ViewAllRecyclerAdapter1 viewAllRecyclerAdapter = new ViewAllRecyclerAdapter1(ctx, viewAllArrayList, 1,cust_id);
            recyclerView.setBackgroundResource(R.color.white);
            recyclerView.setAdapter(viewAllRecyclerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void popularServices(String data) {

        ArrayList<ViewAll1> viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject data_object = jsonObject.getJSONObject("data");

            JSONArray popular_product_array = data_object.getJSONArray("popular_service");
            for (int i = 0; i < popular_product_array.length(); i++) {

                JSONObject object_data = popular_product_array.getJSONObject(i);

                String id1=null,name1=null,name2=null,image1=null,categoryid=null,categoryname=null,subcategoryid=null,subcategoryname=null,subsubcategoryid=null,subsubcategoryname=null;
                if (object_data.has("service_id")){
                    id1 = object_data.getString("service_id");
                }if (object_data.has("service_name ")){
                    name1 = object_data.getString("service_name ");

                }if (object_data.has("image")){
                    image1 = object_data.getString("image");
                }
                if (object_data.has("price")){
                    name2 = object_data.getString("price");
                }

                if (object_data.has("category_id")) {
                    categoryid = object_data.getString("category_id");
                }
                if (object_data.has("category_name")) {
                    categoryname = object_data.getString("category_name");
                }
                if (object_data.has("subcategory_id")) {
                    subcategoryid = object_data.getString("subcategory_id");
                }
                if (object_data.has("subcategory_name")) {
                    subcategoryname = object_data.getString("subcategory_name");
                }
                if (object_data.has("subsubcategory_id")) {
                    subsubcategoryid = object_data.getString("subsubcategory_id");
                }
                if (object_data.has("subsubcategory_name")) {
                    subsubcategoryname = object_data.getString("subsubcategory_name");
                }

                String type1= "popular service";
                viewAllArrayList.add(new ViewAll1(type1,id1,name1,name2,image1,categoryid,categoryname,subcategoryid,subcategoryname,
                        subsubcategoryid,subsubcategoryname));
            }
            ViewAllRecyclerAdapter1 viewAllRecyclerAdapter = new ViewAllRecyclerAdapter1(ctx, viewAllArrayList, 2, cust_id);
            recyclerView.setBackgroundResource(R.color.white);
            recyclerView.setAdapter(viewAllRecyclerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void featuredProducts(String data) {

        ArrayList<ViewAll1> viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject data_object = jsonObject.getJSONObject("data");

            JSONArray popular_product_array = data_object.getJSONArray("featured_product");
            for (int i = 0; i < popular_product_array.length(); i++) {

                JSONObject object_data = popular_product_array.getJSONObject(i);

                String id1=null,name1=null,name2=null,image1=null,categoryid=null,categoryname=null,subcategoryid=null,subcategoryname=null,subsubcategoryid=null,subsubcategoryname=null;
                if (object_data.has("product_id")){
                    id1 = object_data.getString("product_id");
                }if (object_data.has("product_name ")){
                    name1 = object_data.getString("product_name ");
                }if (object_data.has("product_image")){
                    image1 = object_data.getString("product_image");
                }if (object_data.has("product_price")){
                    name2 = object_data.getString("product_price");
                }

                if (object_data.has("category_id")) {
                    categoryid = object_data.getString("category_id");
                }
                if (object_data.has("category_name")) {
                    categoryname = object_data.getString("category_name");
                }
                if (object_data.has("subcategory_id")) {
                    subcategoryid = object_data.getString("subcategory_id");
                }
                if (object_data.has("subcategory_name")) {
                    subcategoryname = object_data.getString("subcategory_name");
                }
                if (object_data.has("subsubcategory_id")) {
                    subsubcategoryid = object_data.getString("subsubcategory_id");
                }
                if (object_data.has("subsubcategory_name")) {
                    subsubcategoryname = object_data.getString("subsubcategory_name");
                }

                String type1= "popular product";
                viewAllArrayList.add(new ViewAll1(type1,id1,name1,name2,image1,categoryid,categoryname,subcategoryid,subcategoryname,
                        subsubcategoryid,subsubcategoryname));
            }
            ViewAllRecyclerAdapter1 viewAllRecyclerAdapter = new ViewAllRecyclerAdapter1(ctx, viewAllArrayList, 1, cust_id);
            recyclerView.setBackgroundResource(R.color.white);
            recyclerView.setAdapter(viewAllRecyclerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void featuredServices(String data) {

        ArrayList<ViewAll1> viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject data_object = jsonObject.getJSONObject("data");

            JSONArray popular_product_array = data_object.getJSONArray("featured_service");
            for (int i = 0; i < popular_product_array.length(); i++) {

                JSONObject object_data = popular_product_array.getJSONObject(i);

                String id1=null,name1=null,name2=null,image1=null,categoryid=null,categoryname=null,subcategoryid=null,subcategoryname=null,subsubcategoryid=null,subsubcategoryname=null;
                if (object_data.has("service_id")){
                    id1 = object_data.getString("service_id");
                }if (object_data.has("service_name ")){
                    name1 = object_data.getString("service_name ");
                }if (object_data.has("image")){
                    image1 = object_data.getString("image");
                }
                if (object_data.has("price")){
                    name2 = object_data.getString("price");
                }

                if (object_data.has("category_id")) {
                    categoryid = object_data.getString("category_id");
                }
                if (object_data.has("category_name")) {
                    categoryname = object_data.getString("category_name");
                }
                if (object_data.has("subcategory_id")) {
                    subcategoryid = object_data.getString("subcategory_id");
                }
                if (object_data.has("subcategory_name")) {
                    subcategoryname = object_data.getString("subcategory_name");
                }
                if (object_data.has("subsubcategory_id")) {
                    subsubcategoryid = object_data.getString("subsubcategory_id");
                }
                if (object_data.has("subsubcategory_name")) {
                    subsubcategoryname = object_data.getString("subsubcategory_name");
                }
                String type1= "popular service";
                viewAllArrayList.add(new ViewAll1(type1,id1,name1,name2,image1,categoryid,categoryname,subcategoryid,subcategoryname,
                        subsubcategoryid,subsubcategoryname));
            }
            ViewAllRecyclerAdapter1 viewAllRecyclerAdapter = new ViewAllRecyclerAdapter1(ctx, viewAllArrayList, 2, cust_id);
            recyclerView.setBackgroundResource(R.color.white);
            recyclerView.setAdapter(viewAllRecyclerAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void allcategoy(String data) {

        ArrayList<ViewAll> viewAllArrayList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);

            JSONArray popular_product_array = jsonObject.getJSONArray("data");
            for (int i = 0; i < popular_product_array.length(); i++) {

                JSONObject object_data = popular_product_array.getJSONObject(i);

                String id=null,name=null,image=null;
                if (object_data.has("category_id")){
                    id = object_data.getString("category_id");
                }
                if (object_data.has("category_name")){
                    name = object_data.getString("category_name");
                }
                if (object_data.has("category_image")){
                    image = object_data.getString("category_image");
                }
                String type= "All Category";
                viewAllArrayList.add(new ViewAll(type,id,name,image));
            }
//            ViewAllAdapter viewAllAdapter = new ViewAllAdapter(ctx,viewAllArrayList,3);
//            gridView.setNumColumns(3);
//            gridView.setAdapter(viewAllAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initXml() {
        et_double_search = view.findViewById(R.id.et_double_search);
        gridView = view.findViewById(R.id.gv_viewall_gridview);
        recyclerView = view.findViewById(R.id.rv_viewall_recyclerview);
        location_sp = view.findViewById(R.id.sp_double_location);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(ctx, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        int padding = (int) ctx.getResources().getDimension(R.dimen._1sdp);
        recyclerView.setPadding(padding, padding, padding, padding);

        et_double_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFragment searchFragment = new SearchFragment(ctx);
                Utility.setFragment(searchFragment,ctx);
            }
        });
    }

    private void getWishlist(String cust_id) {
        final ProgressDialog progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("wait...");
        progressDialog.show();

        AndroidNetworking.post(WebApi.URL_GET_WISHLIST)
                .addBodyParameter("customer_id", cust_id)
                .setTag("test")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        progressDialog.dismiss();
                        sessionManager.setData(SessionManager.KEY_WISHLIST_DATA,response.toString());
                        setView();
                    }

                    @Override
                    public void onError(ANError error) {
                        progressDialog.dismiss();
                        Utility.toastView(ctx, error.toString());
                        // handle error
                    }
                });
    }

    private void setView() {
        String data = sessionManager.getData(SessionManager.HOME_API_DATA);
        if (service.equals("popular product")){
            Utility.setTitle(ctx, "popular product");
            popularProducts(data);
        }else if (service.equals("popular service")){
            popularServices(data);
            Utility.setTitle(ctx, "popular service");
        }else if (service.equals("Featured Product")){
            featuredProducts(data);
            Utility.setTitle(ctx, "Featured Product");
        }else if (service.equals("Featured service")){
            featuredServices(data);
            Utility.setTitle(ctx, "Featured service");
        }else if (service.equals("All category")){
            Utility.setTitle(ctx, "All Category");
            String all_cate = sessionManager.getData(SessionManager.HOME_API_ALL_CATE);
            allcategoy(all_cate);
        }
    }

    private void setResponse() {

        String s = sessionManager.getData(SessionManager.LOCATION_DATA);

        ArrayList<String> city_list = new ArrayList<>();
        city_list.add("Anywhere");
        try {
            JSONObject object = new JSONObject(s);
            String status = object.getString("status");

            if (status.equals("1")) {

                JSONArray array_location = object.getJSONArray("Locations");
                for (int i = 0; i < array_location.length(); i++) {

                    JSONObject data_object = array_location.getJSONObject(i);
                    String city = data_object.getString("city_name");
                    city_list.add(city);
                }
                ArrayAdapter<String> city_adapter =
                        new ArrayAdapter<String>(ctx, android.R.layout.simple_list_item_1, city_list);

                location_sp.setAdapter(city_adapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
