package igrand.technology.info.eganacsi.seller.response.myservices;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kamlesh on 5/16/2018.
 */

public class SellerServices implements Parcelable
{
    @SerializedName("service_id")
    @Expose
    private String serviceId;
    @SerializedName("service_name")
    @Expose
    private String serviceName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("service_price")
    @Expose
    private String servicePrice;
    @SerializedName("discount_price")
    @Expose
    private String discountPrice;
    @SerializedName("discount_percent")
    @Expose
    private String discountPercent;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("subcategory_id")
    @Expose
    private String subcategoryId;
    @SerializedName("subsubcategory_id")
    @Expose
    private String subsubcategoryId;
    @SerializedName("sunday")
    @Expose
    private String sunday;
    @SerializedName("monday")
    @Expose
    private String monday;
    @SerializedName("tuesday")
    @Expose
    private String tuesday;
    @SerializedName("wednesday")
    @Expose
    private String wednesday;
    @SerializedName("thursday")
    @Expose
    private String thursday;
    @SerializedName("friday")
    @Expose
    private String friday;
    @SerializedName("saturday")
    @Expose
    private String saturday;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("sort_order")
    @Expose
    private String sortOrder;

    protected SellerServices(Parcel in) {
        serviceId = in.readString();
        serviceName = in.readString();
        image = in.readString();
        servicePrice = in.readString();
        discountPrice = in.readString();
        discountPercent = in.readString();
        categoryId = in.readString();
        subcategoryId = in.readString();
        subsubcategoryId = in.readString();
        sunday = in.readString();
        monday = in.readString();
        tuesday = in.readString();
        wednesday = in.readString();
        thursday = in.readString();
        friday = in.readString();
        saturday = in.readString();
        description = in.readString();
        sortOrder = in.readString();
    }

    public static final Creator<SellerServices> CREATOR = new Creator<SellerServices>() {
        @Override
        public SellerServices createFromParcel(Parcel in) {
            return new SellerServices(in);
        }

        @Override
        public SellerServices[] newArray(int size) {
            return new SellerServices[size];
        }
    };

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(String discountPercent) {
        this.discountPercent = discountPercent;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public void setSubcategoryId(String subcategoryId) {
        this.subcategoryId = subcategoryId;
    }

    public String getSubsubcategoryId() {
        return subsubcategoryId;
    }

    public void setSubsubcategoryId(String subsubcategoryId) {
        this.subsubcategoryId = subsubcategoryId;
    }

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(serviceId);
        dest.writeString(serviceName);
        dest.writeString(image);
        dest.writeString(servicePrice);
        dest.writeString(discountPrice);
        dest.writeString(discountPercent);
        dest.writeString(categoryId);
        dest.writeString(subcategoryId);
        dest.writeString(subsubcategoryId);
        dest.writeString(sunday);
        dest.writeString(monday);
        dest.writeString(tuesday);
        dest.writeString(wednesday);
        dest.writeString(thursday);
        dest.writeString(friday);
        dest.writeString(saturday);
        dest.writeString(description);
        dest.writeString(sortOrder);
    }
}
