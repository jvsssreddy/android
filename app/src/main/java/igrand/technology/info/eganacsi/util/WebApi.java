package igrand.technology.info.eganacsi.util;

/**
 * Created by kamlesh on 4/4/2018.
 */

public class WebApi
{

    public final static String BASE_URL = "https://www.eganacsi.com/phasev2/ss/";

    // GET apis
    public static final String URL_SUBSCRIPTION= BASE_URL+"subscription-plans.php";
    public static final String URL_LOCATION = BASE_URL+"get-location.php";

    //     post apis

    // login,register

    public static final String URL_REGISTER = BASE_URL+"register.php";
    public static final String URL_SOCIAL_LOGIN = BASE_URL+"login-with-social.php";
    public final static String URL_LOGIN = BASE_URL+"login.php";

    // profile

    public static final String URL_UPDATE_PROFILE = BASE_URL+"edit-profile.php";
    public static final String URL_GET_PROFILE = BASE_URL+"profile.php";
    public static final String URL_CHANGE_PASSWORD = BASE_URL+"change-password.php";
    public static final String URL_CHANGE_MOBILE = BASE_URL+"change-mobile.php";
    public static final String URL_ADDRESS_BOOK = BASE_URL+"address-book.php";
    public static final String URL_ADD_NEW_ADDRESS = BASE_URL+"add-address.php";

    // product
    public final static String HOME_URL = "home.php";
    public final static String ALL_CATEGORY_URL = "category-details.php";
    public final static String PRODUCT_LIST_URL = "product-listing.php";
    public final static String Service_LIST_URL = "service-listing.php";

    public static final String URL_PRODUCT_DETAILS = BASE_URL+"get-product-details.php";
    public static final String URL_SUB_CATEGORY = BASE_URL+"subsub-category-details.php";
    public static final String URL_ADD_WISHLIST = BASE_URL+"wish-list.php";
    public static final String URL_DELETE_WISHLIST = BASE_URL+"delete-wishlist.php";
    public static final String URL_GET_WISHLIST = BASE_URL+"get-wishlist.php";

    public static final String URL_GET_ENQUIRY = BASE_URL+"enquiry-details.php";
    public static final String URL_GET_QUOTE = BASE_URL+"add-enquiry.php";
    public static final String URL_GET_QUOTE_DATA_CATE = BASE_URL+"sync-data1.php";
    public static final String URL_GET_QUOTE_DATA_SUBSUBCATE = BASE_URL+"product-sync-data.php";
    public static final String URL_GET_QUOTE_DATA_SERVICE = BASE_URL+"service-sync-data.php";

    public static final String URL_CHANGE_NOTIFICATION = BASE_URL+"notification.php";

    // other
    public static final String URL_FAQS = BASE_URL+"faq-listing.php";
    public static final String URL_PRIVACY_POLICY = BASE_URL+"privacy-policy.php";
    public static final String URL_ABOUT_US = BASE_URL+"about-screen.php";
    public static final String URL_TERMS = BASE_URL+"terms-screen.php";
    public static final String URL_CONTACT_US = BASE_URL+"contact-listing.php";
    public static final String URL_CONTACT_FILL = BASE_URL+"contact-fill.php";
    public static final String URL_SEARCHING = BASE_URL+"get-search.php";

    // seller
    public static final String URL_MY_LEAD = BASE_URL+"get-quotes.php";
    public static final String URL_ADD_SERVICE = BASE_URL+"seller-service-add.php";
}
