package igrand.technology.info.eganacsi.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import igrand.technology.info.eganacsi.activity.LoginActivity;
import igrand.technology.info.eganacsi.activity.MainActivity;


/**
 * Created by kamlesh on 11/25/2017.
 */
public class SessionManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context ctx;

    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "sessionmanager";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_FNAME = "fname";
    public static final String KEY_LNAME = "lname";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_CID = "cid";
    public static final String KEY_SELLER_ID = "seller_id";
    public static final String KEY_CUSTOMER_ID = "customer_id";
    public static final String KEY_ADDRESS = "address";

    public static final String KEY_PAGE = "page";

    public static final String KEY_WISHLIST_DATA = "wishlistdata";
    public static final String KEY_MAIN_ACTIVITY_TYPE = "mainactivity";

    public static final String HOME_API_DATA = "home_data";
    public static final String HOME_API_ALL_CATE= "all_cate";
    public static final String LOCATION_DATA= "location_data";
    public static final String PROFILE_DATA= "profile_data";

    public SessionManager(Context context) {
        this.ctx = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String fname, String lname,String mobile,String email,String c_id,String customer_id) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_FNAME, fname);
        editor.putString(KEY_LNAME, lname);
        editor.putString(KEY_MOBILE,mobile);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_CID, c_id);
        editor.putString(KEY_CUSTOMER_ID, customer_id);
        editor.commit();
    }

    public void setData(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void setBooleanData(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void checkLogin() {
        if (!this.isLoggedIn()) {
            Intent i = new Intent(ctx, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ctx.startActivity(i);
        }
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(i);
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public String getData(String key) {
        return pref.getString(key, null);
    }

    public boolean getBooleanData(String key) {
        return pref.getBoolean(key, false);
    }
}
