package igrand.technology.info.eganacsi.seller.response.MyLeads;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyLeadResponse
{
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private MyLeadData data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MyLeadData getData() {
        return data;
    }

    public void setData(MyLeadData data) {
        this.data = data;
    }
}
